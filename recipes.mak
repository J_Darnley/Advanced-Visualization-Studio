###############################################################################
# Recipes
###############################################################################

%.o: %.asm
	$(X86ASM) $(X86ASMFLAGS) $(X86ASM_DEPFLAGS) -o $@ $<
	$(STRIP) -x $@

%.a:
	$(RM) $@
	$(AR) $(ARFLAGS) $@ $^

%.so:
	$(LD) $(SHFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

%_g$(EXESUF): %.o
	$(LD) $(LDFLAGS) -o $@ $(filter %.o,$^) $(LDLIBS)

%$(EXESUF): %_g$(EXESUF)
	$(CP) $< $@
	$(STRIP) $@
