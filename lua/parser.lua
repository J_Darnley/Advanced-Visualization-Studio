-- Copyright (c) 2016-2020 James Darnley <james.darnley@gmail.com>
--
-- This file is part of Advanced Visualization Studio.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- TODO:
-- * Should this parser be adding defaults or leaving the values as nil?
-- * JSON parser for webvs files (javascript port or reimplementation)

local bit = {}

-- LuaJIT comes with a bit library
if _VERSION == 'Lua 5.1' then
    bit = require('bit')

-- Lua 5.2 has bit32
elseif _VERSION == 'Lua 5.2' then
    bit = require('bit32')

-- Lua 5.3 and newer has its own bitwise operators so load a shim and use them
else
    load([[
        function band(a,b)   return a &  b end
        function bor(a,b)    return a |  b end
        function rshift(a,b) return a >> b end
        function bxor(a,b)   return a ~  b end
    ]], 'bit operator shim', 't', bit)()
end
local band = bit.band
local bor = bit.bor
local bsr = bit.rshift
local bxor = bit.bxor

local assert = assert
local insert = table.insert
local unpack = assert(string.unpack, 'string.unpack is needed for this script')

-- Wrap data in a little table to track state
local function wrap_data(data, pos, len)
    local wrap = {
        data = data, pos = pos, rem = len
    }
    function wrap:adv(len)
        self.pos = self.pos + len
        self.rem = self.rem - len
    end
    return wrap
end

-- Basic functions to extract the values from the binary data string.  Uses Lua
-- 5.3's string.unpack, or could use the old 'struct' module.
--
-- They all return nothing if there was an overflow error.

-- 32-bit boolean value, C logic: 0 = false, other = true
local function b32(wrap)
    if wrap.rem < 4 then
        log.warn('buffer overflow when reading 4 bytes at %d (%#x)', wrap.pos-1, wrap.pos-1)
        return
    end
    local ret = unpack('<I4', wrap.data, wrap.pos) ~= 0
    wrap:adv(4)
    return ret
end

-- Signed 32-bit integer
local function s32(wrap)
    if wrap.rem < 4 then
        log.warn('buffer overflow when reading 4 bytes at %d (%#x)', wrap.pos-1, wrap.pos-1)
        return
    end
    local ret = unpack('<i4', wrap.data, wrap.pos)
    wrap:adv(4)
    return ret
end

-- Unsigned 8-bit integer
local function u8(wrap)
    if wrap.rem < 1 then
        log.warn('buffer overflow when reading 1 byte at %d (%#x)', wrap.pos-1, wrap.pos-1)
        return
    end
    local ret = unpack('<B', wrap.data, wrap.pos)
    wrap:adv(1)
    return ret
end

-- Single precision floating point number
local function f32(wrap)
    if wrap.rem < 4 then
        log.warn('buffer overflow when reading 4 bytes at %d (%#x)', wrap.pos-1, wrap.pos-1)
        return
    end
    local ret = unpack('f', wrap.data, wrap.pos)
    wrap:adv(4)
    return ret
end

-- Unsigned 32-bit integer
local function u32(wrap)
    if wrap.rem < 4 then
        log.warn('buffer overflow when reading 4 bytes at %d (%#x)', wrap.pos-1, wrap.pos-1)
        return
    end
    local ret = unpack('<I4', wrap.data, wrap.pos)
    wrap:adv(4)
    return ret
end

-- RString
local function rstring(wrap)
    local len = u32(wrap)
    if wrap.rem < len then
        log.warn('buffer overflow when reading %d bytes at %d (%#x)', len, wrap.pos-1, wrap.pos-1)
        return
    end
    if len == 0 then
        return ''
    end
    assert(len < 32768)
    local str = unpack('z', wrap.data, wrap.pos)
    assert(#str == len-1) -- The RString length includes the NUL byte
    wrap:adv(len)
    return str
end

-- Handle colours in a special way
local function colour(wrap)
    if wrap.rem < 4 then
        log.warn('buffer overflow when reading 4 bytes at %d (%#x)', wrap.pos-1, wrap.pos-1)
        return
    end
    local  b, g, r, x = unpack('BBBB', wrap.data, wrap.pos)
    assert(x == 0)
    wrap:adv(4)
    return string.format('#%02x%02x%02x', r, g, b)
end

-- Map values to others through a LUT
local function value_map(wrap, lut)
    if wrap.rem < 4 then
        log.warn('buffer overflow when reading 4 bytes at %d (%#x)', wrap.pos-1, wrap.pos-1)
        return
    end
    local val = u32(wrap)
    -- Lua sequences start at 1 but the common LUTs start with 0.  If the LUT
    -- has a 0 value then don't add 1 to the value.
    local ret = lut[val + (lut[0] and 0 or 1)]
    if not ret then
        log.warn('unknown value (%d) for lut at %d (%#x)', val, wrap.pos-5, wrap.pos-5)
        return
    end
    return ret
end

if not pretty_print then
    colour = u32
    value_map = u32
end

-- Number prefixed list of colours
local function colour_list(wrap, num)
    local colours = {}
    if not num then
        num = u32(wrap)
    end
    if wrap.rem < 4*num then
        log.warn('buffer overflow when reading %d colours at %d (%#x)', num, wrap.pos-1, wrap.pos-1)
        return
    end
    for i = 1, num do
        insert(colours, colour(wrap))
    end
    return colours
end

-- Component parsers

local parser_functions = {}
local function get_parser(code, signature)
    if signature then
        return parser_functions[signature]
    else
        return parser_functions[code]
    end
end

local function parse_generic(wrap, type, fields)
    assert(wrap and type and fields)
    local ret = { type = type }
    for i = 1, #fields do
        local key, func, arg = fields[i][1], fields[i][2], fields[i][3]
        if key == nil then
            wrap:adv(func)
        else
            ret[key] = func(wrap, arg)
        end
    end
    return ret;
end

local lut_audio_channel = {
    [0] = 'Left',
    [1] = 'Right',
    [2] = 'Center',
}

local lut_audio_source = {
    [0] = 'Waveform', [false] = 'Waveform',
    [1] = 'Spectrum', [true] = 'Spectrum',
}

local lut_draw_mode = {
    [0] = 'Dots', [false] = 'Dots',
    [1] = 'Lines', [true] = 'Lines',
}

local lut_position_x = {
    [0] = 'Left',
    [1] = 'Right',
    [2] = 'Center',
}

local lut_position_y = {
    [0] = 'Top',
    [1] = 'Bottom',
    [2] = 'Center',
}

-- Render / Simple
local function parse_simple(wrap)
    local options = { type = 'Simple' }

    local effect = u32(wrap)
    if band(effect, 0x43) == 0 then
        options.audioSource = 'Spectrum'
        options.renderType = 'Solid'
    elseif band(effect, 0x43) == 1 then
        options.audioSource = 'Spectrum'
        options.renderType = 'Lines'
    elseif band(effect, 0x43) == 2 then
        options.audioSource = 'Waveform'
        options.renderType = 'Lines'
    elseif band(effect, 0x43) == 3 then
        options.audioSource = 'Waveform'
        options.renderType = 'Solid'
    elseif band(effect, 0x43) == 0x40 then
        options.audioSource = 'Spectrum'
        options.renderType = 'Dots'
    elseif band(effect, 0x43) == 0x42 then
        options.audioSource = 'Waveform'
        options.renderType = 'Dots'
    end

    options.audioChannel = lut_audio_channel[band(bsr(effect, 2), 3)]
    options.positionY = lut_position_y[band(bsr(effect, 4), 3)]
    options.colors = colour_list(wrap)

    return options
end
parser_functions[0] = parse_simple

-- Render / Dot Plane
local function parse_dot_plane(wrap)
    return parse_generic(wrap, 'DotPlane', {
        { 'rotationSpeed', s32 },
        { 'colorBottom', colour },
        { 'colorLow', colour },
        { 'colorMid', colour },
        { 'colorHigh', colour },
        { 'colorTop', colour },
        { 'angle', s32 },
        { nil, 4 } -- skip the stored rotation position
    })
end
parser_functions[1] = parse_dot_plane

-- Render / Oscilloscope Star
local function parse_osc_star(wrap)
    local options = { type = 'OscilliscopeStar' }
    local effect = u32(wrap)
    options.audioChannel = lut_audio_channel[band(bsr(effect, 2), 3)]
    options.positionX = lut_position_x[band(bsr(effect, 4), 3)]
    options.colors = colour_list(wrap)
    options.size = u32(wrap)
    options.rotation = s32(wrap)
    return options
end
parser_functions[2] = parse_osc_star

-- Trans / Fadeout
local function parse_fadeout(wrap)
    return parse_generic(wrap, 'FadeOut', {
        { 'speed', u32 },
        { 'color', colour }
    })
end
parser_functions[3] = parse_fadeout

-- Trans / Blitter Feedback
local function parse_blit(wrap)
    return parse_generic(wrap, 'BlitterFeedback', {
        { 'zoom', u32 },
        { 'onBeatZoom', u32 },
        { 'blend', b32 },
        { 'onBeat', b32 },
        { 'bilinear', b32 }
    })
end
parser_functions[4] = parse_blit

-- Render / OnBeat Clear
local function parse_beat_clear(wrap)
    return parse_generic(wrap, 'OnBeatClear', {
        { 'color', colour },
        { 'blend', b32 },
        { 'clearBeats', u32 },
    })
end
parser_functions[5] = parse_beat_clear

-- Trans / Blur
local function parse_blur(wrap)
    return parse_generic(wrap, 'Blur', {
        { 'blur', u32 },
        { 'round', b32 },
    })
end
parser_functions[6] = parse_blur

-- Render / Bass Spin
local function parse_bass_spin(wrap)
    local options = { type = 'BassSpin' }
    local temp = u32(wrap)
    if temp then
        options.enabledLeft = band(temp, 1) ~= 0
        options.enabledRight = band(temp, 2) ~= 0
    end
    options.colorLeft = colour(wrap)
    options.colorRight = colour(wrap)
    options.mode = value_map(wrap, { 'Lines', 'Triangles' })
    return options
end
parser_functions[7] = parse_bass_spin

-- Render / Moving Particle
local function parse_particle(wrap)
    local options = { type = 'MovingParticle' }
    local temp = u32(wrap)
    if temp then
        options.enabled = band(temp, 1) ~= 0
        options.onBeatSizeChange = band(temp, 2) ~= 0
    end
    options.color = colour(wrap)
    options.range = u32(wrap)
    options.size = u32(wrap)
    options.onBeatSize = u32(wrap)
    options.output = value_map(wrap, { 'Replace', 'Additive', '50/50', 'Default' })
    return options
end
parser_functions[8] = parse_particle

-- Trans / Roto Blitter
local function parse_roto_blit(wrap)
    return parse_generic(wrap, 'RotoBlitter', {
        { 'zoom', u32 },
        { 'rotate', u32 },
        { 'output', value_map, { 'Replace', '50/50' } },
        { 'onBeatReverse', b32 },
        { 'reversalSpeed', u32 },
        { 'onBeatZoom', u32 },
        { 'onBeat', b32 },
        { 'bilinear', b32 },
    })
end
parser_functions[9] = parse_roto_blit

-- Render / SVP Loader, id 10, not implemented

-- Trans / Colorfade
local function parse_colorfade(wrap)
    local options = { type = 'Colorfade' }
    local temp = u32(wrap)
    if temp then
        options.enabled = band(temp, 1) ~= 0
        options.onBeatRandom = band(temp, 2) ~= 0
        options.onBeat = band(temp, 4) ~= 0
    end
    options.fader1 = s32(wrap)
    options.fader2 = s32(wrap)
    options.fader3 = s32(wrap)
    options.beatFader1 = s32(wrap)
    options.beatFader2 = s32(wrap)
    options.beatFader3 = s32(wrap)
    return options
end
parser_functions[11] = parse_colorfade

-- Trans / Color Clip
local function parse_color_clip(wrap)
    return parse_generic(wrap, 'ColorClip', {
        { 'mode', value_map, { 'Off', 'Below', 'Above', 'Near' } },
        { 'colorFrom', colour },
        { 'colorTo', colour },
        { 'colorDistance', u32 },
    })
end
parser_functions[12] = parse_color_clip

-- Render / Rotating Stars
local function parse_rot_star(wrap)
    return parse_generic(wrap, 'RotatingStars', {
        { 'colors', colour_list },
    })
end
parser_functions[13] = parse_rot_star

-- Render / Ring
local function parse_ring(wrap)
    local options = { type = 'Ring' }
    local temp = u32(wrap)
    if temp then
        options.audioChannel = lut_audio_channel[band(bsr(temp, 2), 3)]
        options.positionX = lut_position_x[band(bsr(temp, 4), 3)]
    end
    options.colours = colour_list(wrap)
    options.size = u32(wrap)
    options.audioSource = value_map(wrap, lut_audio_source)
    return options
end
parser_functions[14] = parse_ring

-- Trans / Movement
local function parse_movement(wrap)
    local options = { type = 'Movement' }
    local rectangular
    local effect = u32(wrap)
    if effect == 32767 then
        if wrap.data:sub(wrap.pos, wrap.pos+6) == '!rect ' then
            wrap:adv(6)
            rectangular = 1
        end
        if wrap.data:byte(wrap.pos) == 1 then
            wrap:adv(1)
            options.code = rstring(wrap)
        elseif wrap.rem >= 256 then
            local l = 256 - (rectangular and 6 or 0)
            options.code = wrap.data:sub(wrap.pos, wrap.pos + l)
            wrap:adv(l)
        end
    end
    options.blend = b32(wrap)
    options.sourceMapped = u32(wrap) -- TODO drop grey state and use boolean?
    options.coordinates = u32(wrap)
    if options.coordinates then
        options.coordinates = options.coordinates ~= 0
    else
        options.coordinates = rectangular
    end
    options.bilinear = b32(wrap) -- TODO default?
    options.wrap = b32(wrap) -- TODO default?
    if effect == 0 and wrap.rem >= 4 then
        effect = u32(wrap)
    end
    if effect ~= 32767 and effect > 23 then
        effect = 0
    end
    options.effect = effect -- TODO look up table?
    return options
end
parser_functions[15] = parse_movement

-- Trans / Scatter
local function parse_scatter(wrap)
    return parse_generic(wrap, 'Scatter', {
        { 'enabled', b32 },
    })
end
parser_functions[16] = parse_scatter

-- Rander / Dot Grid
local function parse_dot_grid(wrap)
    return parse_generic(wrap, 'DotGrid', {
        { 'colors', colour_list },
        { 'spacing', u32 },
        { 'speedX', s32 },
        { 'speedY', s32 },
        { 'output', value_map, { 'Replace', 'Additive', '50/50', 'Default' } },
    })
end
parser_functions[17] = parse_dot_grid

-- Misc / Buffer Save
local function parse_buffer_save(wrap)
    return parse_generic(wrap, 'BufferSave', {
        { 'mode', value_map, {
            'Save', 'Restore', 'Alternate Save/Restore', 'Alternate Restore/Save'
        } },
        { 'buffer', u32 },
        { 'blend', value_map, {
            [0] = 'Replace',           [9]  = 'Subtractive 2',
            [1] = 'Average',           [6]  = 'Xor',
            [2] = 'Additive',          [7]  = 'Maximum',
            [3] = 'Every other pixel', [8]  = 'Minimum',
            [5] = 'Every other line',  [10] = 'Multiply',
            [4] = 'Subtractive 1',     [11] = 'Adjustable',
        } },
        { 'adjustBlend', u32 },
    })
end
parser_functions[18] = parse_buffer_save

-- Render / Dot Fountain
local function parse_dot_fountain(wrap)
    return parse_generic(wrap, 'DotFountain', {
        { 'rotationSpeed', s32 },
        { 'colorTop', colour },
        { 'colorHigh', colour },
        { 'colorMid', colour },
        { 'colorLow', colour },
        { 'colorBottom', colour },
        { 'angle', s32 },
        { nil, 4 } -- current rotation position stored in a float
    })
end
parser_functions[19] = parse_dot_fountain

-- Trans / Water
local function parse_water(wrap)
    return parse_generic(wrap, 'Water', {
        { 'enabled', b32 },
    })
end
parser_functions[20] = parse_water

-- Misc / Comment
local function parse_comment(wrap)
    return parse_generic(wrap, 'Comment', {
        { 'text', rstring },
    })
end
parser_functions[21] = parse_comment

-- Trans / Brightness
local function parse_brightness(wrap)
    return parse_generic(wrap, 'Brightness', {
        { 'enabled', b32 },
        { 'output', u32 },
        { '50/50', u32 }, -- TODO 64-bit read
        { 'red', s32 },
        { 'green', s32 },
        { 'blue', s32 },
        { 'separate', b32 },
        { 'excludeColor', colour },
        { 'exclude', b32 },
        { 'distance', u32 },
    })
end
parser_functions[22] = parse_brightness

-- Trans / Interleave
local function parse_interleave(wrap)
    return parse_generic(wrap, 'Interleave', {
        { 'enabled', b32 },
        { 'x', u32 },
        { 'y', u32 },
        { 'color', colour },
        { 'output', u32 },
        { '50/50', u32 }, -- TODO 64-bit read
        { 'onbeat', b32 },
        { 'x2', u32 },
        { 'y2', u32 },
        { 'beatDuration', u32 },
    })
end
parser_functions[23] = parse_interleave

-- Trans / Grain
local function parse_grain(wrap)
    return parse_generic(wrap, 'Grain', {
        { 'enabled', b32 },
        { 'output', u32 },
        { '50/50', u32 }, -- TODO 64-bit read
        { 'amount', u32 },
        { 'static', b32 },
    })
end
parser_functions[24] = parse_grain

-- Render / Clear Screen
local function parse_clear_screen(wrap)
    return parse_generic(wrap, 'ClearScreen', {
        { 'enabled', b32 },
        { 'color', colour },
        { 'output', u32 },
        { '50/50', u32 }, -- TODO 64-bit read
        { 'onlyFirst', b32 },
    })
end
parser_functions[25] = parse_clear_screen

-- Trans / Mirror
local function parse_mirror(wrap)
    local options = { type = 'Mirror' }
    options.enabled = b32(wrap)
    local temp = u32(wrap)
    if temp then
        options.topToBottom = band(temp, 1) ~= 0
        options.bottomToTop = band(temp, 2) ~= 0
        options.leftToRight = band(temp, 4) ~= 0
        options.rightToLeft = band(temp, 8) ~= 0
    end
    options.onBeat = b32(wrap)
    options.smooth = b32(wrap)
    options.speed = u32(wrap)
    return options
end
parser_functions[26] = parse_mirror

-- Render / Starfield
local function parse_starfield(wrap)
    return parse_generic(wrap, 'Starfield', {
        { 'enabled', b32 },
        { 'color', colour },
        { 'output', u32 },
        { '50/50', u32 }, -- TODO 64-bit read
        { 'WarpSpeed', f32 },
        { 'MaxStars_set', u32 },
        { 'onbeat', b32 },
        { 'spdBeat', f32 },
        { 'durFrames', u32 },
    })
end
parser_functions[27] = parse_starfield

-- Render / Text, id 28, not implemented

-- Render / Bump
local function parse_bump(wrap)
    local options = { type = 'Bump' }
    options.enabled = b32(wrap)
    options.onBeat = b32(wrap)
    options.duration = u32(wrap)
    options.depth = u32(wrap)
    options.onBeatDepth = u32(wrap)
    options.output = u32(wrap)
    options.avg = u32(wrap) -- TODO 64-bit read
    options.code = {
        perFrame = rstring(wrap),
        onbeat = rstring(wrap),
        init = rstring(wrap),
    }
    options.showDot = b32(wrap)
    options.invertDepth = b32(wrap)
    options.oldstyle = u32(wrap)
    if options.oldstyle then
        options.oldstyle = options.oldstyle ~= 0
    else
        options.oldstyle = true
    end
    options.depthBuffer = u32(wrap)
    return options
end
parser_functions[29] = parse_bump

-- Trans / Mosaic
local function parse_mosaic(wrap)
    return parse_generic(wrap, 'Mosaic', {
        { 'enabled', b32 },
        { 'size', u32 },
        { 'sizeOnBeat', u32 },
        { 'output', u32 },
        { 'avg', u32 }, -- TODO 64-bit read
        { 'onbeat', b32 },
        { 'durFrames', u32 },
    })
end
parser_functions[30] = parse_mosaic

-- Trans / Water Bump
local function parse_water_bump(wrap)
    return parse_generic(wrap, 'WaterBump', {
        { 'enabled', b32 },
        { 'density', u32 },
        { 'depth', u32 },
        { 'random', b32 },
        { 'dropPositionX', value_map, { 'Left', 'Center', 'Right' } },
        { 'dropPositionY', value_map, { 'Top', 'Middle', 'Bottom' } },
        { 'dropRadius', u32 },
        { 'method', u32 },
    })
end
parser_functions[31] = parse_water_bump

-- Render / AVI, id 32, not implemented

-- Misc / Custom BPM
local function parse_custom_bpm(wrap)
    return parse_generic(wrap, 'CustomBPM', {
        { 'enabled', b32 },
        { 'arbitrary', b32 },
        { 'skip', b32 },
        { 'invert', b32 },
        { 'arbitraryValue', u32 },
        { 'skipValue', u32 },
        { 'skipFirstBeats', u32 },
    })
end
parser_functions[33] = parse_custom_bpm

-- Render / Picture, id 34, not implemented

-- Trans / Dynamic Distance Modifier
local function parse_dynamic_distance_modifier(wrap)
    local options = { type = 'DynamicDistanceModifier' }
    if wrap.data:byte(wrap.pos) == 1 then
        wrap:adv(1);
        options.code = {
            perPoint = rstring(wrap),
            perFrame = rstring(wrap),
            onBeat = rstring(wrap),
            init = rstring(wrap),
        }
    else
        log.error('unsupported fixed length strings at %d (%#x)', wrap.pos-1, wrap.pos-1)
    end
    options.blendMode = u32(wrap)
    options.bilinear = u32(wrap)
    return options
end
parser_functions[35] = parse_dynamic_distance_modifier

-- Render / SuperScope
local function parse_superscope(wrap)
    local options = { type = 'SuperScope' }
    if wrap.data:byte(wrap.pos) == 1 then
        wrap:adv(1);
        options.code = {
            perPoint = rstring(wrap),
            perFrame = rstring(wrap),
            onBeat = rstring(wrap),
            init = rstring(wrap),
        }
    else
        log.error('unsupported fixed length strings at %d (%#x)', wrap.pos-1, wrap.pos-1)
    end
    local temp = u32(wrap)
    if temp then
        options.audioChannel = lut_audio_channel[band(temp, 3)]
        options.audioSource = lut_audio_source[band(temp, 4) ~= 0]
    end
    options.colors = colour_list(wrap)
    options.drawMode = lut_draw_mode[b32(wrap)]
    return options
end
parser_functions[36] = parse_superscope

-- Trans / Invert
local function parse_invert(wrap)
    return parse_generic(wrap, 'Invert', {
        { 'enabled', b32 },
    })
end
parser_functions[37] = parse_invert

-- Trans / Unique Tone
local function parse_unique_tone(wrap)
    return parse_generic(wrap, 'UniqueTone', {
        { 'enabled', b32 },
        { 'color', colour },
        { 'blend', u32 },
        { 'avg', u32 }, -- TODO 64-bit read
        { 'invert', b32 },
    })
end
parser_functions[38] = parse_unique_tone

-- Render / Timescope
local function parse_timescope(wrap)
    return parse_generic(wrap, 'Timescope', {
        { 'enabled', b32 },
        { 'color', colour },
        { 'blend', u32 },
        { 'avg', u32 }, -- TODO 64-bit read
        { 'audioChannel', value_map, lut_audio_channel },
        { 'bands', u32 },
    })
end
parser_functions[39] = parse_timescope

-- Misc / Set render mode
local function parse_render_mode(wrap)
    local options = { type = 'SetRenderMode' }
    if wrap.rem >= 4 then
        local lut = {
            'Replace', 'Additive', 'Maximum', 'Average', 'Subtractive 1',
            'Subtractive 2', 'Multiply', 'Adjustable', 'Xor', 'Minimum'
        }
        options.blend = lut[u8(wrap)+1]
        options.adjustBlend = u8(wrap)
        options.lineSize = u8(wrap)
        options.enabled = band(u8(wrap), 0x80) ~= 0
    end
    return options
end
parser_functions[40] = parse_render_mode

-- Trans / Interferences
local function parse_interferences(wrap)
    return parse_generic(wrap, 'Interferences', {
        { 'enabled', b32 },
        { 'numberOfLayers', u32 },
        { nil, 4 }, -- skip current rotation
        { 'distance', u32 },
        { 'alpha', u32 },
        { 'rotation', s32 },
        { 'blendMode', u32 },
        { 'avg', u32 }, -- TODO 64-bit read
        { 'onBeatDistance', u32 },
        { 'onBeatAlpha', u32 },
        { 'onBeatRotation', s32 },
        { 'separateRGB', b32 },
        { 'onBeat', b32 },
        { 'speed', f32 },
    })
end
parser_functions[41] = parse_interferences

-- Trans / Dynamic Shift
local function parse_dynamic_shift(wrap)
    local options = { type = 'DynamicShift' }
    if wrap.data:byte(wrap.pos) == 1 then
        wrap:adv(1);
        options.code = {
            init = rstring(wrap),
            perFrame = rstring(wrap),
            onBeat = rstring(wrap),
        }
    else
        log.error('unsupported fixed length strings at %d (%#x)', wrap.pos-1, wrap.pos-1)
    end
    options.blendMode = u32(wrap)
    options.bilinear = b32(wrap)
    return options
end
parser_functions[42] = parse_dynamic_shift

-- Trans / Dynamic Movement
local function parse_dynamic_movement(wrap)
    local options = { type = 'DynamicMovement' }
    if wrap.data:byte(wrap.pos) == 1 then
        wrap:adv(1);
        options.code = {
            perPoint = rstring(wrap),
            perFrame = rstring(wrap),
            onBeat = rstring(wrap),
            init = rstring(wrap),
        }
    else
        log.error('unsupported fixed length strings at %d (%#x)', wrap.pos-1, wrap.pos-1)
    end
    options.bilinear = b32(wrap)
    options.coord = u32(wrap)
    options.gridW = u32(wrap)
    options.gridH = u32(wrap)
    options.blend = b32(wrap)
    options.wrap = b32(wrap)
    options.buffer = u32(wrap)
    options.alphaOnly = b32(wrap)
    return options
end
parser_functions[43] = parse_dynamic_movement

-- Trans / Fast Brightness
local function parse_fast_brightness(wrap)
    return parse_generic(wrap, 'FastBrightness', {
        { 'factor', value_map, { 2.0, 0.5, 1.0 } },
    })
end
parser_functions[44] = parse_fast_brightness

-- Trans / Color Modifier
local function parse_colour_modifier(wrap)
    local options = { type = 'ColorModifier' }
    if wrap.data:byte(wrap.pos) == 1 then
        wrap:adv(1);
        options.code = {
            perPoint = rstring(wrap),
            perFrame = rstring(wrap),
            onBeat = rstring(wrap),
            init = rstring(wrap),
        }
    else
        log.error('unsupported fixed length strings at %d (%#x)', wrap.pos-1, wrap.pos-1)
    end
    options.recomputeEveryFrame = b32(wrap)
    return options
end
parser_functions[45] = parse_colour_modifier

-- Effect List
local function parse_effect_list(wrap, depth)
    assert(depth < 8)

    local options = {
        components = {},
    }

    local mode = u8(wrap)
    if depth == 1 then
        -- Check that the root list only has 1 bit set
        assert(mode == 0 or mode == 1)
    end

    options.clearFrame = band(mode, 1) ~= 0

    local ext = 0
    if band(mode, 128) ~= 0 then
        options.type = 'EffectList'

        assert(u8(wrap) == 0)
        options.enabled = band(mode, 2) == 0 -- ((mode&2)^2) Yes!  XOR!
        options.input = u8(wrap)  -- ((mode>>8)&31)
        options.output = u8(wrap) -- ((mode>>16)&31)^1
        ext = u8(wrap)

        local sub = wrap_data(wrap.data, wrap.pos, ext)
        options.inAdjustBlend = u32(sub)
        options.outAdjustBlend = u32(sub)
        options.inBuffer = u32(sub)
        options.outBuffer = u32(sub)
        options.inBufferInvert = b32(sub)
        options.outBufferInvert = b32(sub)
        options.enableOnBeat = b32(sub)
        options.enableOnBeatFor = u32(sub)

        assert(sub.rem == 4)
        wrap:adv(ext-4)
    end

    while wrap.rem > 0 do
        local offset = wrap.pos - 1

        local code, sig = s32(wrap)
        if code >= 16384 then
           sig = unpack('z', wrap.data, wrap.pos)
           assert(#sig < 32)
           wrap:adv(32)
        end
        local size = u32(wrap)

        if size > wrap.rem then
            log.error('malformed preset at %d (%#x) code: %d, size: %d, sig: %s, rem: %d, depth: %d',
                    offset, offset, code, size, sig, wrap.rem, depth)
        end

        local sub = wrap_data(wrap.data, wrap.pos, size)

        -- Code config for the current effect list
        if ext > 0 and code == 16384 and sig == 'AVS 2.8+ Effect List Config' then
            options.codeEnabled = b32(sub)
            options.init = rstring(sub)
            options.frame = rstring(sub)

        -- Effect list has a code of 0xfffffffe which read as signed is a nice -2
        elseif code == -2 then
            local component = parse_effect_list(sub, depth+1)
            assert(component) -- assert that the settings were returned
            insert(options.components, component)

        else
            local comp_func = get_parser(code, sig)
            if not comp_func then
                log.error('unknown component, code: %d, size: %d, sig: %s',
                        code, size, sig)
            end
            local component = comp_func(sub)
            assert(component) -- assert that the settings were returned
            insert(options.components, component)
        end

        assert(sub.rem == 0) -- check that all data was consumed
        wrap:adv(size)
    end

    assert(wrap.rem == 0)
    return options
end

-- Module functions
local function preset_parse_binary(data)
    -- TODO: version 0.1
    local signature = 'Nullsoft AVS Preset 0.2\x1a'
    local file_sig = data:sub(1, #signature)
    if file_sig ~= signature then
        log.error('Invalid file signature: %q', file_sig)
    end

    local wrap = wrap_data(data, #signature + 1, #data - #signature)
    return parse_effect_list(wrap, 1)
end

function preset_parse(filename)
    local f = assert(io.open(filename, 'rb'))
    local data = f:read('*a')
    if not data or #data == 0 then
        log.error('no data read from file: %s', filename)
        return
    end
    f:close()

    if filename:sub(-3) == 'avs' then
        return preset_parse_binary(data)
    -- elseif filename:sub(-4) == 'json' then
        -- TODO: JSON decode
    else
        error('file '..filename..' has unknown type')
    end
end
