;*
;* Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
;*
;* This File is part of Advanced Visualization Studio.
;*
;* This program is free software: you can redistribute it and/or modify
;* it under the terms of the GNU Affero General Public License as
;* published by the Free Software Foundation, either version 3 of the
;* License, or (at your option) any later version.
;*
;* This program is distributed in the hope that it will be useful,
;* but WITHOUT ANY WARRANTY; without even the implied warranty of
;* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;* GNU Affero General Public License for more details.
;*
;* You should have received a copy of the GNU Affero General Public License
;* along with this program.  If not, see <https://www.gnu.org/licenses/>.
;*

%include "x86/x86util.asm"

SECTION_RODATA

%macro BYTE_POSITION 2
    %ifidn %2,R
        %assign %1 2
    %elifidn %2,G
        %assign %1 1
    %elifidn %2,B
        %assign %1 0
    %endif
%endmacro

%macro CHANNEL_ORDER 3
    BYTE_POSITION LSB, %3
    BYTE_POSITION MID, %2
    BYTE_POSITION MSB, %1
    %rep 2
        %assign i 0
        %rep 4
            db LSB+i, MID+i, MSB+i, 3+i
            %assign i i+4
        %endrep
    %endrep
    %undef LSB
    %undef MID
    %undef MSB
%endmacro

mask:
CHANNEL_ORDER R,B,G
CHANNEL_ORDER G,B,R
CHANNEL_ORDER G,R,B
CHANNEL_ORDER B,R,G
CHANNEL_ORDER B,G,R

SECTION_TEXT

%macro emms_if_mmx 0
    %if mmsize == 8
        emms
    %endif
%endmacro

%macro CHANNEL_SHIFT 0
cglobal channel_shift, 3, 4, 2, 0, frame, pixels, which
    shl          whichd,  5
    movsxdifnidn whichq,  whichd
    lea          r3,     [mask]
    mova         m1,     [r3+whichq]
    .loop:
        movu    m0,      [frameq]
        pshufb  m0,       m1
        movu   [frameq],  m0

        add frameq,  mmsize
        sub pixelsd, mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_XMM ssse3
CHANNEL_SHIFT

INIT_YMM avx2
CHANNEL_SHIFT
