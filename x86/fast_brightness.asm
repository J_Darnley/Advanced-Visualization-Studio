;*
;* Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
;*
;* This file is part of Advanced Visualization Studio.
;*
;* This program is free software: you can redistribute it and/or modify
;* it under the terms of the GNU Affero General Public License as
;* published by the Free Software Foundation, either version 3 of the
;* License, or (at your option) any later version.
;*
;* This program is distributed in the hope that it will be useful,
;* but WITHOUT ANY WARRANTY; without even the implied warranty of
;* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;* GNU Affero General Public License for more details.
;*
;* You should have received a copy of the GNU Affero General Public License
;* along with this program.  If not, see <https://www.gnu.org/licenses/>.
;*

%include "x86/x86util.asm"

SECTION_RODATA

mask:   times 32 db 0x7f

SECTION_TEXT

%macro emms_if_mmx 0
    %if mmsize == 8
        emms
    %endif
%endmacro

%macro FAST_BRIGHTNESS_MUL2 0
cglobal fast_brightness_mul2, 2, 2, 1, 0, frame, pixels
    .loop:
        movu     m0,      [frameq]
        paddusb  m0,       m0
        movu    [frameq],  m0

        add frameq,  mmsize
        sub pixelsd, mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

%macro FAST_BRIGHTNESS_DIV2 0
cglobal fast_brightness_div2, 2, 2, 1, 0, frame, pixels
    .loop:
        movu   m0,      [frameq]
        psrld  m0,       1 ; Does it matter for speed which size shift is used?
        pand   m0,      [mask]
        movu  [frameq],  m0

        add frameq,  mmsize
        sub pixelsd, mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
FAST_BRIGHTNESS_MUL2
FAST_BRIGHTNESS_DIV2

INIT_XMM sse2
FAST_BRIGHTNESS_MUL2
FAST_BRIGHTNESS_DIV2

INIT_YMM avx2
FAST_BRIGHTNESS_MUL2
FAST_BRIGHTNESS_DIV2
