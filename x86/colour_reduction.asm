;*
;* Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
;*
;* This file is part of Advanced Visualization Studio.
;*
;* This program is free software: you can redistribute it and/or modify
;* it under the terms of the GNU Affero General Public License as
;* published by the Free Software Foundation, either version 3 of the
;* License, or (at your option) any later version.
;*
;* This program is distributed in the hope that it will be useful,
;* but WITHOUT ANY WARRANTY; without even the implied warranty of
;* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;* GNU Affero General Public License for more details.
;*
;* You should have received a copy of the GNU Affero General Public License
;* along with this program.  If not, see <https://www.gnu.org/licenses/>.
;*

%include "x86/x86util.asm"

SECTION_TEXT

%macro emms_if_mmx 0
    %if mmsize == 8
        emms
    %endif
%endmacro


%macro COLOUR_REDUCTION 0
cglobal colour_reduction, 3, 3, 2, 0, frame, pixels, mask
    movd      xm1,  maskd
    SPLATD    m1

    .loop:
        movu  m0,      [frameq]

        ; Possibility to use "float" instructions here
        ;%if mmsize == 8
            pand  m0, m1
        ;%else
            ;andps m0, m1
        ;%endif

        movu [frameq],  m0

        add frameq,  mmsize
        sub pixelsd, mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
COLOUR_REDUCTION

INIT_XMM sse2
COLOUR_REDUCTION

INIT_YMM avx2
COLOUR_REDUCTION
