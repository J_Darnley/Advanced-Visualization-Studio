/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

#define IDC_GBR 1018
#define IDC_BRG 1019
#define IDC_RBG 1020
#define IDC_BGR 1021
#define IDC_GRB 1022
#define IDC_RGB 1183

void (*channel_shift)(int *, int, int) = NULL;

typedef struct {
    int mode;
    int on_beat;
} ChannelShiftContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    ChannelShiftContext *shift = ctx->priv;
    int pos = 0;
    R32(shift->mode);
    R32(shift->on_beat);
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    ChannelShiftContext *shift = ctx->priv;
    int modes[] = { IDC_RGB, IDC_RBG, IDC_GBR, IDC_GRB, IDC_BRG, IDC_BGR };

    if (is_beat && shift->on_beat)
        shift->mode = modes[rand() % 6];

    switch (shift->mode) {
        case IDC_RBG:
            if (channel_shift)
                channel_shift(frame, w*h, 0);
            else
                for (int i = 0; i < w*h; i++)
                    frame[i] = I3_TO_I(CR_TO_I(frame[i]),
                                       CB_TO_I(frame[i]),
                                       CG_TO_I(frame[i]));
            break;

        case IDC_GBR:
            if (channel_shift)
                channel_shift(frame, w*h, 1);
            else
                for (int i = 0; i < w*h; i++)
                    frame[i] = I3_TO_I(CG_TO_I(frame[i]),
                                       CB_TO_I(frame[i]),
                                       CR_TO_I(frame[i]));
            break;

        case IDC_GRB:
            if (channel_shift)
                channel_shift(frame, w*h, 2);
            else
                for (int i = 0; i < w*h; i++)
                    frame[i] = I3_TO_I(CG_TO_I(frame[i]),
                                       CR_TO_I(frame[i]),
                                       CB_TO_I(frame[i]));
            break;

        case IDC_BRG:
            if (channel_shift)
                channel_shift(frame, w*h, 3);
            else
                for (int i = 0; i < w*h; i++)
                    frame[i] = I3_TO_I(CB_TO_I(frame[i]),
                                       CR_TO_I(frame[i]),
                                       CG_TO_I(frame[i]));
            break;

        case IDC_BGR:
            if (channel_shift)
                channel_shift(frame, w*h, 4);
            else
                for (int i = 0; i < w*h; i++)
                    frame[i] = I3_TO_I(CB_TO_I(frame[i]),
                                       CG_TO_I(frame[i]),
                                       CR_TO_I(frame[i]));
            break;

        /* default and case IDC_RGB is to do nothing */
    }

    return 0;
}

Component t_channelshift = {
    .name = "Trans / Channel Shift",
    .signature = "Channel Shift",
    .priv_size = sizeof(ChannelShiftContext),
    .load_config = load_config,
    .render = render,
};
