/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

typedef struct {
    int enabled;
    int round_mode;
} BlurContext;

/* FIXME: enabled == 1 indicates medium blur. */

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    BlurContext *blur = ctx->priv;
    int pos = 0;
    R32(blur->enabled);
    R32(blur->round_mode);
    return 0;
}

static void print_config(ComponentContext *ctx, unused int indent)
{
    BlurContext *blur = ctx->priv;

    if (blur->enabled == 0)
        fprintf(stderr, "    No blur (0x%x)\n", blur->enabled);
    if (blur->enabled == 2)
        fprintf(stderr, "    Light blur (0x%x)\n", blur->enabled);
    if (blur->enabled == 1)
        fprintf(stderr, "    Medium blur (0x%x)\n", blur->enabled);
    if (blur->enabled == 3)
        fprintf(stderr, "    Heavy blur (0x%x)\n", blur->enabled);

    if (blur->round_mode)
        fprintf(stderr, "    Round up\n");
    else
        fprintf(stderr, "    Round down\n");
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, unused int is_beat)
{
    BlurContext *blur = ctx->priv;
    if (!blur->enabled)
        return 0;

#define MASK_SH1 (~((( 1 << 7)|( 1 << 15)|( 1 << 23)) << 1))
#define MASK_SH2 (~((( 3 << 6)|( 3 << 14)|( 3 << 22)) << 2))
#define MASK_SH3 (~((( 7 << 5)|( 7 << 13)|( 7 << 21)) << 3))
#define MASK_SH4 (~(((15 << 4)|(15 << 12)|(15 << 20)) << 4))

#define DIV_2(x)  (((x) & MASK_SH1) >> 1)
#define DIV_4(x)  (((x) & MASK_SH2) >> 2)
#define DIV_8(x)  (((x) & MASK_SH3) >> 3)
#define DIV_16(x) (((x) & MASK_SH4) >> 4)

    if (blur->enabled == 2) { /* Light blur */
        int adj_tl1 = 0;
        int adj_tl2 = 0;
        if (blur->round_mode) {
            adj_tl1 = 0x03030303;
            adj_tl2 = 0x04040404;
        }

        /* Top left */
        frame_out[0] = DIV_2(frame[0]) + DIV_4(frame[0]) + DIV_8(frame[1])
            + DIV_8(frame[w]) + adj_tl1;
        frame++;
        frame_out++;

        /* Top center */
        int x = w - 2;
        while (x--) {
            frame_out[0] = DIV_2(frame[0]) + DIV_8(frame[0]) + DIV_8(frame[1])
                + DIV_8(frame[-1]) + DIV_8(frame[w]) + adj_tl2;
            frame++;
            frame_out++;
        }

        /* Top right */
        frame_out[0] = DIV_2(frame[0]) + DIV_4(frame[0]) + DIV_8(frame[-1])
            + DIV_8(frame[w]) + adj_tl1;
        frame++;
        frame_out++;

        if (blur->round_mode) {
            adj_tl1 = 0x04040404;
            adj_tl2 = 0x05050505;
        }

        /* Middle block */
        int y = h - 2;
        while (y--) {
            /* Left edge */
            frame_out[0] = DIV_2(frame[0]) + DIV_8(frame[0]) + DIV_8(frame[1])
                + DIV_8(frame[w]) + DIV_8(frame[-w]) + adj_tl1;
            frame++;
            frame_out++;

            /* Middle */
            x = w - 2;
            while (x--) {
                frame_out[0] = DIV_2(frame[0]) + DIV_4(frame[0])
                    + DIV_16(frame[1]) + DIV_16(frame[-1]) + DIV_16(frame[w])
                    + DIV_16(frame[-w]) + adj_tl2;
                frame++;
                frame_out++;
            }

            /* Right edge */
            frame_out[0] = DIV_2(frame[0]) + DIV_8(frame[0]) + DIV_8(frame[-1])
                + DIV_8(frame[w]) + DIV_8(frame[-w]) + adj_tl1;
            frame++;
            frame_out++;
        }

        if (blur->round_mode) {
            adj_tl1 = 0x03030303;
            adj_tl2 = 0x04040404;
        }

        /* Bottom left */
        frame_out[0] = DIV_2(frame[0]) + DIV_4(frame[0]) + DIV_8(frame[1])
            + DIV_8(frame[-w]) + adj_tl1;
        frame++;
        frame_out++;

        /* Bottom center */
        x = w - 2;
        while (x--) {
            frame_out[0] = DIV_2(frame[0]) + DIV_8(frame[0]) + DIV_8(frame[1])
                + DIV_8(frame[-1]) + DIV_8(frame[-w]) + adj_tl2;
            frame++;
            frame_out++;
        }

        /* Bottom right */
        frame_out[0] = DIV_2(frame[0]) + DIV_4(frame[0]) + DIV_8(frame[-1])
            + DIV_8(frame[-w]) + adj_tl1;
        frame++;
        frame_out++;
    } else if (blur->enabled == 3) { /* Heavy blur */
        int adj_tl1 = 0;
        int adj_tl2 = 0;

        /* The order of these has been swapped to match above. */
        if (blur->round_mode) {
            adj_tl1 = 0x01010101;
            adj_tl2 = 0x02020202;
        }

        /* Top left */
        frame_out[0] = DIV_2(frame[1]) + DIV_2(frame[w]) + adj_tl1;
        frame++;
        frame_out++;

        /* Top center */
        int x = w - 2;
        while (x--) {
            frame_out[0] = DIV_4(frame[1]) + DIV_4(frame[-1]) + DIV_2(frame[w])
                + adj_tl2;
            frame++;
            frame_out++;
        }

        /* Top right */
        frame_out[0] = DIV_2(frame[-1]) + DIV_2(frame[w]) + adj_tl1;
        frame++;
        frame_out++;

        if (blur->round_mode) {
            adj_tl1 = 0x02020202;
            adj_tl2 = 0x03030303;
        }

        int y = h - 2;
        while (y--) {
            /* Left edge */
            frame_out[0] = DIV_2(frame[1]) + DIV_4(frame[w]) + DIV_4(frame[-w])
                + adj_tl1;

            /* Middle block */
            x = w - 2;
            while (x--) {
                frame_out[0] = DIV_4(frame[1]) + DIV_4(frame[-1])
                    + DIV_4(frame[w]) + DIV_4(frame[-w]) + adj_tl2;
                frame++;
                frame_out++;
            }

            /* Right edge */
            frame_out[0] = DIV_2(frame[-1]) + DIV_4(frame[w]) + DIV_4(frame[-w])
                + adj_tl1;
        }

        if (blur->round_mode) {
            adj_tl1 = 0x01010101;
            adj_tl2 = 0x02020202;
        }

        /* Bottom left */
        frame_out[0] = DIV_2(frame[1]) + DIV_2(frame[-w]) + adj_tl1;
        frame++;
        frame_out++;

        /* Bottom center */
        x = w - 2;
        while (x--) {
            frame_out[0] = DIV_4(frame[1]) + DIV_4(frame[-1]) + DIV_2(frame[-w])
                + adj_tl2;
            frame++;
            frame_out++;
        }

        /* Bottom right */
        frame_out[0] = DIV_2(frame[-1]) + DIV_2(frame[-w]) + adj_tl1;
        frame++;
        frame_out++;
    } else { /* Medium Blur */
        int adj_tl1 = 0;
        int adj_tl2 = 0;

        if (blur->round_mode) {
            adj_tl1 = 0x02020202;
            adj_tl2 = 0x03030303;
        }

        frame_out[0] = DIV_2(frame[0]) + DIV_4(frame[1]) + DIV_4(frame[w])
            + adj_tl1;
        frame++;
        frame_out++;

        int x = w - 2;
        while (x--) {
            frame_out[0] = DIV_4(frame[0]) + DIV_4(frame[1]) + DIV_4(frame[-1])
                + DIV_4(frame[w]) + adj_tl2;
            frame++;
            frame_out++;
        }

        frame_out[0] = DIV_2(frame[0]) + DIV_4(frame[-1]) + DIV_4(frame[w])
            + adj_tl1;
        frame++;
        frame_out++;

        if(blur->round_mode) {
            adj_tl1 = 0x03030303;
            adj_tl2 = 0x04040404;
        }

        int y = h - 2;
        while (y--) {
            frame_out[0] = DIV_4(frame[0]) + DIV_4(frame[1]) + DIV_4(frame[w])
                + DIV_4(frame[-w]) + adj_tl1;
            frame++;
            frame_out++;

            x = w - 2;
            while (x--) {
                frame_out[0] = DIV_2(frame[0]) + DIV_8(frame[1])
                    + DIV_8(frame[-1]) + DIV_8(frame[w]) + DIV_8(frame[-w])
                    + adj_tl2;
                frame++;
                frame_out++;
            }

            frame_out[0] = DIV_4(frame[0]) + DIV_4(frame[-1]) + DIV_4(frame[w])
                + DIV_4(frame[-w]) + adj_tl1;
            frame++;
            frame_out++;
        }

        if (blur->round_mode) {
            adj_tl1 = 0x02020202;
            adj_tl2 = 0x03030303;
        }

        frame_out[0] = DIV_2(frame[0]) + DIV_4(frame[1]) + DIV_4(frame[-w])
            + adj_tl1;
        frame++;
        frame_out++;

        x = w - 2;
        while (x--) {
            frame_out[0] = DIV_4(frame[0]) + DIV_4(frame[1]) + DIV_4(frame[-1])
                + DIV_4(frame[-w]) + adj_tl2;
            frame++;
            frame_out++;
        }

        frame_out[0] = DIV_2(frame[0]) + DIV_4(frame[-1]) + DIV_4(frame[-w])
            + adj_tl1;
        frame++;
        frame_out++;
    }

    return 1;
}

Component t_blur = {
    .name = "Trans / Blur",
    .code = 6,
    .priv_size = sizeof(BlurContext),
    .load_config = load_config,
    .print_config = print_config,
    .render = render,
};
