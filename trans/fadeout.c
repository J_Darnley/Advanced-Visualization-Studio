/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

typedef struct {
    int fade_length;
    int colour;
    uint8_t fade_table[3][256];
} FadeoutContext;

static void make_table(FadeoutContext *fctx)
{
    int r_seek = CR_TO_I(fctx->colour);
    int g_seek = CG_TO_I(fctx->colour);
    int b_seek = CB_TO_I(fctx->colour);
    for (int x = 0; x < 256; x++) {
        int r = x, g = x, b = x;

#define CALC_COLOUR(a) \
        if (a <= a##_seek - fctx->fade_length) \
            a += fctx->fade_length; \
        else if (a >= a##_seek + fctx->fade_length) \
            a -= fctx->fade_length; \
        else \
            a = a##_seek;

        CALC_COLOUR(r)
        CALC_COLOUR(g)
        CALC_COLOUR(b)

        /* The original code has the order of the colours wrong, either here or
         * below.  Blue is always (assumed) to be the lowest byte of colours.
         * That means on this little-endian arch, blue is the colour that comes
         * first.  The order has been changed here. */
        fctx->fade_table[0][x] = b;
        fctx->fade_table[1][x] = g;
        fctx->fade_table[2][x] = r;
    }
}

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    FadeoutContext *fctx = ctx->priv;
    int pos = 0;
    R32(fctx->fade_length);
    R32(fctx->colour);
    make_table(fctx);
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    FadeoutContext *fctx = ctx->priv;
    uint8_t *fb = (uint8_t*)frame;
    int x = w * h;

    if (!fctx->fade_length)
        return 0;

    while (x--) {
        fb[0] = fctx->fade_table[0][fb[0]];
        fb[1] = fctx->fade_table[1][fb[1]];
        fb[2] = fctx->fade_table[2][fb[2]];
        fb += 4;
    }

    /* TODO: MMX and SSE2 options, runtime selection of SIMD. */

    return 0;
}

Component t_fadeout = {
    .name = "Trans / Fadeout",
    .code = 3,
    .priv_size = sizeof(FadeoutContext),
    .load_config = load_config,
    .render = render,
};
