/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int num_particles;
    int mode;
    int *texture;
    uint8_t file[260];
    int tw, th;
} TexerContext;

enum {
    MODE_IGNORE = 1,
    MODE_REPLACE = 2,
    MODE_TEXTURE = 4,
    MODE_MASK = 8,
};

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    TexerContext *texer = ctx->priv;
    int pos = 0;

    /* The original code checks that the length is the same as its config. */
    if (buf_len != 288) {
        ERROR("malformed config\n");
        return -1;
    }

    /* An unidentified 16 bytes begin the config. */
    pos = 16;

    /* Next comes a fixed length (260 bytes) path string. */
    memcpy(texer->file, buf + pos, 260);
    pos += 260;

    /* Next we have a bitfield(?) representing the texture blending options.
     * The config window unsets and sets bits when a radio button is checked.
     * The modes appear to be as follows:
     * ignore + texture  = 0x5
     * ignore + mask     = 0x9
     * replace + texture = 0x6
     * replace + mask    = 0xA
     */
    R32(texer->mode);

    /* Next we have the maximum number of particles to draw. */
    R32(texer->num_particles);

    if (texer->mode & MODE_REPLACE)
        WARNING("Input mode Replace not yet supported\n");

    if (!texer->texture) {
        ERROR("No texture found.\n");
        ERROR("While this component does work when tested with a hard-coded"
                "texture, it needs to be able to use a suitable one based on"
                "what the preset requests.  This hasn't been implemented"
                "yet.\n");
        return -1;
    }

    return 0;
}

static void draw_texture(int *f, int fw, int fh, int *t, int tw, int th, int px, int py)
{
    int linesize = tw;

    px -= tw / 2;
    py -= th  /2;

    if (py < 0) {
        th += py;
        t -= py * linesize;
        py = 0;
    }
    if (py + th > fh)
        th += fh - (py + th);

    if (px < 0) {
        tw += px;
        t -= px;
        px = 0;
    }
    if (px + tw > fw)
        tw += fw - (px + tw);

    for (int y = 0; y < th; y++) {
        for (int x = 0; x < tw; x++) {
            int *p = f + (py + y) * fw + (px + x);
            *p = blend_pixel_add(*p, t[y*linesize+x]);
        }
    }
}

static void draw_mask(int *f, int fw, int fh, int *t, int tw, int th, int px, int py, int colour)
{
    int linesize = tw;

    px -= tw / 2;
    py -= th  /2;

    if (py < 0) {
        th += py;
        t -= py * linesize;
        py = 0;
    }
    if (py + th > fh)
        th += fh - (py + th);

    if (px < 0) {
        tw += px;
        t -= px;
        px = 0;
    }
    if (px + tw > fw)
        tw += fw - (px + tw);

    for (int y = 0; y < th; y++) {
        for (int x = 0; x < tw; x++) {
            int *p = f + (py + y) * fw + (px + x);
            *p = blend_pixel_add(*p, blend_pixel_mul(t[y*linesize+x], colour));
        }
    }
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, unused int is_beat)
{
    TexerContext *texer = ctx->priv;
    int p = texer->num_particles;

    if (texer->num_particles == 0)
        return 0;

    if (texer->mode & MODE_MASK) {
        for (int y = 0; y < h && p; y++)
            for (int x = 0; x < w && p; x++)
                if (frame[y*w+x]) {
                    draw_mask(frame_out, w, h,
                            texer->texture, texer->tw, texer->th,
                            x, y, frame[y*w+x]);
                    p--;
                }
    } else {
        for (int y = 0; y < h && p; y++)
            for (int x = 0; x < w && p; x++)
                if (frame[y*w+x]) {
                    draw_texture(frame_out, w, h,
                            texer->texture, texer->tw, texer->th,
                            x, y);
                    p--;
                }
    }

    return 1;
}

Component t_texer = {
    .name = "Trans / Texer",
    .signature = "Texer",
    .priv_size = sizeof(TexerContext),
    .load_config = load_config,
    .render = render,
};
