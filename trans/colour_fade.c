/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

typedef struct {
    int ft[4][3];
    int enabled;
    int faders[3];
    int beat_faders[3];
    int fader_pos[3];
    uint8_t c_tab[512][512];
    uint8_t clip[256 + 40 + 40];
} ColourFadeContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    ColourFadeContext *cf = ctx->priv;
    int pos = 0;
    R32(cf->enabled);
    R32(cf->faders[0]);
    R32(cf->faders[1]);
    R32(cf->faders[2]);

    cf->beat_faders[0] = cf->faders[0];
    cf->beat_faders[1] = cf->faders[1];
    cf->beat_faders[2] = cf->faders[2];

    R32(cf->beat_faders[0]);
    R32(cf->beat_faders[1]);
    R32(cf->beat_faders[2]);

    cf->fader_pos[0] = cf->faders[0];
    cf->fader_pos[1] = cf->faders[1];
    cf->fader_pos[2] = cf->faders[2];

    /* TODO: improve formatting */
    for (int x = 0; x < 512; x++)
        for (int y = 0; y < 512; y++) {
            int xp = x - 255;
            int yp = y - 255;
            if (xp > 0               /* g-b > 0,   or g > b */
                    && xp > -yp)     /* g-b > r-b, or g > r */
                cf->c_tab[x][y] = 0;
            else if (yp < 0          /* b-r < 0,   or r > b */
                    && xp < -yp)     /* g-b < r-b, or g < r */
                cf->c_tab[x][y] = 1;
            else if (xp < 0 && yp > 0)
                cf->c_tab[x][y] = 2;
            else
                cf->c_tab[x][y] = 3;
        }

    for (int x = 0; x < 256 + 40 + 40; x++)
        cf->clip[x] = CLIP(x - 40, 0, 255);

    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    ColourFadeContext *cf = ctx->priv;

    if (!cf->enabled)
        return 0;

    if (cf->fader_pos[0] < cf->faders[0])
        cf->fader_pos[0]++;
    if (cf->fader_pos[1] < cf->faders[2])
        cf->fader_pos[1]++;
    if (cf->fader_pos[2] < cf->faders[1])
        cf->fader_pos[2]++;

    if (cf->fader_pos[0] > cf->faders[0])
        cf->fader_pos[0]--;
    if (cf->fader_pos[1] > cf->faders[2])
        cf->fader_pos[1]--;
    if (cf->fader_pos[2] > cf->faders[1])
        cf->fader_pos[2]--;

    if (!(cf->enabled & 4)) {
        cf->fader_pos[0] = cf->faders[0];
        cf->fader_pos[1] = cf->faders[1];
        cf->fader_pos[2] = cf->faders[2];
    } else if (is_beat && (cf->enabled & 2)) {
        cf->fader_pos[0] = rand() % 32 - 6;
        cf->fader_pos[1] = rand() % 64 - 32;
        cf->fader_pos[2] = rand() % 32 - 6;
        if (cf->fader_pos[1] < 0 && cf->fader_pos[1] > -16)
            cf->fader_pos[1] -= 32;
        if (cf->fader_pos[1] >= 0 && cf->fader_pos[1] < 16)
            cf->fader_pos[1] = 32;
    } else if (is_beat) {
        cf->fader_pos[0] = cf->beat_faders[0];
        cf->fader_pos[1] = cf->beat_faders[1];
        cf->fader_pos[2] = cf->beat_faders[2];
    }

    int fs1 = cf->fader_pos[0];
    int fs2 = cf->fader_pos[1];
    int fs3 = cf->fader_pos[2];

    cf->ft[0][0] = fs3;
    cf->ft[0][1] = fs2;
    cf->ft[0][2] = fs1;

    cf->ft[1][0] = fs2;
    cf->ft[1][1] = fs1;
    cf->ft[1][2] = fs3;

    cf->ft[2][0] = fs1;
    cf->ft[2][1] = fs3;
    cf->ft[2][2] = fs2;

    cf->ft[3][0] = fs3;
    cf->ft[3][1] = fs3;
    cf->ft[3][2] = fs3;

    uint8_t *q = (uint8_t *)frame;
    uint8_t *ctab = cf->c_tab[0] + 255 + (255 << 9);
    uint8_t *clip = cf->clip + 40;

    int x = w * h;
    /* Do not unroll this loop and do 2 pixels per iteration. */
    while (x--) {
        int r = q[0];
        int g = q[1];
        int b = q[2];
        int i = ((g - b) << 9) + b - r;
        int p = ctab[i];
        q[0] = clip[r + cf->ft[p][0]];
        q[1] = clip[g + cf->ft[p][1]];
        q[2] = clip[b + cf->ft[p][2]];
        q += 4;
    }

    return 0;
}

Component t_colourfade = {
    .name = "Trans / Colour Fade",
    .code = 11,
    .priv_size = sizeof(ColourFadeContext),
    .load_config = load_config,
    .render = render,
};
