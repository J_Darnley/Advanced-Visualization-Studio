/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "expressions.h"
#include "pixel.h"

enum new_effect_modes {
    N_EFFECT_BLEND_ADD = 1,
    N_EFFECT_BLEND_AVG,
    N_EFFECT_BLEND_REP,
};

typedef struct {
    int enabled;
    int depth1, depth2;
    int on_beat;
    int dur_frames;
    int this_depth;
    int blend, blend_avg;
    int nf;
    void *expr_context;
    const char *expr_init;
    const char *expr_frame;
    const char *expr_beat;
    int inited;
    int invert;
    int show_light;
    int old_style;
    int buffer_n;
    int new_effect;
} BumpContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    BumpContext *bump = ctx->priv;
    int pos = 0;

    void *expr_ctx = expr_init(ctx->actx);
    if (!expr_ctx)
        return -1;

    R32(bump->enabled);
    R32(bump->on_beat);
    R32(bump->dur_frames);
    R32(bump->depth1);
    R32(bump->depth2);
    R32(bump->blend);
    R32(bump->blend_avg);

    int ret = expr_load_rstring(expr_ctx, &bump->expr_frame, buf + pos, buf_len - pos);
    if (ret < 0)
        return -1;
    pos += ret;

    ret = expr_load_rstring(expr_ctx, &bump->expr_beat, buf + pos, buf_len - pos);
    if (ret < 0)
        return -1;
    pos += ret;

    ret = expr_load_rstring(expr_ctx, &bump->expr_init, buf + pos, buf_len - pos);
    if (ret < 0)
        return -1;
    pos += ret;

    R32(bump->show_light);
    R32(bump->invert);
    R32(bump->old_style);
    R32(bump->buffer_n);

    if (expr_compile(expr_ctx, "__avs_internal_init", bump->expr_init)
            || expr_compile(expr_ctx, "__avs_internal_frame", bump->expr_frame)
            || expr_compile(expr_ctx, "__avs_internal_beat", bump->expr_beat))
        return -1;

    bump->expr_context = expr_ctx;

    return 0;
}

static inline int depth_of(int c, int i)
{
    int r = MAX3(CR_TO_I(c), CG_TO_I(c), CB_TO_I(c));
    return i ? 255 - r : r;
}

static inline int set_depth(int l, int c)
{
    int r;
    r  = MIN((c & 0xff)     + l,         254);
    r |= MIN((c & 0xff00)   + (l << 8),  254 << 8);
    r |= MIN((c & 0xff0000) + (l << 16), 254 << 16);
    return r;
}

static inline int set_depth_0(int c)
{
    int r;
    r  = MIN(c & 0xff,     254);
    r |= MIN(c & 0xff00,   254 << 8);
    r |= MIN(c & 0xff0000, 254 << 16);
    return r;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    BumpContext *bump = ctx->priv;

    if (!bump->enabled)
        return 0;

    /* No need to compile. */

    int *depth_buffer;

    if (bump->buffer_n) {
        depth_buffer = ctx->actx->global_buffer[bump->buffer_n-1];
        if (!depth_buffer)
            depth_buffer = calloc(w * h, sizeof(int));
        if (!depth_buffer)
            return -1;
        ctx->actx->global_buffer[bump->buffer_n-1] = depth_buffer;
    } else
        depth_buffer = frame;

    int cur_buf = depth_buffer == frame;

#define SET(a,b) expr_variable_set(bump->expr_context, (a), (b))
#define GET(a) expr_variable_get(bump->expr_context, (a))

    if (!bump->inited) {
        SET("bi", 1.0);
        if (expr_execute(bump->expr_context, "__avs_internal_init", bump->expr_init))
            return -1;
        bump->inited = 1;
    }

    if (expr_execute(bump->expr_context, "__avs_internal_frame", bump->expr_frame))
        return -1;

    if (is_beat)
        if (expr_execute(bump->expr_context, "__avs_internal_init", bump->expr_init))
            return -1;

    if (is_beat)
        SET("isbeat", -1);
    else
        SET("isbeat", 1);

    if (bump->nf)
        SET("islbeat", -1);
    else
        SET("islbeat", 1);

    if (bump->on_beat && is_beat) {
        bump->this_depth = bump->depth2;
        bump->nf = bump->dur_frames;
    } else if (!bump->nf)
        bump->this_depth = bump->depth1;

    memset(frame_out, 0, w * h * sizeof(int));

    int cx, cy;
    if (bump->old_style) {
        cx = GET("x") / 100.0 * w;
        cy = GET("y") / 100.0 * h;
    } else {
        cx = GET("x") * w;
        cy = GET("y") * h;
    }

    cx = CLIP(cx, 0, w);
    cy = CLIP(cy, 0, h);

    if (bump->show_light)
        frame_out[cy * w + cx] = 0xffffff;

    double bi = GET("bi");
    bi = CLIP(bi, 0, 1);
    bump->this_depth *= bi;

    int this_depth_scaled = bi * 256.0 / 100.0;
    depth_buffer += w+1;
    frame += w+1;
    frame_out += w+1;

    int invert = bump->invert;

    /* TODO: change to a for loop and use array indexes. */
    int ly = 1 - cy;
    int i = h-2;
    while (i--) {
        int j = w-2;
        int lx = 1 - cx;

        /* TODO: Change the if-else statements for a switch. */
        if (bump->blend) {
            while (j--) {
                int m1 = depth_buffer[-1];
                int p1 = depth_buffer[ 1];
                int mw = depth_buffer[-w];
                int pw = depth_buffer[ w];

                if (!cur_buf || (cur_buf && (m1||p1||mw||pw))) {
                    int coul1 = depth_of(p1, invert) - depth_of(m1, invert) - lx;
                    int coul2 = depth_of(pw, invert) - depth_of(mw, invert) - ly;
                    coul1 = 127 - abs(coul1);
                    coul2 = 127 - abs(coul2);

                    if (coul1 <=0 || coul2 <= 0)
                        coul1 = set_depth_0(frame[0]);
                    else
                        coul1 = set_depth((coul1 * coul2 * this_depth_scaled) >> (8+6), frame[0]);

                    frame_out[0] = blend_pixel_add(frame[0], coul1);
                }
                depth_buffer++;
                frame++;
                frame_out++;
                lx++;
            }
        } else if (bump->blend_avg) {
            while (j--) {
                int m1 = depth_buffer[-1];
                int p1 = depth_buffer[ 1];
                int mw = depth_buffer[-w];
                int pw = depth_buffer[ w];

                if (!cur_buf || (cur_buf && (m1||p1||mw||pw))) {
                    int coul1 = depth_of(p1, invert) - depth_of(m1, invert) - lx;
                    int coul2 = depth_of(pw, invert) - depth_of(mw, invert) - ly;
                    coul1 = 127 - abs(coul1);
                    coul2 = 127 - abs(coul2);

                    if (coul1 <=0 || coul2 <= 0)
                        coul1 = set_depth_0(frame[0]);
                    else
                        coul1 = set_depth((coul1 * coul2 * this_depth_scaled) >> (8+6), frame[0]);

                    frame_out[0] = blend_pixel_avg(frame[0], coul1);
                }
                depth_buffer++;
                frame++;
                frame_out++;
                lx++;
            }
        } else {
            while (j--) {
                int m1 = depth_buffer[-1];
                int p1 = depth_buffer[ 1];
                int mw = depth_buffer[-w];
                int pw = depth_buffer[ w];

                if (!cur_buf || (cur_buf && (m1||p1||mw||pw))) {
                    int coul1 = depth_of(p1, invert) - depth_of(m1, invert) - lx;
                    int coul2 = depth_of(pw, invert) - depth_of(mw, invert) - ly;
                    coul1 = 127 - abs(coul1);
                    coul2 = 127 - abs(coul2);

                    if (coul1 <=0 || coul2 <= 0)
                        coul1 = set_depth_0(frame[0]);
                    else
                        coul1 = set_depth((coul1 * coul2 * this_depth_scaled) >> (8+6), frame[0]);

                    frame_out[0] = coul1;
                }
                depth_buffer++;
                frame++;
                frame_out++;
                lx++;
            }
        }

        depth_buffer += 2;
        frame += 2;
        frame_out += 2;
        ly++;
    }

    if (bump->nf) {
        bump->nf--;
        if (bump->nf) {
            int a = abs(bump->depth1 - bump->depth2) / bump->dur_frames;
            bump->this_depth += a * (bump->depth2 > bump->depth1 ? -1 : 1);
        }
    }

    return 1;
}

static void uninit(ComponentContext *ctx)
{
    BumpContext *bump = ctx->priv;

    FREE((void *)bump->expr_init);
    FREE((void *)bump->expr_frame);
    FREE((void *)bump->expr_beat);

    expr_uninit(bump->expr_context);
}

Component t_bump = {
    .name = "Trans / Bump",
    .code = 29,
    .priv_size = sizeof(BumpContext),
    .load_config = load_config,
    .render = render,
    .uninit = uninit,
};
