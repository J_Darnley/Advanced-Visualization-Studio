/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

typedef struct {
    int enabled;
    int *buffer[2];
    int buffer_w, buffer_h;
    int page;
    int density;
    int depth;
    int random_drop;
    int drop_pos_x, drop_pos_y;
    int drop_radius;
    int method;
} WaterBumpContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    WaterBumpContext *water = ctx->priv;
    int pos = 0;
    R32(water->enabled);
    R32(water->density);
    R32(water->depth);
    R32(water->random_drop);
    R32(water->drop_pos_x);
    R32(water->drop_pos_y);
    R32(water->drop_radius);
    R32(water->method);
    return 0;
}

static void sine_blob(int *buffer, int x, int y, int radius, int depth, int w, int h)
{
    float length = (1024.0 / radius) * (1024.0 / radius);

    if (x < 0)
        x = 1 + radius + rand() % (w - 2 * radius - 1);
    if (y < 0)
        y = 1 + radius + rand() % (h - 2 * radius - 1);

    int left = -radius;
    int right = radius;
    int top = -radius;
    int bottom = radius;

    if (x - radius < 1)
        left -= x - radius - 1;
    if (y - radius < 1)
        top -= y - radius - 1;
    if (w + radius > w - 1)
        right -= x + radius - w + 1;
    if (h + radius > h - 1)
        bottom -= y + radius - h + 1;

    for (int cy = top; cy < bottom; cy++) {
        for (int cx = left; cx < right; cx++) {
            int square = cy * cy + cx * cx;

            if (square < radius * radius) {
                float dist = sqrtf(square * length);
                buffer[w * (cy + y) + cx + x] += (int)((cosf(dist) + 65535) * depth) >> 19;
            }
        }
    }
}

static void calc_water(WaterBumpContext *water, int w, int h)
{
    int *new = water->buffer[!water->page];
    int *old = water->buffer[water->page];

    for (int count = w+1; count < (h-1) * w; count += 2) {
        for (int x = count + w-2; count < x; count++) {
            int newh = ((  old[count + w]     + old[count - w]
                         + old[count + 1]     + old[count - 1]
                         + old[count - w - 1] + old[count - w + 1]
                         + old[count + w - 1] + old[count + w + 1]
                        ) >> 2) - new[count];
            new[count] = newh - (newh >> water->density);
        }
    }
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    WaterBumpContext *water = ctx->priv;

    if (!water->enabled)
        return 0;

    if (water->buffer_w != w || water->buffer_h != h) {
        if (water->buffer[0])
            free(water->buffer[0]);
        if (water->buffer[1])
            free(water->buffer[1]);

        water->buffer[0] = NULL;
        water->buffer[1] = NULL;
    }

    if (!water->buffer[0]) {
        water->buffer[0] = calloc(w * h, sizeof(int));
        water->buffer[1] = calloc(w * h, sizeof(int));

        if (!water->buffer[0] || !water->buffer[1])
            return -1;

        water->buffer_w = w;
        water->buffer_h = h;
    }

    if (is_beat) {
        if (water->random_drop) {
            int max = MAX(w, h);
            sine_blob(water->buffer[water->page], -1, -1, (water->drop_radius * max) / 100, -water->depth, w, h);
        } else {
            int x = (w * (water->drop_pos_x + 1)) / 4;
            int y = (h * (water->drop_pos_y + 1)) / 4;
            sine_blob(water->buffer[water->page], x, y, water->drop_radius, -water->depth, w, h);
        }
        /* height blob */
    }

    int offset = w+1;
    int *ptr = water->buffer[water->page];

    for (int y = (h-1) * w; offset < y; offset += 2) {
        for (int x = offset + w-2; offset < x; offset++) {
            int dx = ptr[offset] - ptr[offset+1];
            int dy = ptr[offset] - ptr[offset+w];
            int ofs = offset + w * (dy >> 3) + (dx >> 3);

            if (ofs < w*h && ofs > -1)
                frame_out[offset] = frame[ofs];
            else
                frame_out[offset] = frame[offset];

            offset++;
            dx = ptr[offset] - ptr[offset+1];
            dy = ptr[offset] - ptr[offset+w];
            ofs = offset + w * (dy >> 3) + (dx >> 3);

            if (ofs < w*h && ofs > -1)
                frame_out[offset] = frame[ofs];
            else
                frame_out[offset] = frame[offset];
        }
    }

    calc_water(water, w, h);

    water->page = !water->page;

    return 1;
}

static void uninit(ComponentContext *ctx)
{
    WaterBumpContext *water = ctx->priv;

    FREE(water->buffer[0]);
    FREE(water->buffer[1]);
}

Component t_waterbump = {
    .name = "Trans / Water Bump",
    .code = 31,
    .priv_size = sizeof(WaterBumpContext),
    .load_config = load_config,
    .render = render,
    .uninit = uninit,
};
