/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int enabled;
    int quality1, quality2;
    int blend, blend_avg;
    int on_beat;
    int dur_frames;
    int nf;
    int this_quality;
    int new_effect;
} MosaicContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    MosaicContext *mosaic = ctx->priv;
    int pos = 0;

    R32(mosaic->enabled);
    R32(mosaic->quality1);
    R32(mosaic->quality2);
    R32(mosaic->blend);
    R32(mosaic->blend_avg);
    R32(mosaic->on_beat);
    R32(mosaic->dur_frames);

    return 0;
}

static void print_config(ComponentContext *ctx, unused int indent)
{
    MosaicContext *mosaic = ctx->priv;

#define PRINT_OPTION(var) \
    fprintf(stderr, "    " #var " = %d\n", mosaic->var)

    PRINT_OPTION(enabled);
    PRINT_OPTION(quality1);
    PRINT_OPTION(quality2);
    PRINT_OPTION(blend);
    PRINT_OPTION(blend_avg);
    PRINT_OPTION(on_beat);
    PRINT_OPTION(dur_frames);
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    MosaicContext *mosaic = ctx->priv;

    if (!mosaic->enabled)
        return 0;

    if (mosaic->on_beat && is_beat) {
        mosaic->this_quality = mosaic->quality2;
        mosaic->nf = mosaic->dur_frames;
    } else if (!mosaic->nf)
        mosaic->this_quality = mosaic->quality1;

    /* No change wanted for quality == 100.  Greater than for safety.
     * This condition is swapped compared to the original. */
    if (mosaic->this_quality >= 100)
        return 0;

    if (h <= 100 || w <= 100) {
        ERROR("this frame size (%dx%d) is not supported\n", w, h);
        return -1;
    }

    int sx_inc = (w * 65536) / mosaic->this_quality;
    int sy_inc = (h * 65536) / mosaic->this_quality;
    int y_pos = sy_inc >> 17;

    for (int y = 0, dy_pos = 0; y < h; y++) {
        int x_pos = sx_inc >> 17;

        if (mosaic->blend) {
            for (int x = 0, dx_pos = 0; x < w; x++) {
                frame_out[y*w+x] = blend_pixel_add(frame_out[y*w+x], frame[y_pos * w + x_pos]);
                dx_pos += 65536;
                if (dx_pos >= sx_inc) {
                    x_pos += dx_pos >> 16;
                    dx_pos -= sx_inc;
                }
            }
        } else if (mosaic->blend_avg) {
            for (int x = 0, dx_pos = 0; x < w; x++) {
                frame_out[y*w+x] = blend_pixel_avg(frame_out[y*w+x], frame[y_pos * w + x_pos]);
                dx_pos += 65536;
                if (dx_pos >= sx_inc) {
                    x_pos += dx_pos >> 16;
                    dx_pos -= sx_inc;
                }
            }
        } else {
            for (int x = 0, dx_pos = 0; x < w; x++) {
                frame_out[y*w+x] = frame[y_pos * w + x_pos];
                dx_pos += 65536;
                if (dx_pos >= sx_inc) {
                    x_pos += dx_pos >> 16;
                    dx_pos -= sx_inc;
                }
            }
        }

        dy_pos += 65536;
        if (dy_pos >= sy_inc) {
            y_pos += dy_pos >> 16;
            dy_pos -= sy_inc;
        }
    }

    if (mosaic->nf) {
        mosaic->nf--;
        if ( mosaic->nf) {
            int a = abs(mosaic->quality1 - mosaic->quality2) / mosaic->dur_frames;
            mosaic->this_quality += a * (mosaic->quality2 > mosaic->quality1 ? -1 : 1);
        }
    }

    return 1;
}

Component t_mosaic = {
    .name = "Trans / Mosaic",
    .code = 30,
    .priv_size = sizeof(MosaicContext),
    .load_config = load_config,
    .print_config = print_config,
    .render = render,
};
