/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

void (*colour_reduction)(int *, int, int) = NULL;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    /* For some crazy reason the original code has an unused 206 char array
     * (MAX_PATH) in its config.  So skip that. */
    int pos = 260;

    /* The config of the whole filter is just one integer.  It is the log2 of
     * the number of levels set in the config window. */
    pi_union levels;
    R32(levels.i);
    ctx->priv = levels.p;
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    static const int masks[8] = { 0x808080, 0xc0c0c0, 0xe0e0e0, 0xf0f0f0,
                                 0xf8f8f8, 0xfcfcfc, 0xfefefe, 0xffffff };
    pi_union levels = { .p = ctx->priv };
    levels.i -= 1;
    int mask = masks[levels.i];

    /* When the code is supposed to reduce to 256 levels that is the same as
     * doing nothing but it is quicker to do nothing. */
    if (levels.i == 7)
        return 0;

    if (colour_reduction)
        colour_reduction(frame, w*h, mask);

    for (int i = 0; i < w*h; i++)
        frame[i] = frame[i] & mask;

    return 0;
}

Component t_colourreduction = {
    .name = "Trans / Colour Reduction",
    .signature = "Color Reduction",
    .priv_size = 0,
    .load_config = load_config,
    .render = render,
};
