/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int enabled;
    int red_p, green_p, blue_p;
    int blend, blend_avg;
    int dissoc;
    int colour;
    int exclude;
    int distance;
    int tabs_need_init;
    int red_tab[256];
    int green_tab[256];
    int blue_tab[256];
} BrightnessContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    BrightnessContext *bctx = ctx->priv;
    int pos = 0;
    R32(bctx->enabled);
    R32(bctx->blend);
    R32(bctx->blend_avg);
    R32(bctx->red_p);
    R32(bctx->green_p);
    R32(bctx->blue_p);
    R32(bctx->dissoc);
    R32(bctx->colour);
    R32(bctx->exclude);
    R32(bctx->distance);
    bctx->tabs_need_init = 1;
    return 0;
}

static int in_range(int colour, int ref, int distance)
{
    if (abs((colour & 0xff) - (ref & 0xff)) > distance)
        return 0;
    if (abs((colour & 0xff00) - (ref & 0xff00)) > (distance << 8))
        return 0;
    if (abs((colour & 0xff0000) - (ref & 0xff0000)) > (distance << 16))
        return 0;
    return 1;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    BrightnessContext *bctx = ctx->priv;

    if (!bctx->enabled)
        return 0;

    int rm = (1 + (bctx->red_p < 0 ? 1 : 16) * (bctx->red_p / 4096.0)) * 65536.0;
    int gm = (1 + (bctx->green_p < 0 ? 1 : 16) * (bctx->green_p / 4096.0)) * 65536.0;
    int bm = (1 + (bctx->blue_p < 0 ? 1 : 16) * (bctx->blue_p / 4096.0)) * 65536.0;

    if (bctx->tabs_need_init) {
        for (int n = 0; n < 256; n++) {
            bctx->red_tab[n] = (n * rm) & 0xffff0000;
            bctx->red_tab[n] = CLIP(bctx->red_tab[n], 0, 0xff0000);
            bctx->green_tab[n] = ((n * gm) >> 8) & 0xffff00;
            bctx->green_tab[n] = CLIP(bctx->green_tab[n], 0, 0xff00);
            bctx->blue_tab[n] = ((n * bm) >> 16) & 0xffff;
            bctx->blue_tab[n] = CLIP(bctx->blue_tab[n], 0, 0xff);
        }
        bctx->tabs_need_init = 0;
    }

    if (bctx->blend) {
        if (bctx->exclude) {
            for (int l = w * h; l; l--) {
                int pix = *frame;
                if (in_range(pix, bctx->colour, bctx->distance))
                    *frame = blend_pixel_add(pix, bctx->red_tab[CR_TO_I(pix)]|
                            bctx->green_tab[CG_TO_I(pix)]|
                            bctx->blue_tab[CB_TO_I(pix)]);
                frame++;
            }
        } else {
            for (int l = w * h; l; l--) {
                int pix = *frame;
                *frame = blend_pixel_add(pix, bctx->red_tab[CR_TO_I(pix)] |
                        bctx->green_tab[CG_TO_I(pix)] |
                        bctx->blue_tab[CB_TO_I(pix)]);
                frame++;
            }
        }
    } else if (bctx->blend_avg) {
        if (bctx->exclude) {
            for (int l = w * h; l; l--) {
                int pix = *frame;
                if (in_range(pix, bctx->colour, bctx->distance))
                    *frame = blend_pixel_avg(pix, bctx->red_tab[CR_TO_I(pix)]|
                            bctx->green_tab[CG_TO_I(pix)]|
                            bctx->blue_tab[CB_TO_I(pix)]);
                frame++;
            }
        } else {
            for (int l = w * h; l; l--) {
                int pix = *frame;
                *frame = blend_pixel_avg(pix, bctx->red_tab[CR_TO_I(pix)]|
                        bctx->green_tab[CG_TO_I(pix)]|
                        bctx->blue_tab[CB_TO_I(pix)]);
                frame++;
            }
        }
    } else {
        if (bctx->exclude) {
            for (int l = w * h; l; l--) {
                int pix = *frame;
                if (in_range(pix, bctx->colour, bctx->distance))
                    *frame = bctx->red_tab[CR_TO_I(pix)]|
                        bctx->green_tab[CG_TO_I(pix)]|
                        bctx->blue_tab[CB_TO_I(pix)];
                frame++;
            }
        } else {
            for (int l = w * h; l; l--) {
                int pix = *frame;
                *frame = bctx->red_tab[CR_TO_I(pix)]|
                    bctx->green_tab[CG_TO_I(pix)]|
                    bctx->blue_tab[CB_TO_I(pix)];
                frame++;
            }
        }
    }
    return 0;
}

Component t_brightness = {
    .name = "Trans / Brightness",
    .code = 22,
    .priv_size = sizeof(BrightnessContext),
    .load_config = load_config,
    .render = render,
};
