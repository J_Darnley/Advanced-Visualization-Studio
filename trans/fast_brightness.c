/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

void (*fast_brightness_mul2)(int *frame, int pixels) = NULL;
void (*fast_brightness_div2)(int *frame, int pixels) = NULL;

typedef struct {
    int tab[3][256];
    int direction;
} FastBrightnessContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    FastBrightnessContext *fast = ctx->priv;
    int pos = 0, i = 0;
    R32(fast->direction);

    for (/*do nothing*/; i < 128; i++) {
        fast->tab[0][i] = i+i;
        fast->tab[1][i] = i << 9;
        fast->tab[2][i] = i << 17;
    }
    for (/*do nothing*/; i < 256; i++) {
        fast->tab[0][i] = 255;
        fast->tab[1][i] = 255 << 8;
        fast->tab[2][i] = 255 << 16;
    }

    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    FastBrightnessContext *fast = ctx->priv;

    /* Brighten by 2.0x */
    if (fast->direction == 0) {
        if (fast_brightness_mul2)
            fast_brightness_mul2(frame, w*h);
        else
            for (int i = 0; i < w*h; i++)
                frame[i] = fast->tab[0][CB_TO_I(frame[i])]
                    | fast->tab[1][CG_TO_I(frame[i])]
                    | fast->tab[2][CR_TO_I(frame[i])];

    /* Brightnen by 0.5x */
    } else if (fast->direction == 1) {
        if (fast_brightness_div2)
            fast_brightness_div2(frame, w*h);
        else
            for (int i = 0; i < w*h; i++)
                frame[i] = (frame[i] >> 1) & 0x7f7f7f;
    }

    /* else do nothing */

    return 0;
}

Component t_fastbrightness = {
    .name = "Trans / Fast Brightness",
    .code = 44,
    .priv_size = sizeof(FastBrightnessContext),
    .load_config = load_config,
    .render = render,
};
