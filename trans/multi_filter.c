/*
 * Copyright (c) 2016 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

typedef struct {
    int enabled;
    int type;
    int beat_toggle;
    int interpolate;
    int reactivate_alpha_blend;
} MultiFilterContext;

static int load_config(ComponentContext *ctx, unused const uint8_t *buf, unused int buf_len)
{
    int pos = 0;
    MultiFilterContext *mf = ctx->priv;

    R32(mf->enabled);
    R32(mf->type);
    R32(mf->beat_toggle);

    /* This is unable to be set through the original ape dialog. */
    R32(mf->interpolate);

    if (mf->type == 3) {
        ERROR("Filter type \"Infinite Root Multiplier + Small Border Convolution\" is not supported\n");
        return -1;
    }

    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    MultiFilterContext *mf = ctx->priv;

    if (is_beat)
        mf->enabled = !mf->enabled;

    if (!mf->enabled)
        return 0;

    /* single chrome */
    if (mf->type == 0)
        for (int y = 0; y < h; y++)
            for (int x = 0; x < w; x++) {
                int r = CR_TO_I(frame[y*w+x]);
                int g = CG_TO_I(frame[y*w+x]);
                int b = CB_TO_I(frame[y*w+x]);

                /* No macro used to show what should be done. */
                int r2 = r * 2;
                int g2 = g * 2;
                int b2 = b * 2;

                r2 = CLIP(r2, 0, 255);
                g2 = CLIP(g2, 0, 255);
                b2 = CLIP(b2, 0, 255);

                r2 -= r;
                g2 -= g;
                b2 -= b;

                r2 += r2;
                g2 += g2;
                b2 += b2;

                frame[y*w+x] = I3_TO_I(r2, g2, b2);
            }

#define ONE_CHROME_OPERATION \
    r2 = r * 2; g2 = g * 2; b2 = b * 2; \
    r2 = CLIP(r2, 0, 255); g2 = CLIP(g2, 0, 255); b2 = CLIP(b2, 0, 255); \
    r2 -= r; g2 -= g; b2 -= b; \
    r2 += r2; g2 += g2; b2 += b2; \
    r = r2; g = g2; b = b2;

    /* double chrome */
    else if (mf->type == 1)
        for (int y = 0; y < h; y++)
            for (int x = 0; x < w; x++) {
                int r = CR_TO_I(frame[y*w+x]);
                int g = CG_TO_I(frame[y*w+x]);
                int b = CB_TO_I(frame[y*w+x]);
                int r2, g2, b2;

                ONE_CHROME_OPERATION
                ONE_CHROME_OPERATION

                frame[y*w+x] = I3_TO_I(r, g, b);
            }

    /* triple chrome */
    else if (mf->type == 2)
        for (int y = 0; y < h; y++)
            for (int x = 0; x < w; x++) {
                int r = CR_TO_I(frame[y*w+x]);
                int g = CG_TO_I(frame[y*w+x]);
                int b = CB_TO_I(frame[y*w+x]);
                int r2, g2, b2;

                ONE_CHROME_OPERATION
                ONE_CHROME_OPERATION
                ONE_CHROME_OPERATION

                frame[y*w+x] = I3_TO_I(r, g, b);
            }

    return 0;
}

Component t_multifilter = {
    .name = "Trans / Multi Filter",
    .signature = "Jheriko : MULTIFILTER",
    .priv_size = sizeof(MultiFilterContext),
    .load_config = load_config,
    .render = render,
};
