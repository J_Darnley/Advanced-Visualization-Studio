/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int scale;
    int scale2;
    int blend;
    int beatch;
    int fpos;
    int subpixel;
} BlitterFBContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    BlitterFBContext *bctx = ctx->priv;
    int pos = 0;
    R32(bctx->scale);
    R32(bctx->scale2);
    R32(bctx->blend);
    R32(bctx->beatch);
    R32(bctx->subpixel);
    bctx->fpos = bctx->scale;
    return 0;
}

static int blitter_out(int *frame, int *frame_out, int w, int h, int f_val, int blend)
{
    const int adj = 7;
    int ds_x = (f_val + (1 << adj) - 32) << (16 - adj);
    int x_len = ((w << 16) / ds_x) & ~3;
    int y_len = (h << 16) / ds_x;

    if (x_len >= w || y_len >= h)
        return 0;

    int start_x = (w - x_len) / 2;
    int start_y = (h - y_len) / 2;
    int s_y = 32768;

    /* Are these correct? */
    int *dst = frame + start_y * w + start_x;
    int *src  = frame_out + start_y * w + start_x;

    frame_out += start_y * w;
    for (int y = 0; y < y_len; y++) {
        int s_x = 32768;
        int *src = frame + (s_y >> 16) * w;
        int *old_dest = frame_out + start_x;
        s_y += ds_x;

        if (!blend) {
            int x = x_len / 4;
            while (x--) {
                old_dest[0] = src[s_x >> 16];
                s_x += ds_x;
                old_dest[1] = src[s_x >> 16];
                s_x += ds_x;
                old_dest[2] = src[s_x >> 16];
                s_x += ds_x;
                old_dest[3] = src[s_x >> 16];
                s_x += ds_x;
                old_dest += 4;
            }
        } else {
            int *s2 = frame + (y + start_y) * w + start_x;
            int x = x_len / 4;
            while (x--) {
                old_dest[0] = blend_pixel_avg(s2[0], src[s_x >> 16]);
                s_x += ds_x;
                old_dest[1] = blend_pixel_avg(s2[1], src[s_x >> 16]);
                s_x += ds_x;
                old_dest[2] = blend_pixel_avg(s2[2], src[s_x >> 16]);
                s_x += ds_x;
                old_dest[3] = blend_pixel_avg(s2[3], src[s_x >> 16]);
                s_x += ds_x;
                old_dest += 4;
                s2 += 4;
            }
        }
        frame_out += w;
    }
    for (int y = 0; y < y_len; y++) {
        memcpy(dst, src, x_len * sizeof(int));
        dst += w;
        src += w;
    }

    return 0;
}

static int blitter_normal(int *frame, int *frame_out, int w, int h, int f_val, int blend, int subpixel)
{
    int ds_x = ((f_val + 32) << 16) / 64;
    int isx = ((w << 16) - (ds_x * w)) / 2;
    int s_y = ((h << 16) - (ds_x * h)) / 2;

    if (subpixel) {
        for (int y = 0; y < h; y++) {
            int s_x = isx;
            int *src = frame + (s_y >> 16) * w;
            int ypart = (s_y >> 8) & 0xff;
            s_y += ds_x;
            ypart = (ypart * 255) >> 8;
            int x = w / 4;
            while (x--) {
                frame_out[0] = blend_pixel_4(src + (s_x >> 16), w, (s_x >> 8) & 0xff, ypart);
                s_x += ds_x;
                frame_out[1] = blend_pixel_4(src + (s_x >> 16), w, (s_x >> 8) & 0xff, ypart);
                s_x += ds_x;
                frame_out[2] = blend_pixel_4(src + (s_x >> 16), w, (s_x >> 8) & 0xff, ypart);
                s_x += ds_x;
                frame_out[3] = blend_pixel_4(src + (s_x >> 16), w, (s_x >> 8) & 0xff, ypart);
                s_x += ds_x;
                frame_out += 4;
            }
            if (blend) {
                frame_out -= w;
                int *s2 = frame + y * w;
                int x = w / 4;
                while (x--) {
                    frame_out[0] = blend_pixel_avg(frame_out[0], s2[0]);
                    frame_out[1] = blend_pixel_avg(frame_out[1], s2[1]);
                    frame_out[2] = blend_pixel_avg(frame_out[2], s2[2]);
                    frame_out[3] = blend_pixel_avg(frame_out[3], s2[3]);
                    frame_out += 4;
                    s2 += 4;
                }
            }
        }
    } else {
        for (int y = 0; y < h; y++) {
            int s_x = isx;
            int *src = frame + (s_y >> 16) * w;
            s_y += ds_x;
            if (!blend) {
                int x = w / 4;
                while (x--) {
                    frame_out[0] = src[s_x >> 16];
                    s_x += ds_x;
                    frame_out[1] = src[s_x >> 16];
                    s_x += ds_x;
                    frame_out[2] = src[s_x >> 16];
                    s_x += ds_x;
                    frame_out[3] = src[s_x >> 16];
                    s_x += ds_x;
                    frame_out += 4;
                }
            } else {
                int *s2 = frame + (y * w);
                int x = w / 4;
                while (x--) {
                    frame_out[0] = blend_pixel_avg(s2[0], src[s_x >> 16]);
                    s_x += ds_x;
                    frame_out[0] = blend_pixel_avg(s2[0], src[s_x >> 16]);
                    s_x += ds_x;
                    frame_out[0] = blend_pixel_avg(s2[0], src[s_x >> 16]);
                    s_x += ds_x;
                    frame_out[0] = blend_pixel_avg(s2[0], src[s_x >> 16]);
                    s_x += ds_x;
                    frame_out += 4;
                    s2 += 4;
                }
            }
        }
    }
    return 1;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    BlitterFBContext *bctx = ctx->priv;
    int f_val;

    if (is_beat && bctx->beatch)
        bctx->fpos = bctx->scale2;

    if (bctx->scale < bctx->scale2) {
        f_val = (bctx->fpos > bctx->scale) ? bctx->fpos : bctx->scale;
        bctx->fpos -= 3;
    } else {
        f_val = (bctx->fpos < bctx->scale) ? bctx->fpos : bctx->scale;
        bctx->fpos += 3;
    }

    if (f_val < 0)
        f_val = 0;

    if (f_val < 32)
        return blitter_normal(frame, frame_out, w, h, f_val, bctx->blend, bctx->subpixel);
    if (f_val > 32)
        return blitter_out(frame, frame_out, w, h, f_val, bctx->blend);
    return 0;
}

Component t_blit = {
    .name = "Trans / Blitter Feedback",
    .code = 4,
    .priv_size = sizeof(BlitterFBContext),
    .load_config = load_config,
    .render = render,
};
