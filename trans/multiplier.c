/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

enum modes {
    MD_XI,
    MD_X8,
    MD_X4,
    MD_X2,
    MD_X05,
    MD_X025,
    MD_X0125,
    MD_XS,
};

void (*multiplier_mul1)(int *, int) = NULL;
void (*multiplier_mul2)(int *, int) = NULL;
void (*multiplier_mul3)(int *, int) = NULL;
void (*multiplier_div1)(int *, int) = NULL;
void (*multiplier_div2)(int *, int) = NULL;
void (*multiplier_div3)(int *, int) = NULL;
void (*multiplier_infrt)(int *, int) = NULL;
void (*multiplier_infsq)(int *, int) = NULL;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    pi_union temp;
    if (buf_len >= 4)
        temp.i = read_s32le(buf);
    ctx->priv = temp.p;
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    pi_union temp = { .p = ctx->priv };

    switch (temp.i) {
        case MD_XI:
            if (multiplier_infrt)
                multiplier_infrt(frame, w*h);
            else
                for (int i = 0; i < w*h; i++)
                    frame[i] = (frame[i]) ? 0xffffff : 0;
            break;
        case MD_X8:
            if (multiplier_mul3)
                multiplier_mul3(frame, w*h);
            else
                for (int i = 0; i < w*h; i++) {
                    int r = MIN(CR_TO_I(frame[i]) * 8, 255);
                    int g = MIN(CG_TO_I(frame[i]) * 8, 255);
                    int b = MIN(CB_TO_I(frame[i]) * 8, 255);
                    frame[i] = I3_TO_I(r,g,b);
                }
            break;
        case MD_X4:
            if (multiplier_mul2)
                multiplier_mul2(frame, w*h);
            else
                for (int i = 0; i < w*h; i++) {
                    int r = MIN(CR_TO_I(frame[i]) * 4, 255);
                    int g = MIN(CG_TO_I(frame[i]) * 4, 255);
                    int b = MIN(CB_TO_I(frame[i]) * 4, 255);
                    frame[i] = I3_TO_I(r,g,b);
                }
            break;
        case MD_X2:
            if (multiplier_mul1)
                multiplier_mul1(frame, w*h);
            else
                for (int i = 0; i < w*h; i++) {
                    int r = MIN(CR_TO_I(frame[i]) * 2, 255);
                    int g = MIN(CG_TO_I(frame[i]) * 2, 255);
                    int b = MIN(CB_TO_I(frame[i]) * 2, 255);
                    frame[i] = I3_TO_I(r,g,b);
                }
            break;
        case MD_X05:
            if (multiplier_div1)
                multiplier_div1(frame, w*h);
            else
                for (int i = 0; i < w*h; i++) {
                    int r = CR_TO_I(frame[i]) / 2;
                    int g = CG_TO_I(frame[i]) / 2;
                    int b = CB_TO_I(frame[i]) / 2;
                    frame[i] = I3_TO_I(r,g,b);
                }
            break;
        case MD_X025:
            if (multiplier_div2)
                multiplier_div2(frame, w*h);
            else
                for (int i = 0; i < w*h; i++) {
                    int r = CR_TO_I(frame[i]) / 4;
                    int g = CG_TO_I(frame[i]) / 4;
                    int b = CB_TO_I(frame[i]) / 4;
                    frame[i] = I3_TO_I(r,g,b);
                }
            break;
        case MD_X0125:
            if (multiplier_div3)
                multiplier_div3(frame, w*h);
            else
                for (int i = 0; i < w*h; i++) {
                    int r = CR_TO_I(frame[i]) / 8;
                    int g = CG_TO_I(frame[i]) / 8;
                    int b = CB_TO_I(frame[i]) / 8;
                    frame[i] = I3_TO_I(r,g,b);
                }
            break;
        case MD_XS:
            if (multiplier_infsq)
                multiplier_infsq(frame, w*h);
            else
                for (int i = 0; i < w*h; i++)
                    frame[i] = (frame[i] != 0xffffff) ? 0 : 0xffffff;
            break;
    }

    return 0;
}

Component t_multiplier = {
    .name = "Trans / Multiplier",
    .signature = "Multiplier",
    .priv_size = 0,
    .load_config = load_config,
    .render = render,
};
