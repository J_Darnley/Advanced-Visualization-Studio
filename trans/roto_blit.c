/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int zoom_scale1, zoom_scale2;
    int rot_dir, blend, scale_fpos;
    int beatch, beatch_speed, beatch_scale;
    int rot_rev;
    int subpixel;
    float rot_rev_pos;
    int l_w, l_h;
    int *w_mul;
} RotoBlitterContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    RotoBlitterContext *rbctx = ctx->priv;
    int pos = 0;
    R32(rbctx->zoom_scale1);
    R32(rbctx->rot_dir);
    R32(rbctx->blend);
    R32(rbctx->beatch);
    R32(rbctx->beatch_speed);
    R32(rbctx->zoom_scale2);
    R32(rbctx->beatch_scale);
    R32(rbctx->subpixel);
    rbctx->scale_fpos = rbctx->zoom_scale1;

    /* From init */
    rbctx->rot_rev = 1;
    rbctx->rot_rev_pos = 1.0;

    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    RotoBlitterContext *rbctx = ctx->priv;

    if (rbctx->l_w != w || rbctx->l_h != h || !rbctx->w_mul) {
        if (rbctx->w_mul)
            free(rbctx->w_mul);
        rbctx->l_w = w;
        rbctx->l_w = h;
        rbctx->w_mul = malloc(h * sizeof(int));
        if (!rbctx->w_mul)
            return -1;
        for (int x = 0; x < h; x++)
            rbctx->w_mul[x] = x * w;
    }

    int *w_mul = rbctx->w_mul;

    int *dest = frame_out;
    int *src = frame;
    int *bdest = frame;

    if (is_beat && rbctx->beatch)
        rbctx->rot_rev = -rbctx->rot_rev;

    if (!rbctx->beatch)
        rbctx->rot_rev = 1;

    rbctx->rot_rev_pos += 1.0 / (1 + rbctx->beatch_speed * 4) * (rbctx->rot_rev - rbctx->rot_rev_pos);

    if (rbctx->rot_rev_pos > rbctx->rot_rev && rbctx->rot_rev > 0)
        rbctx->rot_rev_pos = rbctx->rot_rev;
    if (rbctx->rot_rev_pos < rbctx->rot_rev && rbctx->rot_rev < 0)
        rbctx->rot_rev_pos = rbctx->rot_rev;

    if (is_beat && rbctx->beatch_scale)
        rbctx->scale_fpos = rbctx->zoom_scale2;

    int f_val;
    if (rbctx->zoom_scale1 < rbctx->zoom_scale2) {
        f_val = MAX(rbctx->scale_fpos, rbctx->zoom_scale1);
        if (rbctx->scale_fpos > rbctx->zoom_scale1)
            rbctx->scale_fpos -= 3;
    } else {
        f_val = MIN(rbctx->scale_fpos, rbctx->zoom_scale1);
        if (rbctx->scale_fpos < rbctx->zoom_scale1)
            rbctx->scale_fpos += 3;
    }

    float zoom = 1.0 + (f_val - 31) / 31.0;
    float theta = (rbctx->rot_dir - 32) * rbctx->rot_rev_pos;
    float temp;

    temp = cosf(theta * M_PI / 180.0) * zoom;
    int ds_dx = temp * 65536.0;
    int dt_dy = temp * 65536.0;

    temp = sinf(theta * M_PI / 180.0);
    int ds_dy = -(temp * 65536.0); /* The effect is very interesting without this minus sign. */
    int dt_dx = temp * 65536.0;

    int s, sstart, t, tstart;
    s = sstart = (w-1) * (32768 + (1 << 20)) - ((w-1)/2 * ds_dx + (h-1)/2 * ds_dy);
    t = tstart = (h-1) * (32768 + (1 << 20)) - ((w-1)/2 * dt_dx + (h-1)/2 * dt_dy);

    int ds = (w-1) << 16;
    int dt = (h-1) << 16;

    int y = h;

    if (ds_dx <= -ds || ds_dx >= ds || dt_dx <= -dt || dt_dx >= dt)
        return 0;

    while (y--) {
        if (ds)
            s %= ds;
        if (dt)
            t %= dt;
        if (s < 0)
            s += ds;
        if (t < 0)
            t += dt;

        int x = w;

#define DO_LOOP(Z) while (x--) { Z; s += ds_dx; t += dt_dx; }

#define DO_LOOPS(Z) \
    if (ds_dx <= 0 && dt_dx <= 0)                              \
        DO_LOOP(if (t <  0)  t += dt; if (s <  0)  s += ds; Z) \
    else if (ds_dx <= 0)                                       \
        DO_LOOP(if (t >= dt) t -= dt; if (s <  0)  s += ds; Z) \
    else if (dt_dx <= 0)                                       \
        DO_LOOP(if (t <  0)  t += dt; if (s >= ds) s -= ds; Z) \
    else                                                       \
        DO_LOOP(if (t >= dt) t -= dt; if (s >= ds) s -= ds; Z) \

        if (rbctx->subpixel && rbctx->blend) {
            DO_LOOPS(*dest++ = blend_pixel_avg(*bdest++, blend_pixel_4_16(src + (s >> 16) + w_mul[t >> 16], w, s, t)))
        } else if (rbctx->subpixel) {
            DO_LOOPS(*dest++ = blend_pixel_4_16(src + (s >> 16) + w_mul[t >> 16], w, s, t))
        } else if (!rbctx->blend) {
            DO_LOOPS(*dest++ = src[(s >> 16) + w_mul[t >> 16]])
        } else {
            DO_LOOPS(*dest++ = blend_pixel_avg(*bdest++, src[(s >> 16) + w_mul[t >> 16]]))
        }

        sstart += ds_dy;
        s = sstart;
        tstart += dt_dy;
        t = tstart;
    }

    return 1;
}

static void uninit(ComponentContext *ctx)
{
    RotoBlitterContext *rbctx = ctx->priv;

    FREE(rbctx->w_mul);
}

Component t_rotoblit = {
    .name = "Trans / Roto Blitter",
    .code = 9,
    .priv_size = sizeof(RotoBlitterContext),
    .load_config = load_config,
    .render = render,
    .uninit = uninit,
};
