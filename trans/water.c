/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

void (*water_line)(int *, int *, int *, int) = NULL;

typedef struct {
    int *last_frame;
    size_t last_frame_len;
    int enabled;
} WaterContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    WaterContext *water = ctx->priv;
    int pos = 0;
    R32(water->enabled);
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, unused int is_beat)
{
    WaterContext *water = ctx->priv;

    if (!water->enabled)
        return 0;

    if (!water->last_frame || (size_t)w * h != water->last_frame_len) {
        if (water->last_frame)
            free(water->last_frame);
        water->last_frame_len = w * h;
        water->last_frame = calloc(w * h, sizeof(int));
        if (!water->last_frame)
            return -1;
    }

    int *fb = water->last_frame;
    int *fi = frame;
    int *fo = frame_out;

#define RGB_SET(a) \
    r = (a)&(255<<16); g = (a)&(255<<8); b = (a)&255;

#define RGB_ADD(a) \
    r += (a)&(255<<16); g += (a)&(255<<8); b += (a)&255;

#define RGB_SUB(a) \
    r -= (a)&(255<<16); g -= (a)&(255<<8); b -= (a)&255;

#define RGB_CLIP() \
    r = CLIP(r, 0, 255<<16); g = CLIP(g, 0, 255<<8); b = CLIP(b, 0, 255);

#define RGB_DIV2() \
    r >>= 1; g >>= 1; b >>= 1;

#define RGB_OUT() \
    ((r & (255<<16)) | (g & (255<<8)) | (b & 255))

    int r, g, b;

    /* Top line */
    /* Left edge */
    RGB_SET(fi[1]);
    RGB_ADD(fi[w]);
    fi++;
    RGB_SUB(fb[0]);
    fb++;
    RGB_CLIP();
    *fo++ = RGB_OUT();

    /* Middle */
    for (int x = w-2; x; x--) {
        RGB_SET(fi[1]);
        RGB_ADD(fi[-1]);
        RGB_ADD(fi[w]);
        fi++;
        RGB_DIV2();
        RGB_SUB(fb[0]);
        fb++;
        RGB_CLIP();
        *fo++ = RGB_OUT();
    }

    /* Right edge */
    RGB_SET(fi[-1]);
    RGB_ADD(fi[w]);
    fi++;
    RGB_SUB(fb[0]);
    fb++;
    RGB_CLIP();
    *fo++ = RGB_OUT();

    /* Middle block */
    for (int y = h-2; y; y--) {
        /* Left edge */
        RGB_SET(fi[1]);
        RGB_ADD(fi[w]);
        RGB_ADD(fi[-w]);
        fi++;
        RGB_DIV2();
        RGB_SUB(fb[0]);
        fb++;
        RGB_CLIP();
        *fo++ = RGB_OUT();

        /* Middle */
        if (water_line) {
            water_line(fo, fi, fb, w);
            fo += w-2;
            fi += w-2;
            fb += w-2;
        } else {
            for (int x = w-2; x; x--) {
                RGB_SET(fi[1]);
                RGB_ADD(fi[-1]);
                RGB_ADD(fi[w]);
                RGB_ADD(fi[-w]);
                fi++;
                RGB_DIV2();
                RGB_SUB(fb[0]);
                fb++;
                RGB_CLIP();
                *fo++ = RGB_OUT();
            }
        }

        /* Right edge */
        RGB_SET(fi[-1]);
        RGB_ADD(fi[w]);
        RGB_ADD(fi[-w]);
        fi++;
        RGB_DIV2();
        RGB_SUB(fb[0]);
        fb++;
        RGB_CLIP();
        *fo++ = RGB_OUT();
    }

    /* Bottom line */
    /* Left edge */
    RGB_SET(fi[1]);
    RGB_ADD(fi[-w]);
    fi++;
    RGB_SUB(fb[0]);
    fb++;
    RGB_CLIP();
    *fo++ = RGB_OUT();

    /* Middle */
    for (int x = w-2; x; x--) {
        RGB_SET(fi[1]);
        RGB_ADD(fi[-1]);
        RGB_ADD(fi[-w]);
        fi++;
        RGB_DIV2();
        RGB_SUB(fb[0]);
        fb++;
        RGB_CLIP();
        *fo++ = RGB_OUT();
    }

    /* Right edge */
    RGB_SET(fi[-1]);
    RGB_ADD(fi[w]);
    fi++;
    RGB_SUB(fb[0]);
    fb++;
    RGB_CLIP();
    *fo++ = RGB_OUT();

    memcpy(water->last_frame, frame, w * h * sizeof(int));

    return 1;
}

static void uninit(ComponentContext *ctx)
{
    WaterContext *water = ctx->priv;

    FREE(water->last_frame);
}

Component t_water = {
    .name = "Trans / Water",
    .code = 20,
    .priv_size = sizeof(WaterContext),
    .load_config = load_config,
    .render = render,
    .uninit = uninit,
};
