/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

typedef struct {
    int enabled;
    int colour_clip;
    int colour_clip_out;
    int colour_dist;
} ColourClipContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    ColourClipContext *ccctx = ctx->priv;
    int pos = 0;
    R32(ccctx->enabled);
    R32(ccctx->colour_clip);
    R32(ccctx->colour_clip_out);
    R32(ccctx->colour_dist);
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    ColourClipContext *ccctx = ctx->priv;
    int fs_r = CR_TO_I(ccctx->colour_clip);
    int fs_g = CG_TO_I(ccctx->colour_clip);
    int fs_b = CB_TO_I(ccctx->colour_clip);
    int l = (ccctx->colour_dist * 2) * (ccctx->colour_dist * 2);

    int x = w * h;

    if (ccctx->enabled == 1)
        while (x--) {
            if (CR_TO_I(frame[0]) <= fs_r
                    && CG_TO_I(frame[0]) <= fs_g
                    && CB_TO_I(frame[0]) <= fs_b)
                /* Original code preserves the alpha channel value. */
                frame[0] = ccctx->colour_clip_out;
            frame++;
        }
    else if (ccctx->enabled == 2)
        while (x--) {
            if (CR_TO_I(frame[0]) >= fs_r
                    && CG_TO_I(frame[0]) >= fs_g
                    && CB_TO_I(frame[0]) >= fs_b)
                /* Original code preserves the alpha channel value. */
                frame[0] = ccctx->colour_clip_out;
            frame++;
        }
    else
        while (x--) {
            int a = frame[0];
            int r = CR_TO_I(a) - fs_r;
            int g = CG_TO_I(a) - fs_g;
            int b = CB_TO_I(a) - fs_b;
            if (r*r + g*g + b*b <= l)
                /* Original code preserves the alpha channel value. */
                frame[0] = ccctx->colour_clip_out;
            frame++;
        }

    return 0;
}

Component t_colourclip = {
    .name = "Trans / Colour Clip",
    .code = 12,
    .priv_size = sizeof(ColourClipContext),
    .load_config = load_config,
    .render = render,
};
