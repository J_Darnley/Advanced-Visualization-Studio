/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "block.h"
#include "components.h"
#include "common.h"

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    /* Store the enabled flag in an unused pointer.
     *
     * Since it is the only configuration option for the filter and it really
     * only requires 1 bit to store it we might as well not define a pointless
     * private structure, allocate it, use it and destroy it only to store this.
     * Instead we will abuse a pointer which would go unused in this case. */

    pi_union temp;
    if (buf_len >= 4)
        temp.i = read_s32le(buf);
    ctx->priv = temp.p;
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    if (!ctx->priv)
        return 0;

    /* Inverting a colour is as easy as subtracting it from white.  Subtracting
     * from 255 (or any other power of 2 minus 1) is identical to xoring with
     * 255 (or similar). */

    blend_block_const_xor(frame, frame, 0xFFFFFF, w*h);

    return 0;
}

Component t_invert = {
    .name = "Trans / Invert",
    .code = 37,
    .priv_size = 0,
    .load_config = load_config,
    .render = render,
};
