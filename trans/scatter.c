/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

typedef struct {
    int fudge_table[512];
    int enabled, ftw;
} ScatterContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    ScatterContext *scat = ctx->priv;
    int pos = 0;
    R32(scat->enabled);
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, unused int is_beat)
{
    ScatterContext *scat = ctx->priv;

    if (!scat->enabled)
        return 0;

    if (scat->ftw != w) {
        for (int x = 0; x < 512; x++) {
            int xp = (x % 8) - 4;
            int yp = (x / 8) % 8 - 4;
            if (xp < 0)
                xp++;
            if (yp < 0)
                yp++;
            scat->fudge_table[x] = w * yp + xp;
        }
        scat->ftw = w;
    }

    /* Copy first 4 lines. */
    for (int i = w*4; i > 0; i--)
        *frame_out++ = *frame++;

    /* Transform middle h-8 lines. */
    for (int i = w * (h-8); i > 0; i--) {
        *frame_out++ = frame[scat->fudge_table[rand() & 511]];
        frame++;
    }

    /* Copy last 4 lines. */
    for (int i = w*4; i > 0; i--)
        *frame_out++ = *frame++;

    return 1;
}

Component t_scatter = {
    .name = "Trans / Scatter",
    .code = 16,
    .priv_size = sizeof(ScatterContext),
    .load_config = load_config,
    .render = render,
};
