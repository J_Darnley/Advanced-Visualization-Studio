/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int enabled;
    int blend, blend_avg, smax;
    uint8_t *depth_buffer;
    int old_x, old_y;
    int static_grain;
    uint8_t rand_tab[491];
    int rand_tab_pos;
} GrainContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    GrainContext *grain = ctx->priv;
    int pos = 0;
    R32(grain->enabled);
    R32(grain->blend);
    R32(grain->blend_avg);
    R32(grain->smax);
    R32(grain->static_grain);

    for (int x = 0; x < 491; x++)
        grain->rand_tab[x] = rand() & 255;
    grain->rand_tab_pos = rand() % 491;

    return 0;
}

static void init_depth_buffer(uint8_t *p, int len)
{
    do {
        *p++ = rand() % 255;
        *p++ = rand() % 100;
        len -= 2;
    } while (len > 0);
}

static inline int make_colour(int pixel, uint8_t random)
{
    int ret = 0;
    unsigned tmp = ((pixel & 0xff0000) * random) >> 8;
    tmp = MIN(tmp, 0xff0000);
    ret |= tmp & 0xff0000;

    tmp = ((pixel & 0xff00) * random) >> 8;
    tmp = MIN(tmp, 0xff00);
    ret |= tmp & 0xff00;

    tmp = ((pixel & 0xff) * random) >> 8;
    tmp = MIN(tmp, 0xff);
    ret |= tmp;

    return ret;
}

static inline uint8_t fast_rand_byte(GrainContext *grain)
{
    uint8_t ret = grain->rand_tab[grain->rand_tab_pos];
    grain->rand_tab_pos++;
    if (!(grain->rand_tab_pos & 15))
        grain->rand_tab_pos += rand() % 73;
    if (grain->rand_tab_pos >= 491)
        grain->rand_tab_pos -= 491;
    return ret;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    GrainContext *grain = ctx->priv;

    if (!grain->enabled)
        return 0;

    int smax_sc = (grain->smax * 255) / 100;

    if (w != grain->old_x || h != grain->old_y) {
        if (grain->depth_buffer)
            free(grain->depth_buffer);
        grain->depth_buffer = malloc(w * h * 2);
        if (!grain->depth_buffer)
            return -1;
        init_depth_buffer(grain->depth_buffer, w * h * 2);
        grain->old_x = w;
        grain->old_y = h;
    }

    grain->rand_tab_pos += rand() % 300;
    if (grain->rand_tab_pos >= 491)
        grain->rand_tab_pos -= 491;

    int *p = frame;
    uint8_t *q = grain->depth_buffer;

    if (grain->static_grain) {
        if (grain->blend) {
            for (int l = w * h; l; l--) {
                if (*p) {
                    int c = 0;
                    if (q[1] < smax_sc)
                        c = make_colour(p[0], q[0]);
                    *p = blend_pixel_add(*p, c);
                }
                p++;
                q += 2;
            }
        } else if (grain->blend_avg) {
            for (int l = w * h; l; l--) {
                if (*p) {
                    int c = 0;
                    if (q[1] < smax_sc)
                        c = make_colour(p[0], q[0]);
                    *p = blend_pixel_avg(*p, c);
                }
                p++;
                q += 2;
            }
        } else {
            for (int l = w * h; l; l--) {
                if (*p) {
                    int c = 0;
                    if (q[1] < smax_sc)
                        c = make_colour(p[0], q[0]);
                    *p = c;
                }
                p++;
                q += 2;
            }
        }
    } else {
        if (grain->blend) {
            for (int l = w * h; l; l--) {
                if (*p) {
                    int c = 0;
                    if (fast_rand_byte(grain) < smax_sc)
                        c = make_colour(p[0], fast_rand_byte(grain));
                    *p = blend_pixel_add(*p, c);
                }
                p++;
                q += 2;
            }
        } else if (grain->blend_avg) {
            for (int l = w * h; l; l--) {
                if (*p) {
                    int c = 0;
                    if (fast_rand_byte(grain) < smax_sc)
                        c = make_colour(p[0], fast_rand_byte(grain));
                    *p = blend_pixel_avg(*p, c);
                }
                p++;
                q += 2;
            }
        } else {
            for (int l = w * h; l; l--) {
                if (*p) {
                    int c = 0;
                    if (fast_rand_byte(grain) < smax_sc)
                        c = make_colour(p[0], fast_rand_byte(grain));
                    *p = c;
                }
                p++;
                q += 2;
            }
        }
    }

    return 0;
}

static void uninit(ComponentContext *ctx)
{
    GrainContext *grain = ctx->priv;

    FREE(grain->depth_buffer);
}

Component t_grain = {
    .name = "Trans / Grain",
    .code = 24,
    .priv_size = sizeof(GrainContext),
    .load_config = load_config,
    .render = render,
    .uninit = uninit,
};
