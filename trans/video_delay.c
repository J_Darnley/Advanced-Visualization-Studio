/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

typedef struct {
    int enabled;
    int use_beats;
    int delay, frame_delay;
    void *buffer;
    size_t buffer_size, vbuf_old_size, io_pos;
    int frames_since_beat;
} VideoDelayContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    VideoDelayContext *vd = ctx->priv;
    int pos = 0;

    R32(vd->enabled);
    R32(vd->use_beats);
    R32(vd->delay);
    vd->frame_delay = vd->delay;

    /* Initial buffer for 10 640x480 frames. */
    vd->buffer = calloc(640 * 480 * 10, sizeof(int));
    if (!vd->buffer)
        return -1;
    vd->buffer_size = 640 * 480 * 10 * sizeof(int);

    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, unused int is_beat)
{
    VideoDelayContext *vd = ctx->priv;
    uint8_t *buf = vd->buffer;

    if (vd->use_beats) {
        if (is_beat) {
            vd->frame_delay = vd->frames_since_beat * vd->delay;
            vd->frame_delay = MIN(vd->frame_delay, 400);
            vd->frames_since_beat = 0;
        }
        vd->frames_since_beat++;
    }

    /* If not enabled for frame delay is zero then there is nothing to do. */
    if (!vd->enabled || vd->frame_delay == 0)
        return 0;

    size_t frame_mem = w*h * sizeof(int);
    size_t vbuf_size = vd->frame_delay * frame_mem;
    size_t io_pos = vd->io_pos;

    if (vbuf_size > vd->vbuf_old_size) {
        /* If the number of frames to buffer has increased then we need to
         * re-order things and possibly allocate more memory. */

        if (vbuf_size > vd->buffer_size) {
            /* If the size of frames to buffer is greater than the memory we
             * have we need to allocate more. */

            if (vd->use_beats)
                /* If we are using beats then we should grow the buffer to
                 * some size larger than we need.  In this case 1.5 times
                 * larger, up to 400 frames. */
                vd->buffer_size = MIN((3 * vbuf_size) / 2, 400 * frame_mem);
            else
                /* Otherwise just allocate the size we need. */
                vd->buffer_size = vbuf_size;

            vd->buffer = buf = realloc(buf, vbuf_size);
            if (!buf) {
                ERROR("no memory\n");
                return -1;
            }
        }

        /* The number of frames to buffer has increased so we need to
         * duplicate the oldest frame. */
        /* Move the oldest frames to the end of the new buffer. */
        memmove(buf + io_pos + vbuf_size - vd->vbuf_old_size,
                buf + io_pos,
                vd->vbuf_old_size - io_pos);

        /* Duplicate the oldest frame into the new empty space. */
        for (size_t i = io_pos + frame_mem; i < vbuf_size - vd->vbuf_old_size + vd->io_pos; i += frame_mem)
            memcpy(buf + i, buf + io_pos, frame_mem);

    } else if (vbuf_size < vd->vbuf_old_size) {
        /* If the number of frames to buffer has decreased then we need to drop
         * the oldest frames. */

        if (io_pos + frame_mem > vbuf_size) {
            /* If the I/O position is beyond the end of the new buffer we need
             * to move the most recent frames to the start and set I/O position
             * to the oldest of them. */
            memmove(buf,
                    buf + io_pos - vbuf_size,
                    vbuf_size);
            io_pos = 0;
        } else {
            /* If the I/O position is in the middle of the new buffer we need
             * move the oldest allowed frames to the I/O position. */
            memmove(buf + io_pos,
                    buf + vd->vbuf_old_size + io_pos - vbuf_size,
                    vbuf_size - io_pos);
        }
    }

    vd->vbuf_old_size = vbuf_size;

    memcpy(frame_out, buf + io_pos, frame_mem);
    memcpy(buf + io_pos, frame, frame_mem);

    if (io_pos + frame_mem >= vbuf_size)
        vd->io_pos = 0;
    else
        vd->io_pos = io_pos + frame_mem;

    return 1;
}

static void uninit(ComponentContext *ctx)
{
    VideoDelayContext *vd = ctx->priv;

    FREE(vd->buffer);
}

Component t_videodelay = {
    .name = "Trans / Video Delay",
    .signature = "Holden04: Video Delay",
    .priv_size = sizeof(VideoDelayContext),
    .load_config = load_config,
    .render = render,
    .uninit = uninit,
};
