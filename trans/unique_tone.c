/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

void (*unique_tone_add)(int *dst, int *src, int pixels, int colour) = NULL;
void (*unique_tone_avg)(int *dst, int *src, int pixels, int colour) = NULL;
void (*unique_tone_replace)(int *dst, int *src, int pixels, int colour) = NULL;

void (*unique_tone_add_inv)(int *dst, int *src, int pixels, int colour) = NULL;
void (*unique_tone_avg_inv)(int *dst, int *src, int pixels, int colour) = NULL;
void (*unique_tone_replace_inv)(int *dst, int *src, int pixels, int colour) = NULL;

enum {
    EFFECT_BLEND_ADD = 1,
    EFFECT_BLEND_AVG = 2,
    EFFECT_REPLACE = 4,
    EFFECT_INVERT = 8,
};

typedef struct {
    int enabled;
    int invert;
    int colour;
    int blend, blend_avg;
    int effect;
    uint8_t table_r[256], table_g[256], table_b[256];
} UniqueToneContext;

static void rebuild_table(UniqueToneContext *utctx)
{
    float colour_r = CR_TO_I(utctx->colour);
    float colour_g = CG_TO_I(utctx->colour);
    float colour_b = CB_TO_I(utctx->colour);

    for (int i = 0; i < 256; i++) {
        utctx->table_r[i] = i / 255.0 * colour_r;
        utctx->table_g[i] = i / 255.0 * colour_g;
        utctx->table_b[i] = i / 255.0 * colour_b;
    }
}

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    UniqueToneContext *utctx = ctx->priv;
    int pos = 0;
    R32(utctx->enabled);
    R32(utctx->colour);
    R32(utctx->blend);
    R32(utctx->blend_avg);
    R32(utctx->invert);

    rebuild_table(utctx);

    if (utctx->blend)
        utctx->effect = EFFECT_BLEND_ADD;
    else if (utctx->blend_avg)
        utctx->effect = EFFECT_BLEND_AVG;
    else
        utctx->effect = EFFECT_REPLACE;

    utctx->effect |= utctx->invert ? EFFECT_INVERT : 0;

    return 0;
}

static inline int depth_of(int c)
{
    int r = MAX3(CR_TO_I(c), CG_TO_I(c), CB_TO_I(c));
    return r;
}

static inline int depth_of_inv(int c)
{
    int r = MAX3(CR_TO_I(c), CG_TO_I(c), CB_TO_I(c));
    return 255 - r;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, unused int is_beat)
{
    UniqueToneContext *utctx = ctx->priv;

    if (!utctx->enabled)
        return 0;

    switch (utctx->effect) {
        /* Inverted depth calculation. */
        case EFFECT_BLEND_ADD|EFFECT_INVERT:
            if (unique_tone_add_inv)
                unique_tone_add_inv(frame_out, frame, w*h, utctx->colour);
            else
                for (int i = 0; i < w * h; i++) {
                    int d = depth_of_inv(frame[i]);
                    int c = I3_TO_I(utctx->table_r[d], utctx->table_g[d], utctx->table_b[d]);
                    frame_out[i] = blend_pixel_add(frame[i], c);
                }
            break;

        case EFFECT_BLEND_AVG|EFFECT_INVERT:
            if (unique_tone_avg_inv)
                unique_tone_avg_inv(frame_out, frame, w*h, utctx->colour);
            else
                for (int i = 0; i < w * h; i++) {
                    int d = depth_of_inv(frame[i]);
                    int c = I3_TO_I(utctx->table_r[d], utctx->table_g[d], utctx->table_b[d]);
                    frame_out[i] = blend_pixel_avg(frame[i], c);
                }
            break;

        case EFFECT_REPLACE|EFFECT_INVERT:
            if (unique_tone_replace_inv)
                unique_tone_replace_inv(frame_out, frame, w*h, utctx->colour);
            else
                for (int i = 0; i < w * h; i++) {
                    int d = depth_of_inv(frame[i]);
                    frame_out[i] = I3_TO_I(utctx->table_r[d], utctx->table_g[d], utctx->table_b[d]);
                }
            break;

        /* Regular depth calculation. */
        case EFFECT_BLEND_ADD:
            if (unique_tone_add)
                unique_tone_add(frame_out, frame, w*h, utctx->colour);
            else
                for (int i = 0; i < w * h; i++) {
                    int d = depth_of(frame[i]);
                    int c = I3_TO_I(utctx->table_r[d], utctx->table_g[d], utctx->table_b[d]);
                    frame_out[i] = blend_pixel_add(frame[i], c);
                }
            break;

        case EFFECT_BLEND_AVG:
            if (unique_tone_avg)
                unique_tone_avg(frame_out, frame, w*h, utctx->colour);
            else
                for (int i = 0; i < w * h; i++) {
                    int d = depth_of(frame[i]);
                    int c = I3_TO_I(utctx->table_r[d], utctx->table_g[d], utctx->table_b[d]);
                    frame_out[i] = blend_pixel_avg(frame[i], c);
                }
            break;

        case EFFECT_REPLACE:
            if (unique_tone_replace)
                unique_tone_replace(frame_out, frame, w*h, utctx->colour);
            else
                for (int i = 0; i < w * h; i++) {
                    int d = depth_of(frame[i]);
                    frame_out[i] = I3_TO_I(utctx->table_r[d], utctx->table_g[d], utctx->table_b[d]);
                }
            break;
    }

    return 1;
}

Component t_uniquetone = {
    .name = "Trans / Unique Tone",
    .code = 38,
    .priv_size = sizeof(UniqueToneContext),
    .load_config = load_config,
    .render = render,
};
