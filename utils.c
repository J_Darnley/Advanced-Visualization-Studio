/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdlib.h>

#include "common.h"

const char *get_audio_channel_desc(int value)
{
    const char *descriptions[] = {
        "Left", "Right", "Center"
    };

    return descriptions[value];
}

const char *get_position_y_desc(int value)
{
    const char *descriptions[] = {
        "Top", "Bottom", "Center"
    };

    return descriptions[value];
}

int read_colours(const uint8_t *buf, int buf_len, int num_colours, int max_colours, int *colour)
{
    int pos = 0;
    if (num_colours <= 0 || num_colours > max_colours)
        ERROR("invalid number of colours (%d), must be 1..%d\n", num_colours, max_colours);
    /* FIXME: probably should return here. */

    num_colours = MIN(num_colours, max_colours);
    for (int i = 0; i < num_colours; i++)
        R32(colour[i]);

    return pos;
}

int interpolate_colours_64(int num_colours, int *colour, int *colour_pos)
{
    if (num_colours == 1)
        return colour[0];

    int p = *colour_pos / 64;
    int r = *colour_pos & 63;
    *colour_pos = (*colour_pos + 1) % (num_colours * 64);
    int c1, c2;
    int r1, r2, r3;

    c1 = colour[p];
    if (p + 1 >= num_colours)
        c2 = colour[0];
    else
        c2 = colour[p + 1];

    r1 = (CB_TO_I(c1) * (63 - r) + CB_TO_I(c2) * r) / 64;
    r2 = (CG_TO_I(c1) * (63 - r) + CG_TO_I(c2) * r) / 64;
    r3 = (CR_TO_I(c1) * (63 - r) + CR_TO_I(c2) * r) / 64;

    return I3_TO_I(r3, r2, r1);
}
