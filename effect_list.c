/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "blend.h"
#include "block.h"
#include "components.h"
#include "common.h"
#include "expressions.h"
#include "pixel.h"

typedef struct {
    int mode;
    int enabled;
    int clear;
    int in_blend_mode, in_blend_alpha;
    int out_blend_mode, out_blend_alpha;
    int in_buffer, out_buffer;
    int in_invert, out_invert;
    int beat_render, beat_render_frames;
    int use_expr;
    const char *expr_init;
    const char *expr_frame;
    void *expr_context;
    ComponentContext *component_contexts;
    int num_contexts;
    int fake_enabled;
    int *fb;
    int inited;
    int msg_no_alpha_in, msg_no_alpha_out;
} EffectListContext;

static const char sig[32] = "AVS 2.8+ Effect List Config";

static int load_config_code(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    EffectListContext *list = ctx->priv;
    int pos = 0;

    void *expr_ctx = expr_init(ctx->actx);
    if (!expr_ctx)
        return -1;

    R32(list->use_expr);

    int ret = expr_load_rstring(expr_ctx, &list->expr_init, buf + pos, buf_len - pos);
    if (ret < 0)
        return -1;
    pos += ret;

    ret = expr_load_rstring(expr_ctx, &list->expr_frame, buf + pos, buf_len - pos);
    if (ret < 0)
        return -1;

    if (expr_compile(expr_ctx, "__avs_internal_init", list->expr_init)
            || expr_compile(expr_ctx, "__avs_internal_frame", list->expr_frame))
        return -1;

    list->expr_context = expr_ctx;

    return 0;
}

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{

#define MAX_DEPTH 8

    /* Prevent excessive recursion.  See the comment in components.h about the
     * depth field in ComponentContext for more information. */
    if (ctx->depth > MAX_DEPTH) {
        ERROR("too many (%d) nested Effect Lists.  If this is a correct and "
              "sensible preset you can increase the MAX_DEPTH define "
              "(currently %d) and recompile.  Please also report this preset "
              "to the Advanced Visualization Studio project.\n",
              ctx->depth, MAX_DEPTH);
        return -1;
    }

    EffectListContext *list = ctx->priv;
    int pos = 0;
    int temp, mode;

    if (pos >= buf_len) {
        ERROR("unable to parse file correctly (too short)\n");
        return -1;
    }

    mode = buf[pos++];

    if (mode & 0x80) {
        mode &= ~0x80;
        R32(temp);
        mode = mode|temp;
    }

    list->in_blend_mode = (mode >> 8) & 31;
    list->out_blend_mode = ((mode >> 16) & 31) ^ 1;
    list->enabled = (mode & 2) ? 0 : 1;
    list->clear = mode & 1;

    int ext = ((mode & 0xff000000) >> 24) + 5;

    if (ext > 5) {
        if (pos < ext) {
            list->in_blend_alpha = read_s32le(buf + pos);
            pos += 4;
        }
        if (pos < ext) {
            list->out_blend_alpha = read_s32le(buf + pos);
            pos += 4;
        }
        if (pos < ext) {
            list->in_buffer = read_s32le(buf + pos);
            pos += 4;
        }
        if (pos < ext) {
            list->out_buffer = read_s32le(buf + pos);
            pos += 4;
        }
        if (pos < ext) {
            list->in_invert = read_s32le(buf + pos);
            pos += 4;
        }
        if (pos < ext) {
            list->out_invert = read_s32le(buf + pos);
            pos += 4;
        }
        if (pos < ext - 4) {
            list->beat_render = read_s32le(buf + pos);
            pos += 4;
        }
        if (pos < ext - 4) {
            list->beat_render_frames = read_s32le(buf + pos);
            pos += 4;
        }
    }

    int pos_save = pos;
    int component_count = 0;

    /* Count number of components. */
    while (pos < buf_len) {
        int code, size;

        R32(code);

#define DLLRENDERBASE 16384 /* magic number 0x4000 or 16384. */

        /* If the code is greater or equal to DLLRENDERBASE it means the
         * component is an APE plugin.  These have a 32 byte signature (which
         * seems to be an ASCII string) identifying the component. */
        if (code >= DLLRENDERBASE) {
            pos += 32;
            if (pos > buf_len) {
                ERROR("unable to parse file correctly (too short)\n");
                return -1;
            }
        }

        R32(size);

        /* Check that the size is less than the remaining buffer. */
        if (size > buf_len - pos) {
            ERROR("size (%d) is too large\n", size);
            return -1;
        }

        /* If this is an effect list more configuration needs to be loaded.
         * Check that ext is still valid and that the signature matches. */
        if (ext > 5 && code >= DLLRENDERBASE && !memcmp(buf + pos - 36, sig, 32) && size > 8) {
            if (load_config_code(ctx, buf + pos, size))
                return -1;
            ext = 0;
            pos_save = pos + size;
            component_count--; /* Don't count this as another component. */

        /* If this component is not the extra config data and the code is >=
         * DLLRENDERBASE, it must be an APE component. */
        } else if (code >= DLLRENDERBASE) {
            if (!find_component(code, buf + pos - 36)) {
                ERROR("unknown APE signature (%.32s)\n", buf + pos - 36);
                return -1;
            }

        /* Otherwise it is a regular built-in component. */
        } else {
            /* Check that the component exists before continuing. */
            if (!find_component(code, NULL)) {
                ERROR("unknown code (%d 0x%x)\n", code, code);
                return -1;
            }
        }

        component_count++;
        pos += size;
    }

    if (component_count == 0)
        return 0;

    pos = pos_save;
    ComponentContext *cctx = calloc(component_count, sizeof(ComponentContext));
    if (!cctx) {
        ERROR("no memory\n");
        return -1;
    }

    list->component_contexts = cctx;
    list->num_contexts = component_count;

    /* Effect List components don't pass their blend mode into their nested
     * components.  In the original code, each new Effect List sets the blend
     * mode to zero.  It will be better to use the default here. */
    int prev_blend_mode = blend_mode_default();

    for (int i = 0; pos < buf_len; i++) {
        int code, size;
        void *priv;
        const Component *comp;

        R32(code);

        if (code >= DLLRENDERBASE) {
            pos += 32;
            R32(size);
            comp = find_component(code, buf + pos - 36);
        } else {
            R32(size);
            comp = find_component(code, NULL);
        }

        /* If the component says it has no private context don't bother
         * allocating one. */
        if (comp->priv_size) {
            priv = calloc(1, comp->priv_size);
            if (!priv) {
                ERROR("no memory\n");
                return -1;
            }
        } else
            priv = NULL;

        cctx[i].component = comp;
        cctx[i].priv = priv;
        cctx[i].blend_mode = prev_blend_mode;
        cctx[i].depth = ctx->depth + 1;
        cctx[i].actx = ctx->actx;

        if(comp->load_config(&cctx[i], buf + pos, size))
            return -1;

        pos += size;
        prev_blend_mode = cctx[i].blend_mode;
    }

    return 0;
}

static void print_config(ComponentContext *ctx, int indent)
{

#define INDENT() fprintf(stderr, "%*c", indent * 4, ' ')

#define PRINT_BOOL(a, b) \
    INDENT(); \
    fprintf(stderr, a, (b) ? "true" : "false")

#define PRINT1(a) \
    INDENT(); \
    fprintf(stderr, a);

#define PRINT2(a, b) \
    INDENT(); \
    fprintf(stderr, a, b);

    EffectListContext *list = ctx->priv;

    if (!ctx->depth) {
        fprintf(stderr, "Main\n");
        indent = 1;
        PRINT_BOOL("clear every frame = %s\n", list->clear);
    } else {
        PRINT_BOOL("enabled = %s\n", list->enabled);
        PRINT_BOOL("clear every frame = %s\n", list->clear);
        PRINT_BOOL("enabled on beat = %s\n", list->beat_render);
        indent++;
        PRINT2("for %d frames\n", list->beat_render_frames);
        indent--;
        PRINT2("input blending = %d\n", list->in_blend_mode);
        PRINT2("output blending = %d\n", list->out_blend_mode);
        PRINT_BOOL("use expressions = %s\n", list->use_expr);
        PRINT2("expression init = %s\n", list->expr_init);
        PRINT2("expression frame = %s\n", list->expr_frame);
    }

    ComponentContext *cctx = list->component_contexts;

    for (int i = 0; i < list->num_contexts; i++) {
        const Component *comp = cctx[i].component;

        PRINT2("%s\n", comp->name);

        switch (cctx[i].which_vis_type | cctx[i].which_channel) {
            case VIS_SPECTRUM|VIS_LEFT:
                PRINT1("vis_data source = spectrum left\n");
                break;
            case VIS_SPECTRUM|VIS_RIGHT:
                PRINT1("vis_data source = spectrum right\n");
                break;
            case VIS_SPECTRUM|VIS_CENTER:
                PRINT1("vis_data source = spectrum center\n");
                break;
            case VIS_WAVEFORM|VIS_LEFT:
                PRINT1("vis_data source = waveform left\n");
                break;
            case VIS_WAVEFORM|VIS_RIGHT:
                PRINT1("vis_data source = waveform right\n");
                break;
            case VIS_WAVEFORM|VIS_CENTER:
                PRINT1("vis_data source = waveform center\n");
        }

        if (comp->print_config)
            comp->print_config(&cctx[i], indent);
    }
}

static int render_sub_components(ComponentContext *ctx, int *frame, int *frame_out, int w, int h, int is_beat)
{
    EffectListContext *list = ctx->priv;
    int true_ret = 0;

    /* Loop over every context and render. */
    for (int i = 0; i < list->num_contexts; i++) {
        ComponentContext *cctx = &list->component_contexts[i];
        const Component *comp = cctx->component;
        uint8_t *vis_data;

        /* Select vis_data source. */
        switch (cctx->which_vis_type | cctx->which_channel) {
            case VIS_SPECTRUM|VIS_LEFT:
                vis_data = ctx->actx->avsdc.spectrum.left;
                break;
            case VIS_SPECTRUM|VIS_RIGHT:
                vis_data = ctx->actx->avsdc.spectrum.right;
                break;
            case VIS_SPECTRUM|VIS_CENTER:
                vis_data = ctx->actx->avsdc.spectrum.center;
                break;
            case VIS_WAVEFORM|VIS_LEFT:
                vis_data = ctx->actx->avsdc.waveform.left;
                break;
            case VIS_WAVEFORM|VIS_RIGHT:
                vis_data = ctx->actx->avsdc.waveform.right;
                break;
            case VIS_WAVEFORM|VIS_CENTER:
            default:
                vis_data = ctx->actx->avsdc.waveform.center;
        }

        if (comp->render) {
            int ret;
            ret = comp->render(cctx, vis_data, frame, frame_out, w, h, is_beat);
            if (ret < 0)
                return -1;

            /* If the render function returns 1, the rendered frame is in
             * frame_out.  In that case, swap the pointers rather than copying
             * the data.  Also toggle true_ret to keep track of which has the
             * final frame. */
            if (ret == 1) {
                void *temp;
                temp = frame_out;
                frame_out = frame;
                frame = temp;
                true_ret ^= 1;
            }
        }
    }

    return true_ret;
}


static inline int depth_of(int c)
{
    int r = MAX3(CR_TO_I(c), CG_TO_I(c), CB_TO_I(c));
    return r;
}

static inline int depth_of_inv(int c)
{
    int r = MAX3(CR_TO_I(c), CG_TO_I(c), CB_TO_I(c));
    return 255 - r;
}

static void io_blend(int *dst, int *src, int w, int h, int alpha, int *alpha_buf, int blend_mode, int invert)
{
    switch (blend_mode) {
        case 1:
            memcpy(dst, src, w * h * sizeof(int));
            break;

        case 2:
            blend_block_avg(dst, src, dst, w*h);
            break;

        case 3:
            blend_block_max(dst, src, dst, w*h);
            break;

        case 4:
            blend_block_add(dst, src, dst, w*h);
            break;

        case 5:
            blend_block_sub(dst, dst, src, w*h);
            break;

        case 6:
            blend_block_sub(dst, src, dst, w*h);
            break;

        case 7: /* every other line */
            for (int y = 0; y < h; y += 2)
                memcpy(dst + y*w, src + y*w, w * sizeof(int));
            break;

        case 8: { /* every other pixel */
            int r = 0;
            for (int y = h; y > 0; y--) {
                int *in = src + r;
                int *out = dst + r;
                r ^= 1;
                for (int x = w/2; x > 0; x--) {
                    *out = *in;
                    out += 2;
                    in += 2;
                }
                dst += w;
                src += w;
            }
            break; }

        case 9:
            blend_block_xor(dst, src, dst, w*h);
            break;

        case 10:
            blend_block_adj(dst, src, dst, w*h, alpha);
            break;

        case 11:
            blend_block_mul(dst, src, dst, w*h);
            break;

        case 12:
            if (!alpha_buf)  /* If no buffer has yet been allocated then */
                break;       /* there is no useful data to read from it. */
            if (invert)
                for (int i = 0; i < w*h; i++)
                    dst[i] = blend_pixel_adj(src[i], dst[i], depth_of_inv(alpha_buf[i]));
            else
                for (int i = 0; i < w*h; i++)
                    dst[i] = blend_pixel_adj(src[i], dst[i], depth_of(alpha_buf[i]));
            break;

        case 13:
            blend_block_min(dst, src, dst, w*h);
            break;
    }
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, int *frame_out, int w, int h, int is_beat)
{
    EffectListContext *list = ctx->priv;

    if (is_beat && list->beat_render)
        list->fake_enabled = list->beat_render_frames;

    int enabled = list->enabled || list->fake_enabled-- > 0;
    int in_blend_alpha = list->in_blend_alpha;
    int out_blend_alpha = list->out_blend_alpha;
    int clear = list->clear;

    if (ctx->depth && list->use_expr) {

#define SET(a,b) expr_variable_set(list->expr_context, (a), (b))
#define GET(a) expr_variable_get(list->expr_context, (a))

        SET("beat", is_beat ? 1.0 : 0.0);
        SET("enabled", enabled ? 1.0 : 0.0);
        SET("w", w);
        SET("h", h);
        SET("clear", clear ? 1.0 : 0.0);
        SET("alphain", in_blend_alpha / 255.0);
        SET("alphaout", out_blend_alpha / 255.0);

        if (!list->inited) {
            if (expr_execute(list->expr_context, "__avs_internal_init", list->expr_init))
                return -1;
            list->inited = 1;
        }

        if (expr_execute(list->expr_context, "__avs_internal_frame", list->expr_frame))
            return -1;

#define F2B() (temp > 0.1 || temp < -0.1)

        double temp = GET("beat");
        is_beat = F2B();

        temp = GET("alphain");
        in_blend_alpha = CLIP(temp * 255.0, 0, 255);

        temp = GET("alphaout");
        out_blend_alpha = CLIP(temp * 255.0, 0, 255);

        temp = GET("enabled");
        enabled = F2B();

        temp = GET("clear");
        clear = F2B();
    }

    if (!ctx->depth || (enabled && list->in_blend_mode == 1 && list->out_blend_mode == 1)) {
        if (clear && !ctx->depth) {
            memset(frame, 0, w * h * sizeof(int));
            memset(frame_out, 0, w * h * sizeof(int));
        }

        return render_sub_components(ctx, frame, frame_out, w, h, is_beat);
    }

    if (!enabled)
        return 0;

    if (!list->fb) {
        list->fb = calloc(w*h, sizeof(int));
        if (!list->fb)
            return -1;
    }

    if (clear) {
        memset(list->fb, 0, w * h * sizeof(int));

        /* FIXME: This is a quick hack to work around the problem of the buffer
         * being dirty when Texer wants to write into it.  I must be failing to
         * see where this gets cleared in the original code, or I have written
         * Texer incorrectly (likely). */
        memset(frame_out, 0, w * h * sizeof(int));
    }

    int in_blend_mode = list->in_blend_mode;

    if (in_blend_mode == 10 && in_blend_alpha == 255)
        in_blend_mode = 1;
    if (in_blend_mode == 10 && in_blend_alpha == 0)
        in_blend_mode = 0;

    if (in_blend_mode == 12 && !list->msg_no_alpha_in
            && !ctx->actx->global_buffer[list->in_buffer]) {
        ERROR("No %s alpha buffer (%d) allocated.\n", "input", list->in_buffer);
        list->msg_no_alpha_in = 1;
    }

    io_blend(list->fb, frame, w, h, in_blend_alpha, ctx->actx->global_buffer[list->in_buffer], in_blend_mode, list->in_invert);

    int ret = render_sub_components(ctx, list->fb, frame_out, w, h, is_beat);

    if (ret < 0)
        return ret;

    int out_blend_mode = list->out_blend_mode;

    if (ret == 1)
        memcpy(list->fb, frame_out, w * h * sizeof(int));

    if (out_blend_mode == 10 && out_blend_alpha == 255)
        out_blend_mode = 1;
    if (out_blend_mode == 10 && out_blend_alpha == 0)
        out_blend_mode = 0;

    if (ret == 1 && out_blend_mode == 1)
        return 1;

    if (out_blend_mode == 12 && !list->msg_no_alpha_out
            && !ctx->actx->global_buffer[list->out_buffer]) {
        ERROR("No %s alpha buffer (%d) allocated.\n", "output", list->out_buffer);
        list->msg_no_alpha_out = 1;
    }

    io_blend(frame, list->fb, w, h, out_blend_alpha, ctx->actx->global_buffer[list->out_buffer], out_blend_mode, list->out_invert);

    return 0;
}

static void uninit(ComponentContext *ctx)
{
    EffectListContext *list = ctx->priv;
    ComponentContext *cctx = list->component_contexts;

    if (cctx) {
        for (int i = 0; i < list->num_contexts; i++) {
            const Component *comp = cctx[i].component;
            if (comp) {
                if (comp->uninit)
                    comp->uninit(&cctx[i]);
                if (comp->priv_size && cctx[i].priv)
                    free(cctx[i].priv);
            }
        }
        free(cctx);
    }

    FREE((void *)list->expr_init);
    FREE((void *)list->expr_frame);
    FREE(list->fb);

    expr_uninit(list->expr_context);
}

Component effect_list = {
    .name = "Effect List",
    .code = -2,
    .priv_size = sizeof(EffectListContext),
    .load_config = load_config,
    .print_config = print_config,
    .render = render,
    .uninit = uninit,
};
