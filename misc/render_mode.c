/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "avs.h"
#include "components.h"
#include "common.h"

/*
 * Layout of values in g_line_blend_mode:
 * - Least significant byte represents the blend mode (replace, add, max...)
 * - Second byte represents the "amount" for the adjustable blend
 * - Third byte represents the line width
 * - Most significant byte is unused except for the most significant bit which
 *   is used as an "enable" flag in this component.
 */

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    int pos = 0, temp = 0;
    R32(temp);
    if (temp & 0x80000000)
        ctx->blend_mode = temp & 0x7fffffff;
    return 0;
}

Component m_rendermode = {
    .name = "Misc / Set Render Mode",
    .code = 40,
    .load_config = load_config,
};
