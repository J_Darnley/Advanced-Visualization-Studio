/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"

typedef struct {
    uint8_t *comment;
    int length;
} CommentContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    CommentContext *cctx = ctx->priv;
    int pos = 0;

    R32(cctx->length);
    if (cctx->length > 1048576) {
        ERROR("string too long (%d)\n", cctx->length);
        return -1;
    }
    if (!cctx->length)
        return 0;
    cctx->comment = malloc(cctx->length + 1);
    if (!cctx->comment)
        return -1;
    memcpy(cctx->comment, buf + pos, cctx->length);
    cctx->comment[cctx->length] = 0;

    return 0;
}

static void print_config(ComponentContext *ctx, unused int indent)
{
    CommentContext *cctx = ctx->priv;
    if (cctx->length > 0 && cctx->comment)
        fprintf(stderr, "    %s\n", cctx->comment);
    return;
}

static void uninit(ComponentContext *ctx)
{
    CommentContext *cctx = ctx->priv;

    FREE(cctx->comment);
}

Component m_comment = {
    .name = "Misc / Comment",
    .code = 21,
    .priv_size = sizeof(CommentContext),
    .load_config = load_config,
    .print_config = print_config,
    .uninit = uninit,
};
