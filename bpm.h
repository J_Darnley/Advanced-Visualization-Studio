/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AVS_HEADER_BPM
#define AVS_HEADER_BPM 1

#include <stdint.h>

enum BeatConfig {
    BEAT_STANDARD = 0,
    BEAT_ADVANCED = 1,
    BEAT_STICKY = 2,
    BEAT_ONLY_STICKY = 4,
    BEAT_NEW_RESET = 8,
};

typedef struct BeatContext BeatContext;

void *init_bpm(enum BeatConfig);

int bpm_frame_has_beat(BeatContext *ctx, AVSDataContext *avsdc);

int refine_beat(BeatContext *, int, uint32_t);

#endif /* AVS_HEADER_BPM */
