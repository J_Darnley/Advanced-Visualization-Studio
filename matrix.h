/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AVS_HEADER_MATRIX
#define AVS_HEADER_MATRIX 1

void matrix_rotate(float matrix[], char m, float deg);
void matrix_translate(float matrix[], float x, float y, float z);
void matrix_multiply(float *dest, float src[]);
void matrix_apply(float matrix[], float x, float y, float z, float *out_x, float *out_y, float *out_z);

#endif /* AVS_HEADER_MATRIX */
