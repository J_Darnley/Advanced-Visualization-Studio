/*
 * Copyright (c) 2020 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************************************
 * Preamble
 *****************************************************************************/

#include <assert.h>
#include <stdint.h>
#include <limits.h>

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_vout.h>
#include <vlc_aout.h>
#include <vlc_filter.h>

#include <libavutil/audio_fifo.h>

#include "fft.h"
#include "window.h"

#include "avs.h"

/*****************************************************************************
 * Module descriptor
 *****************************************************************************/

static int Open(vlc_object_t *);
static void Close(vlc_object_t *);

#define WIDTH_TEXT "Video width"
#define WIDTH_LONGTEXT "The width of the visualization window, in pixels."

#define HEIGHT_TEXT "Video height"
#define HEIGHT_LONGTEXT "The height of the visualization window, in pixels."

#define PRESET_FILE_TXT "Preset file"
#define PRESET_FILE_LONGTXT "Preset to render"

#define MODULE_STRING "avs"

vlc_module_begin()
    set_shortname("avs")
    set_description("Advanced Visualization Studio")
    set_capability("visualization", 0)
    set_category(CAT_AUDIO)
    set_subcategory(SUBCAT_AUDIO_VISUAL)

    add_integer("avs-width", 640, WIDTH_TEXT, WIDTH_LONGTEXT, false)
    add_integer("avs-height", 480, HEIGHT_TEXT, HEIGHT_LONGTEXT, false)
    add_loadfile("avs-preset", "", PRESET_FILE_TXT, PRESET_FILE_LONGTXT, false)

    add_shortcut("avs")
    set_callbacks(Open, Close)
vlc_module_end()

/*****************************************************************************
 * Local prototypes
 *****************************************************************************/

struct filter_sys_t {
    vlc_thread_t thread;
    vlc_mutex_t mutex;
    vout_thread_t *vout;

    struct AVAudioFifo *fifo;

    AVSContext *actx;
    AVSOptionsContext options;

    /* FFT window parameters */
    window_param wind_param;
    /* internal window data */
    window_context wind_ctx;
    /* internal FFT data */
    fft_state *fft_state;

    bool quit;
};

static block_t *DoWork(filter_t *, block_t *);
static void *Thread(void *);

/*****************************************************************************
 * Open: open the visualizer
 *****************************************************************************/

static int Open(vlc_object_t * p_this)
{
    filter_t *p_filter = (filter_t *)p_this;
    int ret = VLC_SUCCESS;

    struct filter_sys_t *ctx = p_filter->p_sys = calloc(1, sizeof(struct filter_sys_t));
    if (!ctx)
        return VLC_ENOMEM;
    vlc_mutex_init(&ctx->mutex);
    ctx->quit = false;

    ctx->fifo = av_audio_fifo_alloc(AV_SAMPLE_FMT_S16, 2, p_filter->fmt_in.audio.i_rate);
    if (!ctx->fifo) {
        ret = VLC_ENOMEM;
        goto error;
    }

    int width = var_InheritInteger(p_filter , "avs-width");
    int height = var_InheritInteger(p_filter , "avs-height");

    AVSOptionsContext avs_options = {
        .structure_size = sizeof(avs_options),
        .api_version = AVS_API_VERSION,

        .width = width,
        .height = height,

        .log_level = AVS_LOG_INFO,
    };

    AVSContext *actx = NULL;
    if (avs_init(&actx, &avs_options)
            || avs_load_preset(actx, var_InheritString(p_filter, "avs-preset"))) {
        msg_Err(p_filter, "error opening context or preset");
        ret = VLC_EGENERIC;
        goto error;
    }
    ctx->actx = actx;
    ctx->options = avs_options;

    /* Fetch the FFT window parameters */
    window_get_param(VLC_OBJECT(p_filter), &ctx->wind_param);

    ctx->fft_state = visual_fft_init();
    if (!ctx->fft_state) {
        msg_Err(p_filter, "unable to initialize FFT transform");
        ret = VLC_EGENERIC;
        goto error;
    }

    if (!window_init(FFT_BUFFER_SIZE, &ctx->wind_param, &ctx->wind_ctx)) {
        msg_Err(p_filter, "unable to initialize FFT window");
        ret = VLC_EGENERIC;
        goto error;
    }

    /* Open the video output */
    video_format_t fmt = {
        .i_chroma = VLC_CODEC_RGB32,
        .i_width = width,
        .i_height = height,
        .i_visible_width = width,
        .i_visible_height = height,
        .i_sar_num = 1,
        .i_sar_den = 1,
    };
    ctx->vout = aout_filter_RequestVout(p_filter, NULL, &fmt);
    if (!ctx->vout) {
        msg_Err(p_filter, "no suitable vout module");
        ret = VLC_EGENERIC;
        goto error;
    }

    /* Create the thread */
    if (vlc_clone(&ctx->thread, Thread, p_filter, VLC_THREAD_PRIORITY_VIDEO)) {
        ret = VLC_EGENERIC;
        goto error;
    }

    p_filter->fmt_in.audio.i_format = VLC_CODEC_S16L;
    p_filter->fmt_in.audio.i_physical_channels = AOUT_CHANS_STEREO;
    p_filter->fmt_out.audio = p_filter->fmt_in.audio;
    p_filter->pf_audio_filter = DoWork;

    return ret;

error:

    window_close(&ctx->wind_ctx);
    fft_close(ctx->fft_state);

    aout_filter_RequestVout(p_filter, ctx->vout, NULL);

    avs_uninit(actx);
    av_audio_fifo_free(ctx->fifo);
    vlc_mutex_destroy(&ctx->mutex);
    free(ctx);

    return ret;
}

static block_t *DoWork(filter_t *p_filter, block_t *p_in_buf)
{
    struct filter_sys_t *ctx = p_filter->p_sys;
    void *buf[1] = { p_in_buf->p_buffer };
    assert(p_in_buf->i_nb_samples <= INT_MAX);

    vlc_mutex_lock(&ctx->mutex);
    int ret = av_audio_fifo_write(ctx->fifo, buf, p_in_buf->i_nb_samples);
    vlc_mutex_unlock(&ctx->mutex);

    if (ret < 0) {
        msg_Err(p_filter, "av_audio_fifo_write returned %d", ret);
    } else if (ret < (int)p_in_buf->i_nb_samples) {
        msg_Warn(p_filter, "av_audio_fifo_write did not store all samples (%d < %u)",
                ret, p_in_buf->i_nb_samples);
    }

    return p_in_buf;
}


/*****************************************************************************
 * Close: close the plugin
 *****************************************************************************/

static void Close( vlc_object_t *p_this )
{
    filter_t * p_filter = (filter_t *)p_this;
    struct filter_sys_t *ctx = p_filter->p_sys;

    /* Terminate the thread. */
    vlc_mutex_lock(&ctx->mutex);
    ctx->quit = true;
    vlc_mutex_unlock(&ctx->mutex);
    vlc_join(ctx->thread, NULL);

    /* Free the ressources */
    window_close(&ctx->wind_ctx);
    fft_close(ctx->fft_state);

    aout_filter_RequestVout(p_filter, ctx->vout, NULL);

    avs_uninit(ctx->actx);
    av_audio_fifo_free(ctx->fifo);
    vlc_mutex_destroy(&ctx->mutex);
    free(ctx);
}

/*****************************************************************************
 * Render thread
 *****************************************************************************/

static void render(filter_t *p_filter, int16_t *audio_buffer, size_t samples, mtime_t pts)
{
    struct filter_sys_t *ctx = p_filter->p_sys;

    /* First, get a new picture */
    picture_t *p_outpic = vout_GetPicture(ctx->vout);
    if (!p_outpic) {
        msg_Err(p_filter, "error allocating output picture");
        return;
    }

    p_outpic->b_progressive = true;
    p_outpic->date = pts;

    uint8_t waveform[3][576] = {{0}}, spectrum[3][576] = {0};
    AVSDataContext avsdc = {
        .waveform = { waveform[0], waveform[1], waveform[2] },
        .spectrum = { spectrum[0], spectrum[1], spectrum[2] },
    };

    /* Import waveform data. */
    const int16_t *src = audio_buffer;
    for (size_t i = 0; i < 576 && i < samples; i++) {
        int16_t l = src[2*i+0];
        int16_t r = src[2*i+1];
        int16_t c = (l + r) / 2;
        waveform[0][i] = (l / 256) + 128;
        waveform[1][i] = (r / 256) + 128;
        waveform[2][i] = (c / 256) + 128;
    }

    float fft_dst[3][FFT_BUFFER_SIZE] = {0}; /* Raw FFT result */
    int16_t fft_src[3][FFT_BUFFER_SIZE] = {0};  /* Buffer on which we perform the FFT (first channel) */

    for (size_t i = 0; i < FFT_BUFFER_SIZE && i < samples; i++) {
        int16_t l = src[2*i+0];
        int16_t r = src[2*i+1];
        int16_t c = (l + r) / 2;
        fft_src[0][i] = l;
        fft_src[1][i] = r;
        fft_src[2][i] = c;
    }

    window_scale_in_place(fft_src[0], &ctx->wind_ctx);
    window_scale_in_place(fft_src[1], &ctx->wind_ctx);
    window_scale_in_place(fft_src[2], &ctx->wind_ctx);

    fft_perform(fft_src[0], fft_dst[0], ctx->fft_state);
    fft_perform(fft_src[1], fft_dst[1], ctx->fft_state);
    fft_perform(fft_src[2], fft_dst[2], ctx->fft_state);

    for (size_t i = 0; i < FFT_BUFFER_SIZE && i < 576; i++) {
        spectrum[0][i] = sqrtf(fft_dst[0][i]) / 32768;
        spectrum[1][i] = sqrtf(fft_dst[1][i]) / 32768;
        spectrum[2][i] = sqrtf(fft_dst[2][i]) / 32768;
    }

    if (avs_render_frame(ctx->actx, &avsdc, pts*1000/CLOCK_FREQ)) {
        msg_Err(p_filter, "error rendering");
    }

    else {
        int *frame = avs_get_frame(ctx->actx);
        for (int y = 0; y < ctx->options.height; y++)
            memcpy(p_outpic->p[0].p_pixels + y * p_outpic->p[0].i_pitch,
                    frame + y * ctx->options.width,
                    sizeof(int) * ctx->options.width);
    }

    vout_PutPicture(ctx->vout, p_outpic);
    return;
}

static void *Thread(void *data)
{
    filter_t *p_filter = data;
    struct filter_sys_t *ctx = p_filter->p_sys;
    unsigned sample_rate = p_filter->fmt_in.audio.i_rate;
    int16_t audio_buffer[2*4800] = {0};
    void *buf[1] = { audio_buffer };

    mtime_t prev_time = 0;
    mtime_t start_time = mdate();
    int64_t total_frames = 0;
    while (true) {
        vlc_mutex_lock(&ctx->mutex);

        /* record date */
        mtime_t now = mdate();
        /* to quit */
        bool quit = ctx->quit;
        /* how many samples since last read */
        const int samples_to_read = (sample_rate / 60);
        /* try to read the desired samples */
        int ret = av_audio_fifo_read(ctx->fifo, buf, samples_to_read);
        /* TODO: peek and skip the right samples */

        vlc_mutex_unlock(&ctx->mutex);

        if (quit)
            break;

        if (ret < 0) {
            msg_Err(p_filter, "av_audio_fifo_read returned %d", ret);
        } else {
            if (ret < samples_to_read)
                msg_Warn(p_filter, "av_audio_fifo_read did not get all samples (%d < %d)",
                        ret, samples_to_read);
            /* Fill in remaining samples. */
            if (ret < 576) {
                memset(audio_buffer + 2*ret * sizeof audio_buffer[0], 0,
                        2*(576 - ret) * sizeof audio_buffer[0]);
                ret = 576;
            }

            render(p_filter, audio_buffer, ret, now);
        }

        mwait(start_time + (++total_frames) * CLOCK_FREQ / 60);
        prev_time = now;
    }

    return NULL;
}
