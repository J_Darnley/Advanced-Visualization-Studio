/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int num_colours;
    int colour[16];
    int colour_pos;
    int xp, yp;
    int x_move, y_move;
    int spacing;
    int blend;
} DotGridContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    DotGridContext *dgctx = ctx->priv;
    int pos = 0;
    R32(dgctx->num_colours);
    int temp = read_colours(buf + pos, buf_len - pos, dgctx->num_colours, 16, dgctx->colour);
    if (temp < 0)
        return -1;
    pos += temp;
    R32(dgctx->spacing);
    R32(dgctx->x_move);
    R32(dgctx->y_move);
    R32(dgctx->blend);
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    DotGridContext *dgctx = ctx->priv;
    int colour = interpolate_colours_64(dgctx->num_colours, dgctx->colour, &dgctx->colour_pos);
    int xp = dgctx->xp;
    int yp = dgctx->yp;
    int spacing = dgctx->spacing;

    if (spacing < 2)
        spacing = 2;

    while (xp < 0)
        xp += spacing * 256;

    while (yp < 0)
        yp += spacing * 256;

    int sx = (xp >> 8) % spacing;
    int sy = (yp >> 8) % spacing;

    frame += sy * w;

    if (dgctx->blend == 1)
        for (int y = sy; y < h; y += spacing) {
            for (int x = sx; x < w; x += spacing)
                frame[x] = blend_pixel_add(frame[x], colour);
            frame += w * spacing;
        }
    else if (dgctx->blend == 2)
        for (int y = sy; y < h; y += spacing) {
            for (int x = sx; x < w; x += spacing)
                frame[x] = blend_pixel_avg(frame[x], colour);
            frame += w * spacing;
        }
    else if (dgctx->blend == 3)
        for (int y = sy; y < h; y += spacing) {
            for (int x = sx; x < w; x += spacing)
                blend_pixel(frame + x, colour, ctx->blend_mode);
            frame += w * spacing;
        }
    else
        for (int y = sy; y < h; y += spacing) {
            for (int x = sx; x < w; x += spacing)
                frame[x] = colour;
            frame += w * spacing;
        }

    dgctx->xp = xp + dgctx->x_move;
    dgctx->yp = yp + dgctx->y_move;

    return 0;
}

Component r_dotgrid = {
    .name = "Render / Dot Grid",
    .code = 17,
    .priv_size = sizeof(DotGridContext),
    .load_config = load_config,
    .render = render,
};
