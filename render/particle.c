/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int enabled;
    int colour;
    int maxdist;
    int size1;
    int size2;
    int blend;
    int s_pos;
    double c[2];
    double v[2];
    double p[2];
} ParticleContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    ParticleContext *pctx = ctx->priv;
    int pos = 0;
    R32(pctx->enabled);
    R32(pctx->colour);
    R32(pctx->maxdist);
    R32(pctx->size1);
    R32(pctx->size2);
    R32(pctx->blend);
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    ParticleContext *pctx = ctx->priv;

    if (!pctx->enabled)
        return 0;

    int xp, yp;
    int ss = MIN(h / 2, (w * 3) / 8);

    if (is_beat) {
        pctx->c[0] = ((rand() % 33) - 16) / 48.0;
        pctx->c[1] = ((rand() % 33) - 16) / 48.0;
    }

    pctx->v[0] -= 0.004 * (pctx->p[0] - pctx->c[0]);
    pctx->v[1] -= 0.004 * (pctx->p[1] - pctx->c[1]);

    pctx->p[0] += pctx->v[0];
    pctx->p[1] += pctx->v[1];

    pctx->v[0] *= 0.991;
    pctx->v[1] *= 0.991;

    xp = pctx->p[0] * ss * pctx->maxdist / 32.0 + w / 2;
    yp = pctx->p[1] * ss * pctx->maxdist / 32.0 + h / 2;

    if (is_beat && (pctx->enabled & 2))
        pctx->s_pos = pctx->size2;

    int sz = pctx->s_pos;

    pctx->s_pos = (pctx->s_pos + pctx->size1) / 2;
    if (sz <= 1) {
        frame += yp * w + xp;
        if (xp >= 0 && yp >= 0 && xp < w && yp < h) {
            if (pctx->blend == 0)
                frame[0] = pctx->colour;
            else if (pctx->blend == 2)
                frame[0] = blend_pixel_avg(frame[0], pctx->colour);
            else if (pctx->blend == 3)
                blend_pixel(frame, pctx->colour, ctx->blend_mode);
            else
                frame[0] = blend_pixel_add(frame[0], pctx->colour);
        }
        return 0;
    }

    if (sz > 128)
        sz = 128;

    float md = sz * sz * 0.25;
    yp -= sz / 2;
    for (int y = 0; y < sz; y++) {
        if (yp + y >= 0 && yp + y < h) {
            float yd = y - sz * 0.5;
            float l = sqrtf(md - yd * yd);
            int xs = (l + 0.99); /* Could this be replaced with ceil()? */
            if (xs < 1)
                xs = 1;
            int xe = xp + xs;
            if (xe > w)
                xe = w;
            int xst = xp - xs;
            if (xst < 0)
                xst = 0;
            int *f = &frame[xst + (yp + y) * w];

            if (pctx->blend == 0)
                for (int x = xst; x < xe; x++) {
                    f[0] = pctx->colour;
                    f++;
                }
            else if (pctx->blend == 2)
                for (int x = xst; x < xe; x++) {
                    f[0] = blend_pixel_avg(f[0], pctx->colour);
                    f++;
                }
            else if (pctx->blend == 3)
                for (int x = xst; x < xe; x++) {
                    blend_pixel(f, pctx->colour, ctx->blend_mode);
                    f++;
                }
            else
                for (int x = xst; x < xe; x++) {
                    f[0] = blend_pixel_add(f[0], pctx->colour);
                    f++;
                }
        }
    }

    return 0;
}

Component r_particle = {
    .name = "Render / Moving Particle",
    .code = 8,
    .priv_size = sizeof(ParticleContext),
    .load_config = load_config,
    .render = render,
};
