/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int which_ch;
    int enabled;
    int colour;
    int blend, blend_avg;
    int num_bands;
    int x;
    int old_h;
} TimescopeContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    TimescopeContext *scope = ctx->priv;
    int pos = 0;

    R32(scope->enabled);
    R32(scope->colour);
    R32(scope->blend);
    R32(scope->blend_avg);
    R32(scope->which_ch);
    R32(scope->num_bands);

    if (scope->which_ch == 0)
        ctx->which_channel = VIS_LEFT;
    else if (scope->which_ch == 1)
        ctx->which_channel = VIS_RIGHT;
    else
        ctx->which_channel = VIS_CENTER;

    return 0;
}

static int render(ComponentContext *ctx, uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    TimescopeContext *scope = ctx->priv;

    if (!scope->enabled)
        return 0;

    frame += scope->x;
    scope->x = (scope->x + 1) % w;

    int r = CR_TO_I(scope->colour);
    int g = CG_TO_I(scope->colour);
    int b = CB_TO_I(scope->colour);

    for (int i = 0; i < h; i++) {
        int c = vis_data[(i * scope->num_bands) / h];
        c = I3_TO_I((r*c)>>8, (g*c)>>8, (b*c)>>8);

        if (scope->blend == 2)
            blend_pixel(frame, c, ctx->blend_mode);
        else if (scope->blend == 1)
            *frame = blend_pixel_add(*frame, c);
        else if (scope->blend_avg)
            *frame = blend_pixel_avg(*frame, c);
        else
            *frame = c;

        frame += w;
    }

    return 0;
}

Component r_timescope = {
    .name = "Rander / Timescope",
    .code = 39,
    .priv_size = sizeof(TimescopeContext),
    .load_config = load_config,
    .render = render,
};
