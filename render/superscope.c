/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "blend.h"
#include "components.h"
#include "common.h"
#include "expressions.h"
#include "linedraw.h"
#include "pixel.h"

typedef struct {
    const char *expr_point;
    const char *expr_beat;
    const char *expr_frame;
    const char *expr_init;
    int num_colours;
    int colour[16];
    int colour_pos;
    int mode;

    int inited;
    void *expr_context;
} SuperscopeContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    SuperscopeContext *ssctx = ctx->priv;
    void *expr_ctx;
    int pos = 0;
    int temp;

    expr_ctx = expr_init(ctx->actx);
    if (!expr_ctx)
        return -1;

    if (*buf == 1) {
        int ret = 0;
        pos++;

        ret = expr_load_rstring(expr_ctx, &ssctx->expr_point, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &ssctx->expr_frame, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &ssctx->expr_beat, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &ssctx->expr_init, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;
    } else {
        /* read fixed len */
            ERROR("fixed-length expressions not yet implemented\n");
    }

    ssctx->expr_context = expr_ctx;

    R32(temp);
    if (temp & 4)
        ctx->which_vis_type = VIS_SPECTRUM;
    if ((temp & 3) == 0)
        ctx->which_channel = VIS_LEFT;
    else if ((temp & 3) == 1)
        ctx->which_channel = VIS_RIGHT;
    else
        ctx->which_channel = VIS_CENTER;

    R32(ssctx->num_colours );
    temp = read_colours(buf + pos, buf_len - pos, ssctx->num_colours, 16, ssctx->colour);
    if (temp < 0)
        return -1;
    pos += temp;

    R32(ssctx->mode);

    const char *arg_string = "local v, i, skip = ...;\nlocal x, y;\n";
    const char *ret_string = "return x, y, skip, red, green, blue, linesize, drawmode;\n";

    int ret = 0;
    ret |= expr_compile(expr_ctx, "__avs_internal_init", ssctx->expr_init);
    ret |= expr_compile(expr_ctx, "__avs_internal_frame", ssctx->expr_frame);
    ret |= expr_compile(expr_ctx, "__avs_internal_beat", ssctx->expr_beat);
    ret |= expr_compile2(expr_ctx, "__avs_internal_point",
            arg_string, ssctx->expr_point, ret_string);

    return ret;
}

static void print_config(ComponentContext *ctx, unused int indent)
{
    SuperscopeContext *ssctx = ctx->priv;

    fprintf(stderr, "    expression init = %s", ssctx->expr_init);
    fprintf(stderr, "    expression frame = %s", ssctx->expr_frame);
    fprintf(stderr, "    expression beat = %s", ssctx->expr_beat);
    fprintf(stderr, "    expression point = %s", ssctx->expr_point);

    return;
}

static int make_colour(double red, double green, double blue)
{
    red = CLIP(red, 0.0, 1.0);
    green = CLIP(green, 0.0, 1.0);
    blue = CLIP(blue, 0.0, 1.0);
    return I3_TO_I(red * 255.0, green * 255.0, blue * 255.0);
}

static int render(ComponentContext *ctx, uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    SuperscopeContext *ssctx = ctx->priv;
    int lx = 0, ly = 0;
    int colour = interpolate_colours_64(ssctx->num_colours, ssctx->colour, &ssctx->colour_pos);

    /* TODO: correct colour use. */

#define SET(a,b) expr_variable_set(ssctx->expr_context, (a), (b))
#define GET(a) expr_variable_get(ssctx->expr_context, (a))
#define GET_NEXT(a) expr_get_return(ssctx->expr_context, expr_result++)

    SET("w", w);
    SET("h", h);
    SET("b", is_beat ? 1.0 : 0.0);
    SET("red", CR_TO_I(colour) / 255.0);
    SET("green", CG_TO_I(colour) / 255.0);
    SET("blue", CB_TO_I(colour) / 255.0);
    SET("skip", 0.0);
    SET("linesize", blend_mode_get_width(ctx->blend_mode));
    SET("drawmode", ssctx->mode);

    if (!ssctx->inited) {
        if(expr_execute(ssctx->expr_context, "__avs_internal_init", ssctx->expr_init))
            return -1;
        ssctx->inited = 1;
    }
    if (expr_execute(ssctx->expr_context, "__avs_internal_frame", ssctx->expr_frame))
        return -1;

    if (is_beat)
        if (expr_execute(ssctx->expr_context, "__avs_internal_beat", ssctx->expr_beat))
            return -1;

    /* If there is no point expression defined, then there is no need to run
     * this loop because no work will be done. */
    if (!ssctx->expr_point)
        return 0;

    int l = GET("n");
    if (l > 128 * 1024)
        WARNING("a very large number of points set (%d), drawing anyway\n", l);
    for (int a = 0; a < l; a++) {
        int x, y;
        double r = (a * 575.0) / l;
        double s1 = r - (int)r;
        double yr = vis_data[(int)r] * (1.0 - s1) + vis_data[(int)r + 1] * s1;

        if (expr_execute2(ssctx->expr_context, "__avs_internal_point", 3,
                    yr / 128.0 - 1.0, a / (double)(l - 1), 0.0))
            return -1;
        int expr_result = 1;

        x = (GET_NEXT("x") + 1.0) * w * 0.5;
        y = (GET_NEXT("y") + 1.0) * h * 0.5;

        if (GET_NEXT("skip") < 0.00001) {
            double red = GET_NEXT("red");
            double green = GET_NEXT("green");
            double blue = GET_NEXT("blue");
            int this_colour = make_colour(red, green, blue);
            int lw = GET_NEXT("linesize") + 0.5;
            if (GET_NEXT("drawmode") < 0.00001) {
                if (y >= 0 && y < h && x >= 0 && x < w)
                    blend_pixel(frame + y * w + x, this_colour, ctx->blend_mode);
            } else {
                if (a)
                    line(frame, lx, ly, x, y, w, h, this_colour,
                            blend_mode_set_width(ctx->blend_mode, lw));
            }
        }

        lx = x;
        ly = y;

        expr_clear_return(ssctx->expr_context);
    }

    return 0;
}

static void uninit(ComponentContext *ctx)
{
    SuperscopeContext *ssctx = ctx->priv;

    FREE((void *)ssctx->expr_init);
    FREE((void *)ssctx->expr_frame);
    FREE((void *)ssctx->expr_beat);
    FREE((void *)ssctx->expr_point);

    expr_uninit(ssctx->expr_context);
}

Component r_superscope = {
    .name = "Render / Superscope",
    .code = 36,
    .priv_size = sizeof(SuperscopeContext),
    .load_config = load_config,
    .print_config = print_config,
    .render = render,
    .uninit = uninit,
};
