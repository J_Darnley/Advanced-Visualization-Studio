/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "linedraw.h"

typedef struct {
    int num_colours;
    int colour[16];
    int colour_pos;
    float r1;
} RotatingStarContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    RotatingStarContext *rsctx = ctx->priv;
    int pos = 0;
    R32(rsctx->num_colours);
    int temp = read_colours(buf + pos, buf_len - pos, rsctx->num_colours, 16, rsctx->colour);
    if (temp < 0)
        return -1;
    return 0;
}

static int render(ComponentContext *ctx, uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    RotatingStarContext *rsctx = ctx->priv;
    int colour = interpolate_colours_64(rsctx->num_colours, rsctx->colour, &rsctx->colour_pos);
    int x = cosf(rsctx->r1) * w / 4.0;
    int y = sinf(rsctx->r1) * h / 4.0;
    for (int c = 0; c < 2; c++) {
        float r2 = -rsctx->r1;
        int a = x;
        int b = y;
        int s = 0;

        if (c)
            vis_data = ctx->actx->avsdc.spectrum.right;
        else
            vis_data = ctx->actx->avsdc.spectrum.left;

        for (int l = 3; l < 14; l++)
            if (vis_data[l] > s &&
                    vis_data[l] > vis_data[l + 1] + 4 &&
                    vis_data[l] > vis_data[l - 1] + 4)
                s = vis_data[l];

        if (c) {
            a = -a;
            b = -b;
        }

        float vw = w / 8.0 * (s + 9) / 88.0;
        float vh = h / 8.0 * (s + 9) / 88.0;

        int nx = cosf(r2) * vw;
        int ny = sinf(r2) * vh;

        int lx = w / 2 + a + nx;
        int ly = h / 2 + b + ny;

        r2 += M_PI * 4.0 / 5.0;

        for (int t = 0; t < 5; t++) {
            int nx = cosf(r2) * vw + w / 2 + a;
            int ny = sinf(r2) * vh + h / 2 + b;

            r2 += M_PI * 4.0 / 5.0;
            line(frame, lx, ly, nx, ny, w, h, colour, ctx->blend_mode);
            lx = nx;
            ly = ny;
        }
    }

    rsctx->r1 += 0.1;

    return 0;
}

Component r_rotstar = {
    .name = "Render / Rotating Stars",
    .code = 13,
    .priv_size = sizeof(RotatingStarContext),
    .load_config = load_config,
    .render = render,
};
