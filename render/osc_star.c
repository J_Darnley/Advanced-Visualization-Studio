/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "linedraw.h"

typedef struct {
    int x_pos;
    int num_colours;
    int colour[16];
    int size;
    int rotation;
    int colour_pos;
    float m_r;
} OSCStarContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    OSCStarContext *octx = ctx->priv;
    int temp;
    int pos = 0;

    R32(temp);
    octx->x_pos = temp >> 4;
    ctx->which_vis_type = VIS_WAVEFORM;
    if (((temp >> 2) & 3) == 0)
        ctx->which_channel = VIS_LEFT;
    else if(((temp >> 2) & 3) == 1)
        ctx->which_channel = VIS_RIGHT;
    else
        ctx->which_channel = VIS_CENTER;

    R32(octx->num_colours);
    temp = read_colours(buf + pos, buf_len - pos, octx->num_colours, 16, octx->colour);
    if (temp < 0)
        return -1;
    pos += temp;

    R32(octx->size);
    R32(octx->rotation);

    /* TODO interpolate colours. */

    return 0;
}

static int render(ComponentContext *ctx, uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    OSCStarContext *octx = ctx->priv;
    int colour = interpolate_colours_64(octx->num_colours, octx->colour, &octx->colour_pos);

    /* TODO: select colour. */

    float s = octx->size / 32.0;
    int c_x;
    int is = MIN(h * s, w * s);
    if (octx->x_pos == 2)
        c_x = w / 2;
    else if (octx->x_pos == 0)
        c_x = w / 4;
    else
        c_x = (3 * w) / 4;

    for (int q = 0, ii = 0; q < 5; q++) {
        float s = sinf(octx->m_r + q * (M_PI * 2.0 / 5.0));
        float c = cosf(octx->m_r + q * (M_PI * 2.0 / 5.0));
        float p = 0.0;
        int lx = c_x;
        int ly = h / 2;
        int t = 64;
        float dp = is / (float)t;
        float dfactor = 1.0 / 1024.0;
        float hw = is;
        while (t--) {
            float ale = (vis_data[ii] - 128) * dfactor * hw;
            int x = c_x   + (c * p) - (s * ale);
            int y = h / 2 + (s * p) + (c * ale);

            /* The old code checks that either end of the line is within the
             * frame boundaries before calling line. */
            line(frame, x, y, lx, ly, w, h, colour, ctx->blend_mode);

            ii++;
            lx = x;
            ly = y;
            p += dp;
            dfactor -= ((1.0 / 1024.0) - (1.0 / 128.0)) / 64.0;
        }
    }

    octx->m_r += 0.01 * octx->rotation;
    if (octx->m_r >= M_PI * 2.0)
        octx->m_r -= M_PI * 2.0;

    return 0;
}

Component r_oscstar = {
    .name = "Render / Oscilloscope Star",
    .code = 2,
    .priv_size = sizeof(OSCStarContext),
    .load_config = load_config,
    .render = render,
};
