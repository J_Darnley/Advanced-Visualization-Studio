/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "linedraw.h"
#include "pixel.h"

struct Point {
    int x, y;
};

typedef struct {
    int enabled;
    int colour[2];
    int mode;
    int last_a;
    int lx[2][2], ly[2][2]; /* Are these points? */
    double r_v[2];
    double v[2];
    double dir[2];
    struct Point lp[2][2];
} BassSpinContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    BassSpinContext *bspin = ctx->priv;
    int pos = 0;
    R32(bspin->enabled);
    R32(bspin->colour[0]);
    R32(bspin->colour[1]);
    R32(bspin->mode);
    bspin->r_v[0] = M_PI;
    bspin->dir[0] = -1.0;
    bspin->dir[1] = 1.0;
    return 0;
}

#define SWAP(a,b) \
    temp = (a); \
    (a) = (b); \
    (b) = temp;

#define F16(a) ((a) << 16)

static void my_triangle(int *frame, struct Point point[3], int width, int height, int colour, int blend_mode)
{
    int y_max, dx1, dx2, x1, x2;

    for (int y = 0; y < 2; y++) {
        struct Point temp;
        if (point[0].y > point[1].y) {
            SWAP(point[1], point[0])
        }
        if (point[1].y > point[2].y) {
            SWAP(point[2], point[1])
        }
    }

    x1 = x2 = F16(point[0].x);
    if (point[0].y < point[1].y)
        dx1 = F16(point[1].x - point[0].x) / (point[1].y - point[0].y);
    else
        dx1 = 0;

    if (point[0].y < point[2].y)
        dx2 = F16(point[2].x - point[0].x) / (point[2].y - point[0].y);
    else
        dx2 = 0;

    frame += point[0].y * width;
    y_max = MIN(point[2].y, height);

    for (int y = point[0].y; y < y_max; y++) {
        if (y == point[1].y) {
            if (y == point[2].y)
                return;
            x1 = F16(point[1].x);
            dx1 = F16(point[2].x - point[1].x) / (point[2].y - point[1].y);
        }

        if (y >= 0) {
            int x = (MIN(x1, x2) - 32768) >> 16;
            int xl = ((MAX(x1, x2) + 32768) >> 16) - x;

            if (xl < 0)
                xl = -xl;

            if (!xl)
                xl++;

            int *t = frame + x;
            if (x < 0) {
                t -= x;
                xl -= x;
            }

            if (x + xl >= width)
                xl = width - x;

            if (xl > 0)
                while (xl--)
                    blend_pixel(t++, colour, blend_mode);
        }

        frame += width;
        x1 += dx1;
        x2 += dx2;
    }
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    BassSpinContext *bspin = ctx->priv;

    for (int y = 0; y < 2; y++) {
        if (!(bspin->enabled & (1 << y)))
            continue;

        if (y)
            vis_data = ctx->actx->avsdc.spectrum.right;
        else
            vis_data = ctx->actx->avsdc.spectrum.left;

        int xp, yp;
        int ss = MIN(h / 2, (w * 3) / 8);
        float s = ss;
        int c_x = (!y) ? w/2 - ss/2 : w/2 + ss/2;
        int a = 0, d = 0;
        int oc6 = bspin->colour[y];
        struct Point *lp = bspin->lp[y]; /* An idea to clean up the use of lx
                                          * and ly below.  I'm not sure it is
                                          * any cleaner though. */

        for (int x = 0; x < 44; x++)
            d += vis_data[x];

        a = d * 512 / (bspin->last_a + 30 * 256);
        bspin->last_a = d; /* This overwrites it for each channel. */

        if (a > 255)
            a = 255;

        bspin->v[y] = 0.7 * MAX(a - 104, 12) / 96.0 + 0.3 * bspin->v[y];
        bspin->r_v[y] += M_PI / 6.0 * bspin->v[y] * bspin->dir[y];

        s *= a / 256.0;
        yp = sin(bspin->r_v[y]) * s;
        xp = cos(bspin->r_v[y]) * s;

        struct Point current_p = { c_x + xp, h/2 + yp };
        struct Point current_n = { c_x - xp, h/2 - yp };

        if (bspin->mode == 0) {
            /* Should this draw all three sides of the triangle? */
            if (lp[0].x || lp[0].y)
                line(frame, lp[0].x, lp[0].y, current_p.x, current_p.y, w, h, oc6, ctx->blend_mode);
            line(frame, c_x, h/2, current_p.x, current_p.y, w, h, oc6, ctx->blend_mode);
            if (lp[1].x || lp[1].y)
                line(frame, lp[1].x, lp[1].y, current_n.x, current_n.y, w, h, oc6, ctx->blend_mode);
            line(frame, c_x, h/2, current_n.x, current_n.y, w, h, oc6, ctx->blend_mode);
        } else {
            if (lp[0].x || lp[0].y) {
                struct Point p[3] = { { c_x, h/2}, lp[0], current_p };
                my_triangle(frame, p, w, h, oc6, ctx->blend_mode);
            }
            if (lp[1].x || lp[1].y) {
                struct Point p[3] = { { c_x, h/2}, lp[1], current_n };
                my_triangle(frame, p, w, h, oc6, ctx->blend_mode);
            }
        }

        lp[0] = current_p;
        lp[1] = current_n;
    }

    return 0;
}

Component r_bspin = {
    .name = "Render / Bass Spin",
    .code = 7,
    .priv_size = sizeof(BassSpinContext),
    .load_config = load_config,
    .render = render,
};
