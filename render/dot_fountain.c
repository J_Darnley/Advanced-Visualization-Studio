/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "matrix.h"
#include "pixel.h"

#define NUM_ROT_DIV 30
#define NUM_ROT_HEIGHT 256

typedef struct {
    float r, dr;
    float h, dh;
    float ax;
    float ay;
    int c;
} FountainPoint;

typedef struct {
    FountainPoint points[NUM_ROT_HEIGHT][NUM_ROT_DIV];
    float r;
    int colour_tab[64];
    int rotvel;
    int angle;
    int colour[5];
} DotFountainContext;

static void init_colour_table(int colour[5], int colour_tab[64])
{

    for (int t = 0; t < 4; t++) {
        int c0 = colour[t];
        int c1 = colour[t + 1];

        int r = CR_TO_I(c0) << 16;
        int g = CG_TO_I(c0) << 16;
        int b = CB_TO_I(c0) << 16;

        int dr = ((CR_TO_I(c1) - CR_TO_I(c0)) << 16) / 16;
        int dg = ((CG_TO_I(c1) - CG_TO_I(c0)) << 16) / 16;
        int db = ((CB_TO_I(c1) - CB_TO_I(c0)) << 16) / 16;

        for (int x = 0; x < 16; x++) {
            colour_tab[t * 16 + x] = I3_TO_I(r >> 16, g >> 16, b >> 16);
            r += dr;
            g += dg;
            b += db;
        }
    }
}

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    DotFountainContext *dfctx = ctx->priv;
    int pos = 0;
    R32(dfctx->rotvel);

    for (int i = 0; i < 5; i++)
        R32(dfctx->colour[i]);

    R32(dfctx->angle);

    init_colour_table(dfctx->colour, dfctx->colour_tab);

    ctx->which_channel = VIS_CENTER;
    ctx->which_vis_type = VIS_WAVEFORM;

    return 0;
}

static int render(ComponentContext *ctx, uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    DotFountainContext *dfctx = ctx->priv;
    float matrix[16], matrix2[16];

    matrix_rotate(matrix, 2, dfctx->r);
    matrix_rotate(matrix2, 1, dfctx->angle);
    matrix_multiply(matrix, matrix2);
    matrix_translate(matrix2, 0.0, -20.0, 400.0);
    matrix_multiply(matrix, matrix2);

    FountainPoint pb[NUM_ROT_DIV];
    FountainPoint *in, *out;

    memcpy(pb, &dfctx->points[0], sizeof(pb));

    for (int fo = NUM_ROT_HEIGHT-2; fo >= 0; fo--) {
        float booga = 1.3 / (fo + 100);
        in = &dfctx->points[fo][0];
        out = &dfctx->points[fo+1][0];

        for (int p = 0; p < NUM_ROT_DIV; p++) {
            *out = *in;
            out->r  += out->dr;
            out->dh += 0.05;
            out->dr += booga;
            out->h  += out->dh;
            out++;
            in++;
        }
    }

    out = &dfctx->points[0][0];
    in = pb;

    for (int p = 0; p < NUM_ROT_DIV; p++) {
        /* I wonder if this should select the maximum of several samples.
         * Something similar to what the Dot Plane component does. */
        float t = *vis_data * 1.25 - 64.0;
        vis_data++;

        if (is_beat)
            t += 128.0;

        /* I removed the limit-to-255 from here.  If a really loud beat wants to
         * send dots flying off the screen, why not let it? */

        float dr = t / 200.0;
        if (dr < 0)
            dr = -dr;
        dr += 1.0;

        out->h = 250;
        out->dh = -dr * (100.0 + (out->dh - in->dh)) / 100.0 * 2.8;

        int ct = CLIP(t/4, 0, 63);
        out->c = dfctx->colour_tab[ct];

        float a = p * M_PI * 2.0 / NUM_ROT_DIV;
        out->ax = sinf(a);
        out->ay = cosf(a);
        out->r  = 1.0;
        out->dr = 0.0;
        out++;
        in++;
    }

    float adj = MIN(w * 440.0 / 640.0, h * 440.0 / 480.0);
    in = &dfctx->points[0][0];

    for (int fo = 0; fo < NUM_ROT_HEIGHT; fo++)
        for (int p = 0; p < NUM_ROT_DIV; p++) {
            float x, y, z;
            matrix_apply(matrix, in->ax * in->r, in->h, in->ay * in->r, &x, &y, &z);
            z = adj / z;

            if (z > 0.0000001) {
                int ix = (x * z) + w/2;
                int iy = (y * z) + h/2;
                if (iy >= 0 && iy < h && ix >= 0 && ix < w)
                    blend_pixel(frame + iy * w + ix, in->c, ctx->blend_mode);
            }

            in++;
        }
    dfctx->r += dfctx->rotvel / 5.0;
    if (dfctx->r >= 360.0)
        dfctx->r -= 360.0;
    if (dfctx->r < 0.0)
        dfctx->r += 360.0;

    return 0;
}

Component r_dotfountain = {
    .name = "Render / Dot Fountain",
    .code = 19,
    .priv_size = sizeof(DotFountainContext),
    .load_config = load_config,
    .render = render,
};
