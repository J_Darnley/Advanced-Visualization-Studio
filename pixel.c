/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>

#include "blend.h"
#include "common.h"
#include "pixel.h"

uint8_t blend_table[256][256];

void init_blend_table(void)
{
    for (int i = 0; i < 256; i++)
        for (int j = 0; j < 256; j++) {
            double val_mul_f  = (i * j) / 255.0;
            int val_mul_i  = rint(val_mul_f);
            blend_table[i][j] = val_mul_i;
        }
}
