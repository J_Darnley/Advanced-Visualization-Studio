/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Layout of values in g_line_blend_mode:
 * - Least significant byte represents the blend mode (replace, add, max...).
 * - Second byte represents the "amount" for the adjustable blend.
 * - Third byte represents the line width.
 * - Most significant byte is unused except for the most significant bit which
 *   is used as an "enable" flag in the render mode component.
 */

#ifndef AVS_HEADER_BLEND
#define AVS_HEADER_BLEND 1

#include "attributes.h"
#include "common.h"

enum BlendMode {
    BM_REPLACE = 0,
    BM_ADDITIVE,
    BM_MAXIMUM,
    BM_AVERAGE,
    BM_SUBTRACTIVE1,
    BM_SUBTRACTIVE2,
    BM_MULTIPLY,
    BM_ADJUSTABLE,
    BM_XOR,
    BM_MINIMUM,
};

unused static inline int blend_mode_default(void)
{
    return 1 << 16;
}

unused static inline int blend_mode_set_width(int blend_mode, int width)
{
    const int MAX_WIDTH = 32767;
    width = CLIP(width, 1, MAX_WIDTH);
    return (blend_mode & 0xffff) | (width << 16);
}

unused static inline int blend_mode_get_width(int blend_mode)
{
    const int MAX_WIDTH = 32767;
    return (blend_mode >> 16) & MAX_WIDTH;
}

unused static inline int blend_mode_set_adj(int blend_mode, int adj)
{
    return (blend_mode & ~0xff00) | ((adj & 0xff) << 8);
}

unused static inline int blend_mode_get_adj(int blend_mode)
{
    return (blend_mode >> 8) & 0xff;
}

unused static inline int blend_mode_set_mode(int blend_mode, enum BlendMode mode)
{
    return (blend_mode & ~0xff) | (mode & 0xff);
}

unused static inline enum BlendMode blend_mode_get_mode(int blend_mode)
{
    return blend_mode & 0xff;
}

#endif /* AVS_HEADER_BLEND */
