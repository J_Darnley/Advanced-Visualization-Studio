/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AVS_HEADER_COMPONENTS
#define AVS_HEADER_COMPONENTS 1

#include <stdlib.h>
#include <stdint.h>

#include "avs.h"

typedef struct Component Component;
typedef struct ComponentContext ComponentContext;

enum VisData {
    VIS_SPECTRUM = 1,
    VIS_WAVEFORM = 2,
    VIS_TYPE_MASK = VIS_SPECTRUM|VIS_WAVEFORM,
    VIS_LEFT = 4,
    VIS_RIGHT = 8,
    VIS_CENTER = 16,
    VIS_CHANNEL_MASK = VIS_LEFT|VIS_RIGHT|VIS_LEFT,
};

struct Component {
    /**
     * Name of the component, taken from the original.
     */
    const char *name;

    /**
     * APE signature.
     */
    const char signature[32];

    /**
     * Reads the configuration state from the buffer and initialises extra data
     * structures beyond the private context.
     *
     * @return 0 on success, 1 to skip this component.
     */
    int (*load_config)(ComponentContext *, const uint8_t *, int);

    /**
     * Prints the configuration.
     */
    void (*print_config)(ComponentContext *, int);

    /**
     * Performs the rendering onto the frame.
     *
     * @return 1 if the rendered frame is in frame_out or a negative number in
     *         the event of some error.
     */
    int (*render)(ComponentContext *, uint8_t *, int *, int *, int, int, int);

    /**
     * Frees any allocated data structures except the private context.  This is
     * guaranteed to be called even if load_config fails so it needs to cope
     * with that situation.
     */
    void (*uninit)(ComponentContext *);

    /**
     * The size to allocate for the private data context.
     */
    size_t priv_size;

    /**
     * Unique identifier code.  This is filled in by the registration as a
     * convenience.
     */
    int code;

    /**
     * Do not touch.
     */
    struct Component *next;
};

struct ComponentContext {
    /**
     * The Component to which this context belongs.
     */
    const Component *component;

    /**
     * Pointer to the private data structure.
     */
    void *priv;

    /**
     * The global blend mode setting.  Opaque for most uses.
     */
    int blend_mode;

    /**
     * Which data type has been requested.
     */
    enum VisData which_vis_type;

    /**
     * Which channel has been requested.
     */
    enum VisData which_channel;

    /**
     * A counter for nested Effect List components.  This is to prevent too much
     * recursion.  A message is printed when a fixed limit is reached.  The
     * limit, setting, and checking is in effect_list.c.
     */
    int depth;

    /**
     * The AVSContext which owns and controls everything.
     */
    AVSContext *actx;
};

struct AVSContext {
    /**
     * Context for the root Effect List component.
     */
    ComponentContext root_list;

    /*
     * Common data structure for all components to use.
     */
    AVSDataContext avsdc;

    /** Global options.  See avs.h for the members and documentation. */
    AVSOptionsContext options;

    /**
     * Addresses of allocated frames.
     */
    int *frame[2];

    /**
     * Private data allocated by the beat analyzer.  Freed by us.
     */
    void *beat_context;

    /**
     * Globally buffered frames.  Freed by avs_uninit.  Use the lengthof macro
     * to get the maximum number of frames.
     */
    int *global_buffer[8];

    /**
     * The global megabuffer, for expression contexts.
     */
    double expr_global_megabuf[1048576];

    /**
     * The global debug registers, for expression contexts.
     */
    double expr_global_registers[100];
};

/**
 * Register the linked list of Component structures.
 */
void register_components(void);

/**
 * Find a Component structure by name or by numerical code.
 */
Component *find_component(int code, const uint8_t signature[32]);

#endif /* AVS_HEADER_COMPONENTS */
