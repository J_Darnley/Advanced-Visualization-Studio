/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "avs.h"
#include "components.h"

static Component *first_component = NULL;

void register_components(void)
{
    Component *prev_component;

    {
        extern Component effect_list;
        first_component = prev_component = &effect_list;
    }

#define REGISTER_COMPONENT(name)        \
    {                                   \
        extern Component name;          \
        prev_component->next = &(name); \
        prev_component = &(name);       \
    }

    REGISTER_COMPONENT(m_comment);
    REGISTER_COMPONENT(m_rendermode);
    REGISTER_COMPONENT(m_stack);
    REGISTER_COMPONENT(r_beatclear);
    REGISTER_COMPONENT(r_bspin);
    REGISTER_COMPONENT(r_clear);
    REGISTER_COMPONENT(r_dotfountain);
    REGISTER_COMPONENT(r_dotgrid);
    REGISTER_COMPONENT(r_dotplane);
    REGISTER_COMPONENT(r_oscstar);
    REGISTER_COMPONENT(r_particle);
    REGISTER_COMPONENT(r_ring);
    REGISTER_COMPONENT(r_rotstar);
    REGISTER_COMPONENT(r_simple);
    REGISTER_COMPONENT(r_starfield);
    REGISTER_COMPONENT(r_superscope);
    REGISTER_COMPONENT(r_timescope);
    REGISTER_COMPONENT(t_blit);
    REGISTER_COMPONENT(t_blur);
    REGISTER_COMPONENT(t_brightness);
    REGISTER_COMPONENT(t_bump);
    REGISTER_COMPONENT(t_channelshift);
    REGISTER_COMPONENT(t_colourclip);
    REGISTER_COMPONENT(t_colourfade);
    REGISTER_COMPONENT(t_colourmap);
    REGISTER_COMPONENT(t_colourmodifier);
    REGISTER_COMPONENT(t_colourreduction);
    REGISTER_COMPONENT(t_ddm);
    REGISTER_COMPONENT(t_dynamicmovement);
    REGISTER_COMPONENT(t_dynamicshift);
    REGISTER_COMPONENT(t_fadeout);
    REGISTER_COMPONENT(t_fastbrightness);
    REGISTER_COMPONENT(t_grain);
    REGISTER_COMPONENT(t_interferences);
    REGISTER_COMPONENT(t_interleave);
    REGISTER_COMPONENT(t_invert);
    REGISTER_COMPONENT(t_mirror);
    REGISTER_COMPONENT(t_multiplier);
    REGISTER_COMPONENT(t_multifilter);
    REGISTER_COMPONENT(t_mosaic);
    REGISTER_COMPONENT(t_movement);
    REGISTER_COMPONENT(t_rotoblit);
    REGISTER_COMPONENT(t_scatter);
    REGISTER_COMPONENT(t_texer);
    REGISTER_COMPONENT(t_uniquetone);
    REGISTER_COMPONENT(t_videodelay);
    REGISTER_COMPONENT(t_water);
    REGISTER_COMPONENT(t_waterbump);

    REGISTER_COMPONENT(not_implemented_avi);
    REGISTER_COMPONENT(not_implemented_bpm);
    REGISTER_COMPONENT(not_implemented_picture);
    REGISTER_COMPONENT(not_implemented_svp);
    REGISTER_COMPONENT(not_implemented_text);
    REGISTER_COMPONENT(not_implemented_avstrans);
    REGISTER_COMPONENT(not_implemented_fpslimiter);
    REGISTER_COMPONENT(not_implemented_convolution);
}

Component *find_component(int code, const uint8_t signature[32])
{
    Component *comp = first_component;
    while (comp) {
        if (comp->code == code || (signature && !(memcmp(signature, comp->signature, 32))))
            return comp;
        comp = comp->next;
    }
    return NULL;
}
