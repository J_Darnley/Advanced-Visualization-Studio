/*
 * Copyright (c) 2016
 *     Jakob K <grandchild@gmx.net>
 *     Sebastian Pipping <sebastian@pipping.org>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <inttypes.h>
#include <getopt.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <SDL.h>
#include <assert.h>

#if defined(_POSIX_C_SOURCE)
    #include <dlfcn.h>
    #include <sys/inotify.h>
    #include <fcntl.h>
    #include <errno.h>
#else
    #error "unknown platform"
#endif

#include "avs.h"

#define lengthof(a) ((int)(sizeof(a) / sizeof((a)[0])))

#define ERROR(message, ...) \
    fprintf(stderr, "(%s:%d) [ERROR] " message, __FILE__, __LINE__, ##__VA_ARGS__)
#define INFO(message, ...) \
    fprintf(stdout, "(%s:%d) [INFO] " message, __FILE__, __LINE__, ##__VA_ARGS__)

const struct option cmd_options[] = {
    { "help",          no_argument, 0, 'h' },
    { "input",   required_argument, 0, 'i' },
    { "library", required_argument, 0, 'l' },
    { "size",    required_argument, 0, 's' },
    { "verbose",       no_argument, 0, 'v' },
    { 0 }
};

static void usage(const char *prog)
{
    fprintf(stderr, "Usage: %s [OPTIONS]\n", prog);
    fprintf(stderr, "long options:\n");
    for (int i = 0; i < lengthof(cmd_options) - 1; i++) {
        /* Print an argument if needed. */
        const char *arg = "";
        if (cmd_options[i].has_arg == required_argument)
            arg = " arg";
        else if (cmd_options[i].has_arg == optional_argument)
            arg = " [arg]";
        /* Print short option if it is a good character. */
        if (isgraph(cmd_options[i].val))
            fprintf(stderr, "    -%c, --%s%s\n", cmd_options[i].val, cmd_options[i].name, arg);
        else
            fprintf(stderr, "        --%s%s\n", cmd_options[i].name, arg);
    }
}

struct TestOptions {
    char *file_lib;
    char *file_input;
    int width, height;
    int verbose;
};

struct AVSAPI {
    int (*avs_init)(AVSContext**, AVSOptionsContext*);
    int (*avs_load_preset)(AVSContext*, const char*);
    void (*avs_print_preset)(AVSContext*);
    int (*avs_render_frame)(AVSContext*, AVSDataContext*, uint32_t);
    void *(*avs_get_frame)(AVSContext*);
    void (*avs_uninit)(AVSContext*);
};

typedef struct watcher {
    char *filename;
    int watch_fd;
    int lib_modified;
} watcher_t;

static void sine_wave(uint8_t data[576], int amplitude, int periods)
{
    for (int i = 0; i < 576; i++)
        data[i] = sin((i * periods * 2 * M_PI)/575.0) * amplitude + 128.0;
}

static void *load_libary(char *filename, struct AVSAPI *api)
{
    void *handle = NULL;

    handle = dlopen(filename, RTLD_LAZY | RTLD_GLOBAL);
    if (!handle) {
        ERROR("%s (%s)\n", dlerror(), filename);
        return NULL;
    }

    dlerror(); // clear errors

    api->avs_init =
        (int (*)(AVSContext**, AVSOptionsContext*))
        dlsym(handle, "avs_init");
    api->avs_load_preset =
        (int (*)(AVSContext*, const char*))
        dlsym(handle, "avs_load_preset");
    api->avs_print_preset =
        (void (*)(AVSContext*))
        dlsym(handle, "avs_print_preset");
    api->avs_render_frame =
        (int (*)(AVSContext*, AVSDataContext*, uint32_t))
        dlsym(handle, "avs_render_frame");
    api->avs_get_frame =
        (void *(*)(AVSContext*))
        dlsym(handle, "avs_get_frame");
    api->avs_uninit =
        (void (*)(AVSContext*))
        dlsym(handle, "avs_uninit");

    char *error = dlerror();
    if (error != NULL) {
        ERROR("%s\n", error);
        return NULL;
    }

    return handle;
}

static void unload_library(void *handle)
{
    dlclose(handle);
    char *error = dlerror();
    if (error != 0) {
        ERROR("%s\n", error);
    }
}

static int start_watcher(const char *filepath, watcher_t *watcher)
{
    watcher->watch_fd = inotify_init();
    if (watcher->watch_fd < 0) {
        ERROR("starting inotify\n");
        return 1;
    }

    int path_length = strnlen(filepath, FILENAME_MAX);
    if (path_length > FILENAME_MAX) {
        ERROR("watch path too long (>%d)", FILENAME_MAX);
        return 1;
    }
    if (*(filepath + path_length - 1) == '/') {
        ERROR("no watch file name given");
        return 1;
    }
    char *separator = strrchr(filepath, '/');
    char *directory = NULL;
    const char *filename = NULL;
    if (separator==NULL) {
        INFO("no directory given");
        if(path_length <= 0) {
            ERROR("empty filename string");
            goto error;
        }
        filename = filepath;
        directory = calloc(2, sizeof(char));
        *directory = '.';
    } else {
        directory = strndup(filepath, separator - filepath);
        filename = separator + 1;
    }
    watcher->filename = calloc(strlen(filename)+1, sizeof(char));
    strcpy(watcher->filename, filename);

    fcntl(watcher->watch_fd, F_SETOWN, getpid());
    int flags = fcntl(watcher->watch_fd, F_GETFL, 0);
    fcntl(watcher->watch_fd, F_SETFL, flags | O_NONBLOCK);

    int watch_desc = inotify_add_watch(
        watcher->watch_fd,
        directory,
        IN_MODIFY | IN_CLOSE_WRITE);
    if (watch_desc < 0) {
        ERROR("starting file watcher: %d, %s\n", watch_desc, strerror(errno));
        goto error;
    }
    watcher->lib_modified = 0;
    free(directory);
    return 0;

error:
    free(directory);
    return 1;
}

static void check_watcher(watcher_t *watcher)
{
    while (1) {
        char buffer[sizeof(struct inotify_event) + FILENAME_MAX + 1];
        int len = read(watcher->watch_fd, buffer, sizeof(struct inotify_event) + FILENAME_MAX + 1);
        if (len <= 0) {
            break;
        }
        struct inotify_event *watch_event = (struct inotify_event*)buffer;
        if (strcmp(watch_event->name, watcher->filename) == 0) {
            if (watch_event->mask & IN_MODIFY) {
                watcher->lib_modified = 1;
            } else if (watch_event->mask & IN_CLOSE_WRITE) {
                INFO("Write finished\n");
                watcher->lib_modified = 1;
            }
        }
    }
}

static int render_frame(AVSContext *actx, AVSDataContext *avsdc,
        int height, u_int8_t *raw_image_dest, int bytes_per_row,
        uint32_t time_ms, struct AVSAPI *api) {
    const int avs_render_error = (*api->avs_render_frame)(actx, avsdc, time_ms);
    if (avs_render_error) {
        return avs_render_error;
    }

    uint8_t * const raw_image_source = (*api->avs_get_frame)(actx);
    assert(raw_image_source);

    memcpy(raw_image_dest, raw_image_source, bytes_per_row * height);

    return 0;
}

int main(int argc, char **argv)
{
    struct TestOptions options = {
        .width = 640, .height = 480,
    };

    int arg;
    while ((arg = getopt_long(argc, argv, "hi:l:s:v", cmd_options, 0)) != -1) switch (arg) {
        default:
            usage(argv[0]);
            return 1;

        case 'h':
            usage(argv[0]);
            return 0;

        case 'i':
            options.file_input = optarg;
            break;

        case 'l':
            options.file_lib = optarg;
            break;

        case 's': {
            char c;
            if (sscanf(optarg, "%dx%d%c", &options.width, &options.height, &c) != 2) {
                ERROR("unable to parse %s as frame dimensions\n", optarg);
                return 1;
            }
            break;
        }

        case 'v':
            options.verbose += 1;
            break;
    }

    if (!options.file_lib) {
        ERROR("No library file given\n");
        return -1;
    }

    if (!options.file_input) {
        ERROR("No input file given\n");
        return -1;
    }

    uint8_t waveform[576], spectrum[576];

    AVSContext *actx = NULL;
    AVSDataContext avsdc = {
        .waveform = { waveform, waveform, waveform },
        .spectrum = { spectrum, spectrum, spectrum },
    };
    AVSOptionsContext avs_options = {
        .structure_size = sizeof(avs_options),
        .api_version = AVS_API_VERSION,
    };

    memset(spectrum,      51, 128);
    memset(spectrum+128, 102, 128);
    memset(spectrum+256, 154, 128);
    memset(spectrum+384, 204, 128);
    sine_wave(waveform, 96, 1);

    int ret = 0;
    void *library = NULL;
    struct AVSAPI api;
    watcher_t watcher;

    if (start_watcher(options.file_lib, &watcher)) {
        goto error;
    }

    SDL_Init(SDL_INIT_VIDEO);

    SDL_Window * window = SDL_CreateWindow("AVS",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            options.width, options.height,
            SDL_WINDOW_RESIZABLE);
    assert(window);

    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1,
            SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
    assert(renderer);

    SDL_Texture * texture = SDL_CreateTexture(renderer,
            SDL_PIXELFORMAT_ARGB8888,
            SDL_TEXTUREACCESS_STREAMING,
            options.width, options.height);

    SDL_EventState(SDL_DROPFILE, SDL_ENABLE);
    SDL_Event event;
    u_int8_t * raw_image;
    const int expected_bytes_per_row = 4 * options.width;
    int actual_bytes_per_row;
    uint32_t time_ms = 1;

    while(1) {
        int lib_error_loading = 0;
        library = load_libary(options.file_lib, &api);
        if(!library) {
            ERROR("(re)loading AVS lib\n");
            lib_error_loading = 1;
            sleep(5);
            continue;
        }
        watcher.lib_modified = 0;

        avs_options.width = options.width;
        avs_options.height = options.height;
        avs_options.log_level = options.verbose;

        ret = (*api.avs_init)(&actx, &avs_options) || (*api.avs_load_preset)(actx, options.file_input);
        if (ret) {
            ERROR("initializing AVS\n");
            lib_error_loading = 1;
        }

        if (options.verbose) {
            (*api.avs_print_preset)(actx);
        }
        while(!watcher.lib_modified) {
            if(!lib_error_loading) {
                SDL_PollEvent(&event);
                switch (event.type) {
                    case SDL_QUIT: {
                        return 0;
                    } break;
                    case SDL_DROPFILE: {
                        if(strcmp(strrchr(event.drop.file, '.'), ".avs") == 0) {
                            (*api.avs_uninit)(actx);
                            ret = (*api.avs_init)(&actx, &avs_options) || (*api.avs_load_preset)(actx, options.file_input);
                            if (ret) {
                                return ret;
                            } else {
                                INFO("Loaded preset %s\n", event.drop.file);
                            }
                        }
                    }
                }

                const int lock_error = SDL_LockTexture(texture,
                        NULL,
                        (void **)&raw_image,
                        &actual_bytes_per_row);

                if (! lock_error) {
                    assert(actual_bytes_per_row == expected_bytes_per_row);

                    int i = 0;
                    for (; i < 576; i++) {
                        waveform[i] = rand();
                        spectrum[i] = rand();
                    }

                    ret = render_frame(actx, &avsdc, avs_options.height, raw_image, actual_bytes_per_row, time_ms, &api);

                    SDL_UnlockTexture(texture);

                    SDL_RenderClear(renderer);
                    SDL_RenderCopy(renderer, texture, NULL, NULL);
                    SDL_RenderPresent(renderer);
                }
                // ret = (*api.avs_render_frame)(actx, &avsdc, 1);
                if (ret) {
                    printf("!");
                    sleep(1);
                } else {
                    // printf(".");
                }
            } else {
                sleep(5);
                printf(":");
            }
            fflush(stdout);
            check_watcher(&watcher);
        }
        INFO("AVS lib modified, reloading...\n");
        (*api.avs_uninit)(actx);
        unload_library(library);
    }

error:
    // (*api.avs_uninit)(actx);

    return ret;
}
