local max, min, sqrt = math.max, math.min, math.sqrt;

local function printf(fmt, ...)
    io.write(string.format(fmt, ...));
end

local function new_stats()
    return {
        min = 1000000000,
        max = -1000000000,
        sum = 0,
        s2 = 0,
        num = 0,
        update = function(self, value)
            self.min = min(self.min, value);
            self.max = max(self.max, value);
            self.sum = self.sum + value;
            self.s2  = self.s2 + (value * value);
            self.num = self.num + 1;
        end,
        get_mean = function(self)
            local ret = 0;
            if self.num ~= 0 then
                ret = self.sum / self.num;
            end
            return ret;
        end,
        get_rms = function(self)
            local ret = 0;
            if self.num ~= 0 then
                ret = sqrt(self.s2 / self.num);
            end
            return ret;
        end,
        get_stddev = function(self)
            local ret = 0;
            if self.num ~= 0 then
                local sum = self.sum / self.num;
                local s2 = self.s2 / self.num;
                ret = sqrt(s2 - sum^2);
            end
            return ret;
        end,
        get_pm90 = function(self)
            local ret = 0;
            if self.num ~= 0 then
                local sum = self.sum / self.num;
                local s2 = self.s2 / self.num;
                ret = sqrt(s2 - sum^2) * 1.645 / sqrt(self.num);
            end
            return ret;
        end,
    };
end

local count = 8;
local a, b, c, d, xp, yp = {}, {}, {}, {}, {0, 1, 254, 255}, {0, 1, 254, 255}
do
    local time = os.time();
    -- perhaps mutate it in some fashion
    printf("seed = %d\n", time);
    math.randomseed(time);

    local random = math.random;
    local function rand_rgb()
        local red = random(0,255);
        local green = random(0,255);
        local blue = random(0,255);
        return red + green*256 + blue*65536;
    end

    local insert = table.insert;
    for i = 1, count do
        insert(a, rand_rgb());
        insert(b, rand_rgb());
        insert(c, rand_rgb());
        insert(d, rand_rgb());
        insert(xp, random(2,253));
        insert(yp, random(2,235));
    end
end

for _,entry in ipairs(pixel_ops) do
    local func_c, func_i, op = entry.c, entry.i, entry.op;
    local stats = new_stats();
    local ok = false;

    printf("Testing %s... ", op);
    io.output():flush();

    if op == "adj" then
        for i = 1, count do
            local a = a[i];
            for j = 1, count do
                local b = b[j];
                for k = 1, count+4 do
                    local v = xp[k];
                    stats:update(func_i(a,b,v) - func_c(a,b,v));
                end
            end
        end

    elseif op == "pix4" then
        for i = 1, count do
            local a = a[i];
            for j = 1, count do
                local b = b[j];
                for k = 1, count do
                    local c = c[k];
                    for l = 1, count do
                        local d = d[l];
                        for m = 1, count+4 do
                            local xp = xp[m];
                            for n = 1, count+4 do
                                local yp = yp[n];
                                stats:update(func_i(a,b,c,d,xp,yp) - func_c(a,b,c,d,xp,yp));
                            end
                        end
                    end
                end
            end
        end

    else
        for a = 0, 255 do
            for b = 0, 255 do
                stats:update(func_i(a,b) - func_c(a,b));
            end
        end
    end

    if stats.s2 == 0 then
        printf("OK\n");
    else
        printf("not OK\n");
        stats = new_stats();

        -- get stats for 0-255 values
        if op == "adj" then
            printf("Please report this difference.\n");
            for a = 0, 255 do
                for b = 0, 255 do
                    for v = 0, 255 do
                        stats:update(func_i(a,b,v) - func_c(a,b,v));
                    end
                end
            end

        elseif op == "pix4" then
            for i = 1, count+4 do
                local a = xp[i];
                for j = 1, count+4 do
                    local b = yp[j];
                    for k = 1, count+4 do
                        local c = xp[k];
                        for l = 1, count+4 do
                            local d = yp[l];
                            for m = 1, count+4 do
                                local xp = xp[m];
                                for n = 1, count+4 do
                                    local yp = yp[n];
                                    stats:update(func_i(a,b,c,d,xp,yp) - func_c(a,b,c,d,xp,yp));
                                end
                            end
                        end
                    end
                end
            end

        else
            printf("Please report this difference.\n");
            for a = 0, 255 do
                for b = 0, 255 do
                    stats:update(func_i(a,b) - func_c(a,b));
                end
            end
        end

        printf("\tmin: %.3g, max: %.3g, mean: %.3g, stddev: %.3g, rms: %.3g\n",
            stats.min, stats.max, stats:get_mean(), stats:get_stddev(), stats:get_rms());
    end
end
