/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "block.c"
#include "x86/init.c"

/* Okay, this is horrible but it tested what I wanted it to. */

static uint8_t buffer[4][256];

void (*mmx2[7])(int *, int *, int *, int) = {
    avsint_blend_block_avg_mmx2,
    avsint_blend_block_add_mmx,
    avsint_blend_block_sub_mmx,
    avsint_blend_block_xor_mmx,
    avsint_blend_block_max_mmx2,
    avsint_blend_block_min_mmx2,
    avsint_blend_block_mul_mmx,
};

void (*sse2[7])(int *, int *, int *, int) = {
    avsint_blend_block_avg_sse2,
    avsint_blend_block_add_sse2,
    avsint_blend_block_sub_sse2,
    avsint_blend_block_xor_sse2,
    avsint_blend_block_max_sse2,
    avsint_blend_block_min_sse2,
    avsint_blend_block_mul_sse2,
};

void (*avx2[7])(int *, int *, int *, int) = {
    avsint_blend_block_avg_avx2,
    avsint_blend_block_add_avx2,
    avsint_blend_block_sub_avx2,
    avsint_blend_block_xor_avx2,
    avsint_blend_block_max_avx2,
    avsint_blend_block_min_avx2,
    avsint_blend_block_mul_avx2,
};

static int ref_avg(int a, int b)
{
    return (a + b + 1) >> 1;
}

static int ref_add(int a, int b)
{
    return (a + b > 255) ? 255 : a + b;
}

static int ref_sub(int a, int b)
{
    return (a - b < 0) ? 0 : a - b;
}

static int ref_xor(int a, int b)
{
    return a ^ b;
}

static int ref_max(int a, int b)
{
    return (a > b) ? a : b;
}

static int ref_min(int a, int b)
{
    return (a < b) ? a : b;
}

static int ref_mul(int a, int b)
{
    return (a * b) >> 8;
}

static int ref_adj(int a, int b, int v)
{
    return (a * v + b * (255-v)) >> 8;
}

int (*c_ref[7])(int, int) = {
    ref_avg,
    ref_add,
    ref_sub,
    ref_xor,
    ref_max,
    ref_min,
    ref_mul,
};

int main(void)
{
    uint8_t *a = buffer[0];
    uint8_t *b = buffer[1];
    uint8_t *ref = buffer[2];
    uint8_t *xyz = buffer[3];
    int ret = 0;

    for (int j = 0; j < 256; j++)
        b[j] = j;

    for (int type = 0; type < 7; type++) {

        for (int i = 0; i < 256; i++) {
            memset(a, i, 256);

            mmx2[type]((int*)xyz, (int*)a, (int*)b, 64);

            for (int j = 0; j < 256; j++) {
                ref[j] = c_ref[type](i, j);
                if (ref[j] != xyz[j]) {
                    ret = 1;
                    printf("(%3d * %3d) >> 8 = %3d, asm = %3d\n",
                            i, b[j], ref[j], xyz[j]);
                }
            }
        }
    }

    /* adjustable */

    for (int v = 0; v < 256; v++) {
        for (int i = 0; i < 256; i++) {
            memset(a, i, 256);

            avsint_blend_block_adj_sse2((int*)xyz, (int*)a, (int*)b, 64, v);

            for (int j = 0; j < 256; j++) {
                ref[j] = ref_adj(i, j, v);
                if (ref[j] != xyz[j]) {
                    ret = 1;
                    printf("(%3d * %3d + %3d * %3d) >> 8 = %3d, asm = %3d\n",
                            i, v, b[j], 255-v, ref[j], xyz[j]);
                }
            }
        }
    }

    return ret;
}
