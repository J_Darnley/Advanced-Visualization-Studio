/*
 * Copyright (c) 2016 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "avs.h"
#include "log.h"

static const int log_color[] = {
    [AVS_LOG_ERROR] = 31,
    [AVS_LOG_WARNING] = 33,
    [AVS_LOG_VERBOSE] = 32,
    [AVS_LOG_DEBUG] = 34,
    [AVS_LOG_TRACE] = 34,
};

static int log_level = AVS_LOG_INFO;

static void log_default(int level, const char *format, va_list arguments)
{
    if (level > log_level)
        return;
    if (level != AVS_LOG_INFO)
        fprintf(stderr, "\x1B[%dm", log_color[level]);
    vfprintf(stderr, format, arguments);
    if (level != AVS_LOG_INFO)
        fprintf(stderr, "\x1B[0m");
}

static void (*log_callback)(int, const char *, va_list) = log_default;

void avs_log(int level, const char *format, ...)
{
    va_list arguments;
    va_start(arguments, format);
    log_callback(level, format, arguments);
    va_end(arguments);
}

void avs_log_set_callback(void (*callback)(int, const char *, va_list))
{
    if (callback)
        log_callback = callback;
}

void avs_log_set_level(int level)
{
    log_level = level;
}
