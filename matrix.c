/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This file is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <string.h>

#include "matrix.h"

void matrix_rotate(float matrix[], char m, float deg)
{
    char m1, m2;
    float c, s;
    deg *= 3.141592654 / 180.0;

    memset(matrix, 0, sizeof(float) * 16);

    matrix[((m - 1) << 2) + m - 1] = matrix[15] = 1.0;

    m1 = m % 3;
    m2 = (m1 + 1) % 3;
    c = cosf(deg);
    s = sinf(deg);

    matrix[(m1 << 2) + m1] = c;
    matrix[(m1 << 2) + m2] = s;
    matrix[(m2 << 2) + m2] = c;
    matrix[(m2 << 2) + m1] = -s;
}

void matrix_translate(float matrix[], float x, float y, float z)
{
    memset(matrix, 0, sizeof(float) * 16);
    matrix[0] = matrix[4 + 1] = matrix[8 + 2] = matrix[12 + 3] = 1.0;
    matrix[0 + 3] = x;
    matrix[4 + 3] = y;
    matrix[8 + 3] = z;
}

void matrix_multiply(float *dest, float src[])
{
    float temp[16];

    memcpy(temp, dest, sizeof(float) * 16);

#if 0
    for (int i = 0; i < 16; i += 4)
        for (int j = 0; j < 4; j++)
            *dest++ = src[i + 0] * temp[(0 << 2) + j]
                    + src[i + 1] * temp[(1 << 2) + j]
                    + src[i + 2] * temp[(2 << 2) + j]
                    + src[i + 3] * temp[(3 << 2) + j];

#else
    for (int i = 0; i < 16; i += 4) {
        *dest++ = src[i + 0] * temp[(0 << 2) + 0]
                + src[i + 1] * temp[(1 << 2) + 0]
                + src[i + 2] * temp[(2 << 2) + 0]
                + src[i + 3] * temp[(3 << 2) + 0];

        *dest++ = src[i + 0] * temp[(0 << 2) + 1]
                + src[i + 1] * temp[(1 << 2) + 1]
                + src[i + 2] * temp[(2 << 2) + 1]
                + src[i + 3] * temp[(3 << 2) + 1];

        *dest++ = src[i + 0] * temp[(0 << 2) + 2]
                + src[i + 1] * temp[(1 << 2) + 2]
                + src[i + 2] * temp[(2 << 2) + 2]
                + src[i + 3] * temp[(3 << 2) + 2];

        *dest++ = src[i + 0] * temp[(0 << 2) + 3]
                + src[i + 1] * temp[(1 << 2) + 3]
                + src[i + 2] * temp[(2 << 2) + 3]
                + src[i + 3] * temp[(3 << 2) + 3];
    }
#endif
}

void matrix_apply(float matrix[], float x, float y, float z, float *out_x, float *out_y, float *out_z)
{
    *out_x = x * matrix[0] + y * matrix[1] + z * matrix[2] + matrix[3];
    *out_y = x * matrix[4] + y * matrix[5] + z * matrix[6] + matrix[7];
    *out_z = x * matrix[8] + y * matrix[9] + z * matrix[10] + matrix[11];
}
